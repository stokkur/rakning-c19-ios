//
//  AppColor.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

public protocol Colors: AnyObject {
    var lightOrange: UIColor { get }
    var darkOrange: UIColor { get }
    var darkBlue: UIColor { get }
    var white: UIColor { get }
    var darkGray: UIColor { get }
    var gray: UIColor { get }
    var iconGray: UIColor { get }
    var green: UIColor { get }
    var red: UIColor { get }
    var lightGray: UIColor { get }
}

final class AppColors: Colors {
    var lightOrange: UIColor { return color(for: "LightOrange") }
    var darkOrange: UIColor { return color(for: "DarkOrange") }
    var darkBlue: UIColor { return color(for: "DarkBlue") }
    var white: UIColor { return color(for: "White") }
    var darkGray: UIColor { return color(for: "DarkGray") }
    var gray: UIColor { return color(for: "Gray") }
    var iconGray: UIColor { return color(for: "IconGray") }
    var green: UIColor { return color(for: "Green") }
    var red: UIColor { return color(for: "Red") }
    var lightGray: UIColor { return color(for: "LightGray") }

    private func color(for name: String) -> UIColor {
        let bundle = Bundle(for: AppColors.self)
        if let color = UIColor(named: name, in: bundle, compatibleWith: nil) {
            return color
        }
        return .clear
    }
}
