//
//  Image.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

import UIKit

final class Image: UIImage {
    static let appColors = AppColors()
    class func named(_ name: String) -> UIImage? {
        let bundle = Bundle(for: Image.self)

        return UIImage(named: name, in: bundle, compatibleWith: nil)
    }
}

extension UIImage {
    // MARK: - Onboarding
    static var backgroundDecoration: UIImage? { return Image.named("BackgroundDecoration") }
    static var appLogo: UIImage? { return Image.named("AppLogo") }
    static var exposureOnboarding: UIImage? { return Image.named("ExposureOnboarding") }
    static var exposure: UIImage? { return Image.named("Exposure") }
    static var bluetooth: UIImage? { return Image.named("Bluetooth") }
    static var coatOfArms: UIImage? { return Image.named("CoatOfArms") }
    static var helpInfo: UIImage? { return Image.named("InfoHelp") }
    static var bluetoothInfo: UIImage? { return Image.named("InfoBluetooth") }
    static var notificationInfo: UIImage? { return Image.named("InfoNotification") }
    static var shortExposureInfo: UIImage? { return Image.named("InfoShortExposure") }
    static var longExposureInfo: UIImage? { return Image.named("InfoLongExposure") }
    static var backArrow: UIImage? { return Image.named("BackArrow") }
    static var closeCircle: UIImage? { return Image.named("CloseCircle") }
    static var englishFlag: UIImage? { return Image.named("EnglishFlag") }
    static var icelandicFlag: UIImage? { return Image.named("IcelandicFlag") }
    static var polskiFlag: UIImage? { return Image.named("PolskiFlag") }
    
    // MARK: - Exposure
    static var calendar: UIImage? { return Image.named("Calendar") }
    static var information: UIImage? { return Image.named("Information") }
    static var attention: UIImage? { return Image.named("Attention") }
    static var noExposures: UIImage? { return Image.named("NoExposures") }
    static var exposureOff: UIImage? { return Image.named("ExposuresOff") }
    
    // MARK: - Notify
    static var notify: UIImage? { return Image.named("Notify") }
    static var checkmarkCircle: UIImage? { Image.named("CheckmarkCircle") }
    static var close: UIImage? { return Image.named("Close")?.withRenderingMode(.alwaysOriginal) }
    
    // MARK: - TabBar
    static var exposureTabActive: UIImage? { return Image.named("ExposureTabActive")?.withRenderingMode(.alwaysOriginal) }
    static var exposureTabInactive: UIImage? { return Image.named("ExposureTabInactive")?.withRenderingMode(.alwaysOriginal) }
    static var notifyTabActive: UIImage? { return Image.named("NotifyTabActive")?.withRenderingMode(.alwaysOriginal) }
    static var notifyTabInactive: UIImage? { return Image.named("NotifyTabInactive")?.withRenderingMode(.alwaysOriginal) }
    static var settingsTabActive: UIImage? { return Image.named("SettingsTabActive")?.withRenderingMode(.alwaysOriginal) }
    static var settingsTabInactive: UIImage? { return Image.named("SettingsTabInactive")?.withRenderingMode(.alwaysOriginal) }
    
    // MARK: - Settings images
    static var language: UIImage? { return Image.named("Language") }
    static var exposureNotification: UIImage? { return Image.named("ExposureNotification") }
    static var alertBell: UIImage? { return Image.named("AlertBell") }
    static var exclamationCircle: UIImage? { return Image.named("InfoCircle") }
    static var docPlain: UIImage? { return Image.named("LegalSheet") }
    static var checkShield: UIImage? { return Image.named("PrivacyShield") }
    static var squareArrowUp: UIImage? { return Image.named("Localization") }
    static var brackets: UIImage? { return Image.named("CodeBrackets") }
    static var debugBug: UIImage? { return Image.named( "CodeBrackets") }
    static var share: UIImage? { return Image.named("ShareApp") }
}
