//
//  AppDelegate.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import UIKit
import ExposureNotification
import BackgroundTasks
import UserNotifications
import CoreData
import Firebase
import FirebaseMessaging

protocol CoviceAppDelegate: AnyObject {
    var theme: Theme { get }
    var localStorageManager: LocalStorageManager { get }
    var networkManager: NetworkManager { get }
    var exposureManager: ExposureManager { get }
    var storageManager: StorageManager { get }
    var coreDataManager: CoreDataManager { get }
}

extension AppDelegate: CoviceAppDelegate {}

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    let backgroundTaskIdentifier = Bundle.main.bundleIdentifier! + ".exposure-notification"
    static let negativeTestNotificationName = NSNotification.Name(rawValue: Bundle.main.bundleIdentifier! + ".negative-test-received")
    static let exposureDetectedNotificationName = NSNotification.Name(rawValue: Bundle.main.bundleIdentifier! + ".exposure-detected")
    static let bluetoothOffNotificationName = NSNotification.Name(rawValue: Bundle.main.bundleIdentifier! + ".bluetooth-off")
    static let bluetoothOffAlertNotificationName = NSNotification.Name(rawValue: Bundle.main.bundleIdentifier! + ".bluetooth-off-alert")
    static let phoneReRegistrationNotificationName = NSNotification.Name(rawValue: Bundle.main.bundleIdentifier! + ".bluetooth-off")
    
    var theme: Theme = AppTheme()
    
    lazy var networkManager: NetworkManager = NetworkManager(configuration: .networkConfiguration, session: .shared, storageManager: storageManager)
    
    var exposureManager = ExposureManager()
    
    var storageManager = StorageManager()
    
    var localStorageManager = LocalStorageManager()
    
    var coreDataManager = CoreDataManager(coreData: CoreDataStack())
    
    // Add window for iOS 12 support
    var window: UIWindow?
    lazy var navigationController: UINavigationController = {
        let controller = NavigationController(theme: theme)
        controller.setNavigationBarHidden(true, animated: false)
        
        return controller
    }()
    lazy var appCoordinator: AppCoordinator = {
       return AppCoordinator(navigationController: navigationController,
                             theme: theme,
                             networkManager: networkManager,
                             exposureManager: exposureManager,
                             storageManager: storageManager,
                             localStorageManager: localStorageManager,
                             coreDataManager: coreDataManager)
    }()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 13.0, *) {
            // Do nothing, scene delegate will handle the window initialization.
        }
        else {
            let window = UIWindow(frame: UIScreen.main.bounds)
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
            self.window = window
            
            self.appCoordinator.start()
        }
        
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        // Clear badge icon.
        application.applicationIconBadgeNumber = 0
        
        // In iOS 13.5 and later, the Background Tasks framework is available,
        // so create and schedule a background task for downloading keys and
        // detecting exposures
        if #available(iOS 13.5, *) {
            // In iOS 13.5 and later, the Background Tasks framework is available,
            // so create and schedule a background task for downloading keys and
            // detecting exposures
            createBackgroundTaskIfNeeded()
            scheduleBackgroundTaskIfNeeded()
        } else if ENManagerIsAvailable() {
            // If `ENManager` exists, and the iOS version is earlier than 13.5,
            // the app is running on iOS 12.5, where the Background Tasks
            // framework is unavailable. Specify an EN activity handler here, which
            // allows the app to receive background time for downloading keys
            // and looking for exposures when background tasks aren't available.
            // Apps should should call this method before calling activate().
            self.exposureManager.manager.setLaunchActivityHandler { [weak self] (activityFlags) in
                guard let this = self else { return }
                if activityFlags.contains(.periodicRun) {
                    this.iOS12DetectExposure()
                }
            }
        }
        exposureManager.activate()
        
        if !self.localStorageManager.hasPhoneBeenReset.wrappedValue {
            if self.localStorageManager.registeredPhoneNumber.wrappedValue != nil {
                self.localStorageManager.registeredPhoneNumber.wrappedValue = nil
                LocalNotification.sendLocalNotification(title: Localization.phone_re_registration_title, body: Localization.phone_re_registration_body,identifier: .PhoneReregistration)
            }
            self.localStorageManager.hasPhoneBeenReset.wrappedValue = true
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // Notification received and app was in background
        switch response.notification.request.content.categoryIdentifier {
        case LocalNotification.LocalIdentifier.ExposureDetected.identifier:
            NotificationCenter.default.post(name: AppDelegate.exposureDetectedNotificationName, object: nil)
        case LocalNotification.LocalIdentifier.BluetoothOff.identifier:
            NotificationCenter.default.post(name: AppDelegate.bluetoothOffNotificationName, object: nil)
        case LocalNotification.LocalIdentifier.PhoneReregistration.identifier:
            NotificationCenter.default.post(name: AppDelegate.phoneReRegistrationNotificationName, object: nil)
        default:
            localStorageManager.showNegativeTestResult.wrappedValue = true
            NotificationCenter.default.post(name: AppDelegate.negativeTestNotificationName, object: nil)
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Noification received and app was open.
        switch notification.request.content.categoryIdentifier {
        case LocalNotification.LocalIdentifier.ExposureDetected.identifier:
            NotificationCenter.default.post(name: AppDelegate.exposureDetectedNotificationName, object: nil)
        case LocalNotification.LocalIdentifier.BluetoothOff.identifier:
            NotificationCenter.default.post(name: AppDelegate.bluetoothOffAlertNotificationName, object: nil)
        case LocalNotification.LocalIdentifier.PhoneReregistration.identifier:
            NotificationCenter.default.post(name: AppDelegate.phoneReRegistrationNotificationName, object: nil)
        default:
            NotificationCenter.default.post(name: AppDelegate.negativeTestNotificationName, object: nil)
        }
        completionHandler(.sound)
    }
    
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
    }
}
