//
//  GradientView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class GradientView: View {
    var colors: [UIColor] = [] {
        didSet {
            gradient.colors = colors.map { $0.cgColor }
        }
    }
    
    var locations: [NSNumber] = [] {
        didSet {
            gradient.locations = locations
        }
    }

    private lazy var gradient: CAGradientLayer = {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        
        return layer
    }()
    
    override init(theme: Theme, _ setDefaultBackground: Bool = false) {
        super.init(theme: theme, setDefaultBackground)
    }
    
    override func build() {
        super.build()
        
        layer.addSublayer(gradient)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradient.frame = self.bounds
    }
}
