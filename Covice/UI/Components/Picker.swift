//
//  File.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol PickerDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
}

protocol PickerDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
}

class Picker: UITextField, Themeable, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var triangleWidth: CGFloat = 10.scaled {
        didSet{
            self.setNeedsLayout()
        }
    }
    var triangleHeight: CGFloat = 5.scaled {
        didSet{
            self.setNeedsLayout()
        }
    }
    var rightViewSpacing: CGFloat = 15.scaled {
        didSet{
            self.setNeedsLayout()
        }
    }
    
    var pickerDataSource: PickerDataSource?
    var pickerDelegate: PickerDelegate?
    
    private lazy var pickerAccessory: UIToolbar = {
        let view = UIToolbar()
        view.autoresizingMask = .flexibleHeight
        view.frame.size.height = 44.scaled
        return view
    }()

    private lazy var containerRightView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.bounds.size.width =  rightViewSpacing + triangleWidth
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var triangle: Triangle = {
        let view = Triangle(theme: theme)
        view.bounds.size.width = triangleWidth
        view.bounds.size.height = triangleHeight
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var pickerView: UIPickerView = {
        let view = UIPickerView()
        view.backgroundColor = theme.colors.lightGray
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    
    private lazy var doneBarButton: UIBarButtonItem = {
        let view = UIBarButtonItem(title: Localization.btn_done, style: .done, target: self, action: #selector(donePessed))
        return view
    }()
    
    let theme: Theme
    let gestureRecognizer = UIGestureRecognizer()
    let padding: UIEdgeInsets
    
    
    required init( theme: Theme, with padding: UIEdgeInsets, displayDone: Bool = true) {
        self.padding = padding
        self.theme = theme
        super.init(frame: .zero)
        setupPickerToolbar(displayDone)
        setupRightView()
    }
    
    required init( theme: Theme, displayDone: Bool = true) {
        self.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.theme = theme
        super.init(frame: .zero)
        setupPickerToolbar(displayDone)
        setupRightView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateRightView()
    }
    
    private func updateRightView(){
        containerRightView.bounds.size.height = self.bounds.size.height
        
        triangle.layer.position.x = (containerRightView.bounds.size.width-(triangleWidth/2))-padding.right
        triangle.layer.position.y = (containerRightView.bounds.size.height/2)
    }
    
    private func setupRightView(){
        // Add Gesture recognizer to right view
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(self.rightViewTouched))
        triangle.isUserInteractionEnabled = true
        triangle.addGestureRecognizer(tapGesture)
        containerRightView.isUserInteractionEnabled = true
        containerRightView.addGestureRecognizer(tapGesture)
        self.rightView = containerRightView
        containerRightView.addSubview(triangle)
    }
    
    private func setupPickerToolbar(_ displayDone:Bool){
        translatesAutoresizingMaskIntoConstraints = false
        if displayDone {
            self.inputAccessoryView = pickerAccessory
            let toolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: UIScreen.main.bounds.width, height: CGFloat(44.scaled))))
            toolbar.sizeToFit()
            toolbar.backgroundColor = self.theme.colors.lightOrange
            toolbar.barTintColor = self.theme.colors.lightOrange
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            toolbar.items = [ flexSpace, self.doneBarButton]
            toolbar.isUserInteractionEnabled = true
            self.inputAccessoryView = toolbar
        }
        self.inputView = pickerView
        self.rightViewMode = .always
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
    
    func selectRow(_ row: Int){
        self.pickerView.selectRow(row, inComponent: 0, animated: false)
    }
    
    func reloadView(){
        doneBarButton.title = Localization.btn_done
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        guard let ds = self.pickerDataSource else {
            return 0
        }
        return ds.numberOfComponents(in: pickerView)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard let ds = self.pickerDataSource else {
            return 0
        }
        return ds.pickerView(pickerView, numberOfRowsInComponent: component)
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let del = pickerDelegate else {
            return nil
        }
        return del.pickerView(pickerView, titleForRow: row, forComponent: component)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.text = self.pickerView(pickerView, titleForRow: row, forComponent: component)
        self.pickerView.reloadAllComponents()
        if let del = pickerDelegate {
            del.pickerView(pickerView, didSelectRow: row, inComponent: component)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        guard let rowString = self.pickerView(pickerView, titleForRow: row, forComponent: component) else {
            return NSAttributedString(
                string: "English",
                attributes: [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.scaled),
                    NSAttributedString.Key.foregroundColor:UIColor.black
                ]
            )
        }
        return NSAttributedString(
            string: rowString,
            attributes: [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.scaled),
                NSAttributedString.Key.foregroundColor:UIColor.black
            ]
        )
    }
    
    @objc private func rightViewTouched() {
        self.becomeFirstResponder()
    }
    
    @objc private func donePessed(){
        self.resignFirstResponder()
    }
    
}
