//
//  PickerLabel.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class PickerLabel: UILabel, Themeable, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var pickerDataSource: PickerDataSource?
    var pickerDelegate: PickerDelegate?
    
    private lazy var pickerView: UIPickerView = {
        let view = UIPickerView()
        view.backgroundColor = theme.colors.lightGray
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    
    private lazy var doneBarButton: UIBarButtonItem = {
        let view = UIBarButtonItem(title: Localization.btn_done, style: .done, target: self, action: #selector(donePessed))
        return view
    }()
    
    private let toolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: UIScreen.main.bounds.width, height: CGFloat(44.scaled))))
    
    override var inputView: UIView? {
        return pickerView
    }
    
    override var inputAccessoryView: UIView? {
        return toolbar
    }
    
    let theme: Theme
    let gestureRecognizer = UIGestureRecognizer()
    
    required init( theme: Theme, displayDone: Bool = true) {
        self.theme = theme
        super.init(frame: .zero)
        setupPickerToolbar(displayDone)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    private func setupPickerToolbar(_ displayDone:Bool){
        translatesAutoresizingMaskIntoConstraints = false
        if displayDone {
            toolbar.sizeToFit()
            toolbar.backgroundColor = self.theme.colors.lightOrange
            toolbar.barTintColor = self.theme.colors.lightOrange
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            toolbar.items = [ flexSpace, self.doneBarButton]
            toolbar.isUserInteractionEnabled = true
        }
    }
    
    func selectRow(_ row: Int){
        self.pickerView.selectRow(row, inComponent: 0, animated: false)
    }
    
    func reloadView(){
        doneBarButton.title = Localization.btn_done
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        guard let ds = self.pickerDataSource else {
            return 0
        }
        return ds.numberOfComponents(in: pickerView)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard let ds = self.pickerDataSource else {
            return 0
        }
        return ds.pickerView(pickerView, numberOfRowsInComponent: component)
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let del = pickerDelegate else {
            return nil
        }
        return del.pickerView(pickerView, titleForRow: row, forComponent: component)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.text = self.pickerView(pickerView, titleForRow: row, forComponent: component)
        self.pickerView.reloadAllComponents()
        if let del = pickerDelegate {
            del.pickerView(pickerView, didSelectRow: row, inComponent: component)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        guard let rowString = self.pickerView(pickerView, titleForRow: row, forComponent: component) else {
            return NSAttributedString(
                string: "English",
                attributes: [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.scaled),
                    NSAttributedString.Key.foregroundColor:UIColor.black
                ]
            )
        }
        return NSAttributedString(
            string: rowString,
            attributes: [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.scaled),
                NSAttributedString.Key.foregroundColor:UIColor.black
            ]
        )
    }
    
    @objc private func donePessed(){
        self.resignFirstResponder()
    }
    
}
