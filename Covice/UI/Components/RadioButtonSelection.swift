//
//  RadioButtonSelection.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


protocol RadioButtonSelectionDelegate {
    func didSelectOption<T>(_ radioButtonSelection: RadioButtonSelection<T>, option: T)
    func didSelectDateOption<T>(_ radioButtonSelection: RadioButtonSelection<T>, option: T, date: Date)
}

protocol RadioButtonOption: CaseIterable, Equatable {
    var title: String { get }
    var type: RadioButtonType { get }
}

class RadioButtonSelection<T: RadioButtonOption>: View {
    private let options = T.allCases
    
    private let initialSelection: T?
    
    private var selectedButton: RadioButtonSelectable? {
        willSet {
            selectedButton?.isSelected = false
        }
        
        didSet {
            selectedButton?.isSelected = true
        }
    }
    var delegate: RadioButtonSelectionDelegate?
    
    lazy var stackView: UIStackView = {
        let buttons = options.map { (option) -> UIView in
            
            switch option.type {
            case .normal:
                let button = RadioButton(theme: theme)
                button.title = option.title
                
                button.action = { [weak self] in
                    guard let this = self else { return }
                    this.selectedButton = button
                    this.delegate?.didSelectOption(this, option: option)
                }
                
                if initialSelection == option {
                    self.selectedButton = button
                    button.isSelected = true
                }
                
                return button
            
            case .datePicker:
                let button = RadioDatePickerButton(theme: theme)
                button.title = option.title
                
                button.action = { [weak self] date in
                    guard let this = self else { return }
                    this.selectedButton = button
                    this.delegate?.didSelectDateOption(this, option: option, date: date)
                }
                
                if initialSelection == option {
                    self.selectedButton = button
                    button.isSelected = true
                }
                
                return button
            }
        }
        
        let view = UIStackView(arrangedSubviews: buttons)
        view.axis = .vertical
        view.spacing = 30.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    required init(theme: Theme, initiallySelected: T?) {
        self.initialSelection = initiallySelected
        super.init(theme: theme)
    }
    
    override func build() {
        super.build()
        
        addSubview(stackView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
}


// MARK: - Radiobutton types

enum RadioButtonType {
    case normal
    case datePicker
}

protocol RadioButtonSelectable {
    var isSelected: Bool { get set }
}

class RadioButton: View, RadioButtonSelectable {
    init(theme: Theme) {
        super.init(theme: theme)
        
        self.isUserInteractionEnabled = true

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(touchAction))
        self.addGestureRecognizer(gestureRecognizer)
    }
    
    var action: (() -> ())?
    var useHapticFeedback = true
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var isSelected: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let this = self else { return }
                
                this.fillView.backgroundColor = this.isSelected ? this.theme.colors.darkOrange : .clear
            }
        }
    }
    
    lazy var outlineView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 12.scaled
        view.layer.borderColor = theme.colors.darkOrange.cgColor
        view.layer.borderWidth = 2.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var fillView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.cornerRadius = 6.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = theme.colors.darkBlue
        label.font = .systemFont(ofSize: 16.scaled, weight: .semibold)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    override func build() {
        super.build()
        
        addSubview(outlineView)
        outlineView.addSubview(fillView)
        addSubview(titleLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            outlineView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            outlineView.topAnchor.constraint(equalTo: self.topAnchor),
            outlineView.heightAnchor.constraint(equalToConstant: 24.scaled),
            outlineView.widthAnchor.constraint(equalToConstant: 24.scaled),
        ])
        
        NSLayoutConstraint.activate([
            fillView.centerXAnchor.constraint(equalTo: outlineView.centerXAnchor),
            fillView.centerYAnchor.constraint(equalTo: outlineView.centerYAnchor),
            fillView.heightAnchor.constraint(equalToConstant: 12.scaled),
            fillView.widthAnchor.constraint(equalToConstant: 12.scaled),
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: outlineView.trailingAnchor, constant: 10.scaled),
            titleLabel.centerYAnchor.constraint(equalTo: outlineView.centerYAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            titleLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor),
        ])
    }
    
    @objc private func touchAction() {
        guard !isSelected else { return }
        
        if useHapticFeedback { Haptic.light() }
        isSelected = true
        
        action?()
    }
}


class RadioDatePickerButton: View, RadioButtonSelectable {
    init(theme: Theme) {
        super.init(theme: theme)
        
        self.isUserInteractionEnabled = true

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(touchAction))
        self.addGestureRecognizer(gestureRecognizer)
    }
    
    var action: ((Date) -> ())?
    var useHapticFeedback = true
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var isSelected: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.2) { [weak self] in
                guard let this = self else { return }
                
                this.fillView.backgroundColor = this.isSelected ? this.theme.colors.darkOrange : .clear
            }
        }
    }
    
    lazy var outlineView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 12.scaled
        view.layer.borderColor = theme.colors.darkOrange.cgColor
        view.layer.borderWidth = 2.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var fillView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.cornerRadius = 6.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = theme.colors.darkBlue
        label.font = .systemFont(ofSize: 16.scaled, weight: .semibold)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var pickerContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var pickerTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = theme.colors.darkBlue
        label.font = .systemFont(ofSize: 10.scaled, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Localization.symptoms_start_date
        
        return label
    }()
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.maximumDate = Date()
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .compact
        } else {
            // Default date picker style
        }
        picker.tintColor = theme.colors.darkBlue
        picker.addTarget(self, action: #selector(selectedDateChanged), for: .editingDidEnd)
        picker.translatesAutoresizingMaskIntoConstraints = false
        
        return picker
    }()
    
    override func build() {
        super.build()
        
        addSubview(outlineView)
        outlineView.addSubview(fillView)
        addSubview(titleLabel)
        
        addSubview(pickerContainer)
        pickerContainer.addSubview(pickerTitleLabel)
        pickerContainer.addSubview(datePicker)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            outlineView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            outlineView.topAnchor.constraint(equalTo: self.topAnchor),
            outlineView.heightAnchor.constraint(equalToConstant: 24.scaled),
            outlineView.widthAnchor.constraint(equalToConstant: 24.scaled),
        ])
        
        NSLayoutConstraint.activate([
            fillView.centerXAnchor.constraint(equalTo: outlineView.centerXAnchor),
            fillView.centerYAnchor.constraint(equalTo: outlineView.centerYAnchor),
            fillView.heightAnchor.constraint(equalToConstant: 12.scaled),
            fillView.widthAnchor.constraint(equalToConstant: 12.scaled),
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: outlineView.trailingAnchor, constant: 10.scaled),
            titleLabel.centerYAnchor.constraint(equalTo: outlineView.centerYAnchor),
        ])
        
        NSLayoutConstraint.activate([
            pickerContainer.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            pickerContainer.trailingAnchor.constraint(lessThanOrEqualTo: self.trailingAnchor),
            pickerContainer.topAnchor.constraint(equalTo: outlineView.bottomAnchor, constant: 10.scaled),
            pickerContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            pickerTitleLabel.topAnchor.constraint(equalTo: pickerContainer.topAnchor),
            pickerTitleLabel.leadingAnchor.constraint(equalTo: pickerContainer.leadingAnchor),
            pickerTitleLabel.trailingAnchor.constraint(lessThanOrEqualTo: pickerContainer.trailingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            datePicker.topAnchor.constraint(equalTo: pickerTitleLabel.bottomAnchor, constant: 5.scaled),
            datePicker.leadingAnchor.constraint(equalTo: pickerContainer.leadingAnchor),
            datePicker.trailingAnchor.constraint(equalTo: pickerContainer.trailingAnchor),
            datePicker.bottomAnchor.constraint(equalTo: pickerContainer.bottomAnchor),
        ])
        
    }
    
    @objc private func touchAction() {
        guard !isSelected else { return }
        
        if useHapticFeedback { Haptic.light() }
        isSelected = true
        
        let date = datePicker.date
        
        action?(date)
    }
    
    @objc func selectedDateChanged(_ sender: UIDatePicker) {
        let date = sender.date
        action?(date)
    }
}
