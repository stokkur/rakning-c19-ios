//
//  LinkLabel.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class LinkLabel: UILabel, Themeable {
    let theme: Theme
    var action: (() -> ())?
    var useHapticFeedback = true
    var underlined: Bool

    // MARK: - Init
    required init(theme: Theme, underlined: Bool = false) {
        self.theme = theme
        self.underlined = underlined
        super.init(frame: .zero)
        
        self.textColor = theme.colors.darkOrange
        self.isUserInteractionEnabled = true
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(touchAction))
        gestureRecognizer.cancelsTouchesInView = false
        self.addGestureRecognizer(gestureRecognizer)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var text: String? {
        didSet{
            guard let text = text, underlined else { return }
            let textRange = NSRange(location: 0, length: text.count)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
            }
    }
    @objc private func touchAction() {
        if useHapticFeedback { Haptic.light() }
        action?()
    }
}
