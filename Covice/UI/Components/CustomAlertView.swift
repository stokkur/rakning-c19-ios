//
//  CustomAlertView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol CustomAlertViewDelegate {
    func didDismissView( _ customAlertView: CustomAlertView)
}

class CustomAlertView: View {
        
    lazy var containerView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 0
        
        let background = UIView(frame: view.bounds)
        background.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        background.backgroundColor = .white
        background.layer.borderWidth = 1
        background.layer.cornerRadius = 10.scaled
        background.layer.borderColor = UIColor.clear.cgColor
        background.layer.shadowColor = UIColor.black.cgColor
        background.layer.shadowOpacity = 0.4
        background.layer.shadowOffset = .init(width: 5.scaled, height: 5.scaled)
        background.layer.shadowRadius = 10.scaled
        view.addSubview(background)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var bodyTitle: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.darkGray
        view.font = .systemFont(ofSize: 22.scaled, weight: .semibold)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var bodyContent: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.darkGray
        view.font = .systemFont(ofSize: 18.scaled, weight: .regular)
        view.textAlignment = .center
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var actionsView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.isLayoutMarginsRelativeArrangement = true
        view.layoutMargins = UIEdgeInsets(top: 30.scaled, left: 30.scaled, bottom: 30.scaled, right: 30.scaled)
        view.backgroundColor = .clear
        view.spacing = 15.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var actions: [Button] = []
    var delegate: CustomAlertViewDelegate?
    
    required init(theme: Theme, title: String?, content: String?) {
        super.init(theme: theme)
        bodyTitle.text = title
        bodyContent.text = content
    }
    
    override func build() {
        super.build()
        
        addSubview(containerView)
        
        containerView.addArrangedSubview(bodyView)
        
        bodyView.addSubview(bodyTitle)
        bodyView.addSubview(bodyContent)
        
        containerView.addArrangedSubview(actionsView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            containerView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40.scaled),
            containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40.scaled)
        ])
        
        NSLayoutConstraint.activate([
            bodyView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            bodyView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            bodyTitle.topAnchor.constraint(equalTo: bodyView.topAnchor, constant: 25.scaled),
            bodyTitle.centerXAnchor.constraint(equalTo: bodyView.centerXAnchor),
            bodyTitle.trailingAnchor.constraint(equalTo: bodyView.trailingAnchor, constant: -40.scaled),
            bodyTitle.leadingAnchor.constraint(equalTo: bodyView.leadingAnchor, constant: 40.scaled),
        ])
        
        NSLayoutConstraint.activate([
            bodyContent.topAnchor.constraint(equalTo: bodyTitle.bottomAnchor, constant: 20.scaled),
            bodyContent.leadingAnchor.constraint(equalTo: bodyView.leadingAnchor, constant: 30.scaled),
            bodyContent.trailingAnchor.constraint(equalTo: bodyView.trailingAnchor, constant: -30.scaled),
            bodyContent.bottomAnchor.constraint(equalTo: bodyView.bottomAnchor, constant: -30.scaled),
            bodyContent.heightAnchor.constraint(lessThanOrEqualToConstant: 400.scaled)
        ])
        
        NSLayoutConstraint.activate([
            actionsView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            actionsView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
        ])
        
    }
    
    func addAction(button: Button, dismissOnTouch: Bool){
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = true
        if dismissOnTouch {
            button.secondAction = { [weak self] in
                guard let this = self else {
                    return
                }
                this.delegate?.didDismissView(this)
            }
        }
        actions = actions + [button]
        actionsView.addArrangedSubview(button)
    }
    
}

