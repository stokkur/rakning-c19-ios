//
//  View.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

open class View: UIStackView, Themeable {
    public let theme: Theme
    var backgroundView: UIView?
    
    // MARK: - Init
    init(theme: Theme, _ setDefaultBackground: Bool = true) {
        self.theme = theme
        super.init(frame: .zero)
        if setDefaultBackground {
            setBackground(color: theme.colors.lightOrange)
        }
        configure()
    }

    @available(*, unavailable, message: "Use `init(theme:)`")
    init() {
        fatalError("Not Supported")
    }

    @available(*, unavailable, message: "Use `init(theme:)`")
    override public init(frame: CGRect) {
        fatalError("Not Supported")
    }
    
    @available(*, unavailable, message: "NSCoder and Interface Builder is not supported. Use Programmatic layout.")
    required public init(coder: NSCoder) {
        fatalError("Not Supported")
    }
    
    // MARK: - Internal
    func configure() {
        build()
        setupConstraints()
    }

    open func build() {}

    open func setupConstraints() {}
    
    internal func setBackground(color: UIColor) {
        if self.backgroundView == nil {
            self.backgroundView = UIView(frame: bounds)
            self.backgroundView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            insertSubview(self.backgroundView!, at: 0)
        }
        self.backgroundView!.backgroundColor = color
    }
}



