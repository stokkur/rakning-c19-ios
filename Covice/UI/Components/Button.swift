//
//  Button.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class Button: UIButton, Themeable {

    enum ButtonType {
        case primary
        case info
        case ghost
    }

    var style = ButtonType.primary {
        didSet {
            updateButtonType()
        }
    }

    var rounded = false {
        didSet {
            updateRoundedCorners()
        }
    }

    var title = "" {
        didSet {
            self.setTitle(title, for: .normal)
        }
    }

    override var isEnabled: Bool {
        didSet {
            let enabledAlpha: CGFloat = isEnabled ? 1 : 0.5
            
            backgroundColor = backgroundColor?.withAlphaComponent(enabledAlpha)
            layer.borderColor = backgroundColor?.withAlphaComponent(enabledAlpha).cgColor
        }
    }
    
    var isLoading: Bool = false {
        didSet {
            let loadingAlpha: CGFloat = isLoading ? 0 : 1
            
            backgroundColor = backgroundColor?.withAlphaComponent(loadingAlpha)
            titleLabel?.alpha = loadingAlpha
            
            if isLoading {
                loadingIndicator.startAnimating()
            } else {
                loadingIndicator.stopAnimating()
            }
        }
    }

    let theme: Theme
    var action: (() -> ())?
    var secondAction: (() -> ())?
    var useHapticFeedback = true
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        if #available(iOS 13.0, *) {
            view.style = .large
        } else {
            view.style = .whiteLarge
        }
        view.color = theme.colors.darkBlue
        view.translatesAutoresizingMaskIntoConstraints = false
    
        return view
    }()

    // MARK: - Init
    required init(title: String = "", theme: Theme) {
        self.theme = theme
        super.init(frame: .zero)

        self.setTitle(title, for: .normal)
        self.titleLabel?.font = .systemFont(ofSize: 24.scaled, weight: .bold)
        self.titleLabel?.adjustsFontSizeToFitWidth = true

        self.layer.cornerRadius = 10.scaled
        self.clipsToBounds = true
        
        self.contentEdgeInsets = .init(top: 15.scaled, left: 30.scaled, bottom: 15.scaled, right: 30.scaled)

        self.addTarget(self, action: #selector(self.touchUpAnimation), for: .touchDragExit)
        self.addTarget(self, action: #selector(self.touchUpAnimation), for: .touchCancel)
        self.addTarget(self, action: #selector(self.touchUpAnimation), for: .touchUpInside)
        self.addTarget(self, action: #selector(self.touchDownAnimation), for: .touchDown)
        self.addTarget(self, action: #selector(self.touchUpAction), for: .touchUpInside)
        
        configure()
        
        updateButtonType()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Internal
    func configure() {
        build()
        setupConstraints()
    }

    func build() {
        addSubview(loadingIndicator)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            loadingIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            loadingIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor),
        ])
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()
        updateRoundedCorners()
    }

    // MARK: - Private

    private func updateButtonType() {
        switch style {
        case .primary:
            backgroundColor = theme.colors.darkOrange
            setTitleColor(theme.colors.white, for: .normal)
        case .info:
            backgroundColor = .clear
            setTitleColor(theme.colors.darkBlue, for: .normal)
        case .ghost:
            backgroundColor = .clear
            layer.borderWidth = 1
            setTitleColor(theme.colors.darkOrange, for: .normal)
            layer.borderColor = theme.colors.darkOrange.cgColor
        }

        tintColor = theme.colors.white
    }

    private func updateRoundedCorners() {
        if rounded {
            layer.cornerRadius = min(bounds.width, bounds.height) / 2
        }
    }

    @objc private func touchDownAnimation() {

        if useHapticFeedback { Haptic.light() }

        UIButton.animate(withDuration: 0.2, animations: {
            self.transform = CGAffineTransform(scaleX: 0.98, y: 0.98)
        })
    }

    @objc private func touchUpAnimation() {
        UIButton.animate(withDuration: 0.2, animations: {
            self.transform = CGAffineTransform.identity
        })
    }

    @objc private func touchUpAction() {
        guard isEnabled && !isLoading else { return }
        
        action?()
        secondAction?()
    }
}
