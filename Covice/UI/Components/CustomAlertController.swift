//
//  CustomAlertController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

protocol CustomAlertControllerDelegate {
    func didDismissView(_ customAlertController: CustomAlertController)
}

class CustomAlertController: ViewController {
    var delegate: CustomAlertControllerDelegate?
    
    var alertView: CustomAlertView
    
    required init(theme: Theme, title: String?, content: String?) {
        self.alertView = CustomAlertView(theme: theme, title: title, content: content)
        super.init(theme: theme)
        self.delegate = self
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    
    override func loadView() {
        alertView.delegate = self
        
        self.internalView = alertView
        self.alertView.backgroundView?.backgroundColor = #colorLiteral(red: 0.3409830049, green: 0.3198650779, blue: 0.3134161628, alpha: 0.75)
        
        self.internalView?.isOpaque = false
    }
    
    func addAction(button: Button, dismissOnAction: Bool = true ) {
        alertView.addAction(button: button, dismissOnTouch: dismissOnAction)
    }
}

extension CustomAlertController: CustomAlertViewDelegate {
    func didDismissView(_ customAlertView: CustomAlertView) {
        delegate?.didDismissView(self)
    }
}

extension CustomAlertController: CustomAlertControllerDelegate {
    func didDismissView(_ customAlertController: CustomAlertController) {
        customAlertController.dismiss(animated: true)
    }
}
