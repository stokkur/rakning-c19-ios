//
//  ViewController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class ViewController: UIViewController, Themeable {
    public let theme: Theme
    
    override var title: String? {
        didSet {
            let label = UILabel()
            label.font = .systemFont(ofSize: 20.scaled, weight: .bold)
            label.textColor = theme.colors.darkBlue
            label.textAlignment = .center
            label.text = title
            label.adjustsFontSizeToFitWidth = true
            
            navigationItem.titleView = label
        }
    }
    
    public var internalView: UIView? {
        didSet {
            self.view = internalView
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    public init(theme: Theme, relocalizable: Bool = false) {
        self.theme = theme
        super.init(nibName: nil, bundle: nil)
        if relocalizable {
            self.enableRelocalization()
        }
    }

    @available(*, unavailable, message: "NSCoder and Interface Builder is not supported. Use Programmatic layout.")
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override open func viewDidLoad() {
        super.viewDidLoad()
    
        edgesForExtendedLayout = []
    }

    override open func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()

        setBottomMargin()
    }

    // MARK: - Private

    private func setBottomMargin() {
        view.layoutMargins.bottom = view.safeAreaInsets.bottom == 0 ? 20.scaled : 0
    }
    
    public func showError(_ error: LocalizedError) {
        self.presentAlert(title: NSLocalizedString("ERROR", comment: "Title"), content: error.errorDescription)
    }
    
    func presentAlert(title: String, content: String?){
        let alert = CustomAlertController(theme: theme, title: title, content: content)
        let cancel = Button(title: Localization.btn_done, theme: theme)
        cancel.style = .ghost
        alert.addAction(button: cancel)
        self.present(alert, animated: true)
    }
    
    func relocalizeContent(){
        print("LocalizeContent method hasn't been implemented.")
    }
    
    @objc func relocalizationNotificationRecieved(){
        self.relocalizeContent()
    }
    
    private func enableRelocalization(){
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(relocalizationNotificationRecieved),
                                               name: Bundle.languageChangedNotificatioName,
                                               object: nil)
    }
}
