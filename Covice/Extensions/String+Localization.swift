//
//  String+Localization.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


extension String {
    var localized: String {
        let bundle = Bundle.localizeBundle() ?? Bundle.main
        return NSLocalizedString(self, tableName: nil, bundle: bundle, comment: self)
    }
}

extension Bundle {
    private static var bundle: Bundle?
    public static let languageChangedNotificatioName: NSNotification.Name = NSNotification.Name(rawValue: Bundle.main.bundleIdentifier! + ".languageChanged")
    
    public static func setLanguage(lang: String) {
        if #available(iOS 13, *){
            UserDefaults.standard.set([lang], forKey: "AppleLanguages")
        } else {
            UserDefaults.standard.set(lang,forKey: "appLang")
        }
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
        NotificationCenter.default.post(name: languageChangedNotificatioName, object: nil)
    }
    
    public static func localizeBundle() -> Bundle? {
        if bundle == nil {
            let appLangCode = getSelectedAppLangCode()
            let path = Bundle.main.path(forResource: appLangCode, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
        return bundle
    }
    
    public static func getSelectedAppLangCode() -> String {
        if #available(iOS 13, *){
            guard let languages = UserDefaults.standard.stringArray(forKey: "AppleLanguages"),
                  let fullLanguage = languages.first,
                  fullLanguage.count >= 2 else {
                return "is"
            }
            return SupportedLanguage.mapLanguageCode(String(fullLanguage.prefix(2))).code
        } else {
            return UserDefaults.standard.string(forKey: "appLang") ?? "is"
        }
    }
    
    enum SupportedLanguage: Int, CaseIterable {
        case english = 0
        case icelandic
        case polish
        
        var code: String {
            switch self {
            case .english: return "en"
            case .icelandic: return "is"
            case .polish: return "pl-PL"
            }
        }
        
        static func mapLanguageCode(_ code: String) -> Bundle.SupportedLanguage {
            for suportedLanguage in Bundle.SupportedLanguage.allCases {
                let prefixCode = String(code.prefix(2))
                if String(suportedLanguage.code.prefix(2)) == prefixCode {
                    return suportedLanguage
                }
            }
            return .icelandic
        }
    }
}


class Localization {
    // MARK: - APP DISTRIBUTION SCREEN
    static var update_version_title: String { return "update_version_title".localized }
    static var upadte_version_body: String { return "upadte_version_body".localized }
    static var update_button: String { return "update_button".localized }
    
    // MARK: - COMMON SCREENS
    static var btn_consent: String { return "btn_consent".localized }
    static var btn_continue: String { return "btn_continue".localized }
    static var btn_got_it: String { return "btn_got_it".localized }
    static var btn_turn_on: String { return "btn_turn_on".localized }
    static var btn_turn_off: String { return "btn_turn_off".localized }
    static var btn_not_now: String { return "btn_not_now".localized }
    static var btn_cancel: String { return "btn_cancel".localized }
    static var btn_delete_status: String { return "btn_delete_status".localized }
    static var btn_delete: String { return "btn_delete".localized }
    static var btn_share_this_app: String { return "btn_share_this_app".localized }
    static var btn_no_go_back: String { return "btn_no_go_back".localized }
    static var btn_yes_continue: String { return "btn_yes_continue".localized }
    static var btn_share_positive: String { return "btn_share_positive".localized }
    static var btn_done: String { return "btn_done".localized }
    static var btn_back: String { return "btn_back".localized }
    static var btn_save: String { return "btn_save".localized }
    static var btn_discard: String { return "btn_discard".localized }
    static var btn_verify: String { return "btn_verify".localized }
    static var learn_more: String { return "learn_more".localized }
    static var home_tab_exposures_text: String { return "home_tab_exposures_text".localized }
    static var home_tab_notify_text: String { return "home_tab_notify_text".localized }
    static var home_tab_settings_text: String { return "home_tab_settings_text".localized }
    static var notification_channel_name: String { return "notification_channel_name".localized }
    static var notification_channel_description: String { return "notification_channel_description".localized }
    static var navigate_up: String { return "navigate_up".localized }
    
    static var turn_on_exposure_notification_title: String { return "turn_on_exposure_notification_title".localized }
    static var turn_on_exposure_notification_detail: String { return "turn_on_exposure_notification_detail".localized }
    static var language: String { return "language".localized }
    static var language_english: String { return "language_english".localized }
    static var language_icelandic: String { return "language_icelandic".localized }
    static var language_polish: String { return "language_Polish".localized }
    
    
    static var notification_exposure_title: String { return "notification_exposure_title".localized }
    static var notification_exposure_body: String { return "notification_exposure_body".localized }
    static var alert_exposure_title: String { return "alert_exposure_title".localized }
    static var alert_exposure_body: String { return "alert_exposure_body".localized }
    static var notification_bluetooth_off_title: String { return "notification_bluetooth_off_title".localized }
    static var notification_bluetooth_off_body: String { return "notification_bluetooth_off_body".localized }
    static var alert_bluetooth_off_title: String { return "alert_bluetooth_off_title".localized }
    static var alert_bluetooth_off_body: String { return "alert_bluetooth_off_body".localized }

    // MARK: - ONBOARDING SCREENS
    static var onboarding_opt_in_title: String { return "onboarding_opt_in_title".localized }
    static var onboarding_finish_title: String { return "onboarding_finish_title".localized }
    static var onboarding_exposure_notifications_title: String { return "onboarding_exposure_notifications_title".localized }
    static var onboarding_exposure_notifications_detail: String { return "onboarding_exposure_notifications_detail".localized }
    static var onboarding_metrics_title: String { return "onboarding_metrics_title".localized }
    static var onboarding_metrics_subtitle: String { return "onboarding_metrics_subtitle".localized }
    static var onboarding_metrics_detail: String { return "onboarding_metrics_detail".localized }
    static var onboarding_confirm_later_title: String { return "onboarding_confirm_later_title".localized }
    static var onboarding_confirm_later_detail: String { return "onboarding_confirm_later_detail".localized }
    static var onboarding_exposure_bluetooth_title: String { return "onboarding_exposure_bluetooth_title".localized }
    static var onboarding_exposure_bluetooth_detail: String { return "onboarding_exposure_bluetooth_detail".localized }
    static var onboarding_exposure_info_title: String { return "onboarding_exposure_info_title".localized }
    static var onboarding_exposure_info_sub_title: String { return "onboarding_exposure_info_sub_title".localized }
    static var onboarding_exposure_info_detail: String { return "onboarding_exposure_info_detail".localized }
    static var onboarding_help_title: String { return "onboarding_help_title".localized }
    static var onboarding_help_detail: String { return "onboarding_help_detail".localized }
    static var onboarding_notification_title: String { return "onboarding_notification_title".localized }
    static var onboarding_notification_detail: String { return "onboarding_notification_detail".localized }
    static var onboarding_bluetooth_title: String { return "onboarding_bluetooth_title".localized }
    static var onboarding_bluetooth_detail: String { return "onboarding_bluetooth_detail".localized }
    static var onboarding_short_exposure_title: String { return "onboarding_short_exposure_title".localized }
    static var onboarding_short_exposure_detail: String { return "onboarding_short_exposure_detail".localized }
    static var onboarding_long_exposure_title: String { return "onboarding_long_exposure_title".localized }
    static var onboarding_long_exposure_detail: String { return "onboarding_long_exposure_detail".localized }
    static var onboarding_travel_title: String { return "onboarding_travel_title".localized }
    static var onboarding_travel_detail: String { return "onboarding_travel_detail".localized }
    static var onboarding_travel_add_phone: String { return "onboarding_travel_add_phone".localized }
    static var onboarding_travel_skip: String { return "onboarding_travel_skip".localized }
    static var onboarding_add_phone_title: String { return "onboarding_add_phone_title".localized }
    static var onboarding_add_phone_detail: String { return "onboarding_add_phone_detail".localized }
    static var onboarding_add_phone_placeholder: String { return "onboarding_add_phone_placeholder".localized }
    static var onboarding_verify_code_title: String { return "onboarding_verify_code_title".localized }
    static var onboarding_verify_code_detail: String { return "onboarding_verify_code_detail".localized }
    static var onboarding_verify_code_send_again: String { return "onboarding_verify_code_send_again".localized }
    static var onboarding_verify_code_placeholder: String { return "onboarding_verify_code_placeholder".localized }
    static var onboarding_consent_title: String { "onboarding_consent_title".localized }
    static var onboarding_consent_bold_1: String { return "onboarding_consent_bold_1".localized }
    static var onboarding_consent_subtitle_1: String { return "onboarding_consent_subtitle_1".localized }
    static var onboarding_consent_subtitle_2: String { return "onboarding_consent_subtitle_2".localized }
    static var onboarding_consent_subtitle_3: String { return "onboarding_consent_subtitle_3".localized }
    static var onboarding_consent_subtitle_4: String { return "onboarding_consent_subtitle_4".localized }
    static var onboarding_consent_subtitle_5: String { return "onboarding_consent_subtitle_5".localized }
    static var onboarding_consent_subtitle_6: String { return "onboarding_consent_subtitle_6".localized }
    static var onboarding_consent_subtitle_7: String { return "onboarding_consent_subtitle_7".localized }
    static var onboarding_consent_link_1: String { return "onboarding_consent_link_1".localized }
    static var onboarding_consent_link_2: String { return "onboarding_consent_link_2".localized }
    static var onboarding_consent_body_1: String { return "onboarding_consent_body_1".localized }
    static var onboarding_consent_body_2: String { return "onboarding_consent_body_2".localized }
    static var onboarding_consent_body_3: String { return "onboarding_consent_body_3".localized }
    static var onboarding_consent_body_4: String { return "onboarding_consent_body_4".localized }
    static var onboarding_consent_body_5: String { return "onboarding_consent_body_5".localized }
    static var onboarding_consent_body_6: String { return "onboarding_consent_body_6".localized }
    static var onboarding_consent_body_7: String { return "onboarding_consent_body_7".localized }
    static var onboarding_consent_body_8: String { return "onboarding_consent_body_8".localized }
    static var onboarding_consent_body_9: String { return "onboarding_consent_body_9".localized }
    static var onboarding_consent_body_10: String { return "onboarding_consent_body_10".localized }
    static var onboarding_consent_body_11: String { return "onboarding_consent_body_11".localized }
    
    static var onboarding_consent_link: String { "onboarding_consent_link".localized }
    static var onboarding_consent_body_after_link: String { "onboarding_consent_body_after_link".localized }
    
    // MARK: - EXPOSURES SCREENS
    static var no_recent_exposure_subtitle: String { return "no_recent_exposure_subtitle".localized }
    static var notifications_enabled_info: String { return "notifications_enabled_info".localized }
    static var exposure_notifications_are_turned_off: String { return "exposure_notifications_are_turned_off".localized }
    static var turn_on_exposure_notifications_action: String { return "turn_on_exposure_notifications_action".localized }
    static var recent_exposures_subtitle: String { return "recent_exposures_subtitle".localized }
    static var exposure_details_status_subtitle: String { return "exposure_details_status_subtitle".localized }
    static var exposure_details_status_exposure: String { return "exposure_details_status_exposure".localized }
    static var exposure_details_date_exposed_subtitle: String { return "exposure_details_date_exposed_subtitle".localized }
    static var exposure_details_expiring_subtitle: String { return "exposure_details_expiring_subtitle".localized }
    static var exposure_details_learn_more: String { return "exposure_details_learn_more".localized }
    static var exposure_details_new_badge: String { return "exposure_details_new_badge".localized }
    static var exposure_details_next_steps: String { return "exposure_details_next_steps".localized }
    static var exposure_details_click_here: String { return "exposure_details_click_here".localized }
    static var exposure_details_next_steps_link: String { return "exposure_details_next_steps_link".localized }
    
    // MARK: - NOTIFY OTHERS SCREENS
    static var share_test_result: String { return "share_test_result".localized }
    static var share_test_result_title: String { return "share_test_result_title".localized }
    static var share_test_result_description: String { return "share_test_result_description".localized }
    static var test_result_subtitle: String { return "test_result_subtitle".localized }
    static var test_result_type_confirmed: String { return "test_result_type_confirmed".localized }
    static var test_result_type_likely: String { return "test_result_type_likely".localized }
    static var test_result_type_negative: String { return "test_result_type_negative".localized }
    static var positive_test_result_status_shared: String { return "positive_test_result_status_shared".localized }
    static var positive_test_result_status_not_shared: String { return "positive_test_result_status_not_shared".localized }
    static var notify_turn_on_exposure_notifications_header: String { return "notify_turn_on_exposure_notifications_header".localized }
    static var share_begin_title: String { return "share_begin_title".localized }
    static var share_close_title: String { return "share_close_title".localized }
    static var share_close_detail: String { return "share_close_detail".localized }
    static var btn_resume_later: String { return "btn_resume_later".localized }
    static var verify_test_result_title: String { return "verify_test_result_title".localized }
    static var enter_your_test_identifier: String { return "enter_your_test_identifier".localized }
    static var share_test_identifier_help: String { return "share_test_identifier_help".localized }
    static var share_test_identifier_verified: String { return "share_test_identifier_verified".localized }
    static var share_test_identifier_link: String { return "share_test_identifier_link".localized }
    static var share_onset_title: String { return "share_onset_title".localized }
    static var share_onset_subtitle: String { return "share_onset_subtitle".localized }
    static var test_date_label: String { return "test_date_label".localized }
    static var share_onset_symptoms: String { return "share_onset_symptoms".localized }
    static var share_onset_no_symptoms: String { return "share_onset_no_symptoms".localized }
    static var share_onset_no_answer: String { return "share_onset_no_answer".localized }
    static var share_travel_title: String { return "share_travel_title".localized }
    static var share_travel_confirmed: String { return "share_travel_confirmed".localized }
    static var share_travel_no_travel: String { return "share_travel_no_travel".localized }
    static var share_travel_no_answer: String { return "share_travel_no_answer".localized }
    static var share_review_title: String { return "share_review_title".localized }
    static var share_review_status_subtitle: String { return "share_review_status_subtitle".localized }
    static var share_review_status_confirmed: String { return "share_review_status_confirmed".localized }
    static var share_review_status_likely: String { return "share_review_status_likely".localized }
    static var share_review_status_negative: String { return "share_review_status_negative".localized }
    static var share_review_onset_subtitle: String { return "share_review_onset_subtitle".localized }
    static var share_review_onset_date: String { return "share_review_onset_date".localized }
    static var share_review_onset_no_symptoms: String { return "share_review_onset_no_symptoms".localized }
    static var share_review_onset_no_answer: String { return "share_review_onset_no_answer".localized }
    static var share_review_travel_subtitle: String { return "share_review_travel_subtitle".localized }
    static var share_review_travel_confirmed: String { return "share_review_travel_confirmed".localized }
    static var share_review_travel_no_travel: String { return "share_review_travel_no_travel".localized }
    static var share_review_travel_no_answer: String { return "share_review_travel_no_answer".localized }
    static var share_confirm_title: String { return "share_confirm_title".localized }
    static var not_shared_confirm_title: String { return "not_shared_confirm_title".localized }
    static var not_shared_confirm_detail: String { return "not_shared_confirm_detail".localized }
    static var status_shared_detail_title: String { return "status_shared_detail_title".localized }
    static var delete_test_result_title: String { return "delete_test_result_title".localized }
    static var delete_test_result_detail: String { return "delete_test_result_detail".localized }
    static var delete_test_result_confirmed: String { return "delete_test_result_confirmed".localized }
    static var share_error_no_internet: String { return "share_error_no_internet".localized }
    static var notify_verify_code_failed: String { return "notify_verify_code_failed".localized }
    
    // MARK: - SETTINGS SCREENS
    static var settings_exposure_notifications_subtitle: String { return "settings_exposure_notifications_subtitle".localized }
    static var settings_app_analytics_subtitle: String { return "settings_app_analytics_subtitle".localized }
    static var settings_privacy_policy: String { return "settings_privacy_policy".localized }
    static var settings_legal_terms: String { return "settings_legal_terms".localized }
    static var settings_debug: String { return "settings_debug".localized }
    static var settings_share: String { return "settings_share".localized }
    static var settings_share_message: String { return "settings_share_message".localized }
    static var settings_open_source: String { return "settings_open_source".localized }
    static var settings_exposure_notifications_on: String { return "settings_exposure_notifications_on".localized }
    static var settings_exposure_notifications_off: String { return "settings_exposure_notifications_off".localized }
    static var settings_analytics_on: String { return "settings_analytics_on".localized }
    static var settings_analytics_off: String { return "settings_analytics_off".localized }
    static var exposure_about_title: String { return "exposure_about_title".localized }
    static var exposure_about_toggle: String { return "exposure_about_toggle".localized }
    static var exposure_about_detail: String { return "exposure_about_detail".localized }
    static var exposure_turn_off_title: String { return "exposure_turn_off_title".localized }
    static var exposure_turn_off_detail: String { return "exposure_turn_off_detail".localized }
    static var app_analytics_title: String { return "app_analytics_title".localized }
    static var app_analytics_toggle: String { return "app_analytics_toggle".localized }
    static var app_analytics_detail: String { return "app_analytics_detail".localized }
    static var agency_message_link: String { return "agency_message_link".localized }
    
    static var settings_enter_phone_number: String { return "settings_enter_phone_number".localized}
    static var settings_directorate_of_health: String { return "settings_directorate_of_health".localized }
    static var settings_phone_off: String { return "settings_phone_off".localized }
    static var settings_country_name: String { return "settings_country_name".localized }
    static var settings_directorate_message: String { return "settings_directorate_message".localized }
    static var settings_legal_terms_message: String { return "settings_legal_terms_message".localized }
    static var settings_licenses_app_version: String { return "settings_licenses_app_version".localized }
    static var settings_learn_more: String { return "settings_learn_more".localized }
    static var settings_learn_more_link: String { return "settings_learn_more_link".localized }
    
    
    // MARK: - EDGE CASES
    static var exposure_notifications_are_inactive: String { return "exposure_notifications_are_inactive".localized }
    static var exposure_notifications_currently_inactive: String { return "exposure_notifications_currently_inactive".localized }
    static var device_settings: String { return "device_settings".localized }
    static var manage_storage: String { return "manage_storage".localized }
    static var location_off_warning: String { return "location_off_warning".localized }
    static var ble_off_warning: String { return "ble_off_warning".localized }
    static var location_ble_off_warning: String { return "location_ble_off_warning".localized }
    static var storage_low_warning: String { return "storage_low_warning".localized }
    
    // MARK: - ERROR MESSAGE
    static var input_error_onset_date_past: String { return "input_error_onset_date_past".localized }
    static var input_error_onset_date_future: String { return "input_error_onset_date_future".localized }
    static var network_error_code_invalid: String { return "network_error_code_invalid".localized }
    static var network_error_code_expired: String { return "network_error_code_expired".localized }
    static var network_error_server_error: String { return "network_error_server_error".localized }
    static var network_error_unsupported_test_type: String { return "network_error_unsupported_test_type".localized }
    static var share_error: String { return "share_error".localized }
    static var generic_error_message: String { return "generic_error_message".localized }
    static var code_error_already_submitted: String { return "code_error_already_submitted".localized }
    
    // MARK: - ICELANDIC ONBOARDING SCREENS
    static var landlaeknir_user_tutorial: String { return "landlaeknir_user_tutorial".localized }
    static var landlaeknir_tutorial_screen1_header: String { return "landlaeknir_tutorial_screen1_header".localized }
    static var landlaeknir_tutorial_screen1_body: String { return "landlaeknir_tutorial_screen1_body".localized }
    static var landlaeknir_tutorial_screen2_header: String { return "landlaeknir_tutorial_screen2_header".localized }
    static var landlaeknir_tutorial_screen2_body: String { return "landlaeknir_tutorial_screen2_body".localized }
    static var landlaeknir_tutorial_screen3_header: String { return "landlaeknir_tutorial_screen3_header".localized }
    static var landlaeknir_tutorial_screen3_body: String { return "landlaeknir_tutorial_screen3_body".localized }
    static var landlaeknir_tutorial_screen4_header: String { return "landlaeknir_tutorial_screen4_header".localized }
    static var landlaeknir_tutorial_screen4_body: String { return "landlaeknir_tutorial_screen4_body".localized }
    static var landlaeknir_tutorial_screen5_header: String { return "landlaeknir_tutorial_screen5_header".localized }
    static var landlaeknir_tutorial_screen5_body: String { return "landlaeknir_tutorial_screen5_body".localized }
    
    // MARK: - ADDITIONAL
    static var phone_register_main_text: String { return "phone_register_main_text".localized }
    static var phone_number_fragment_offline_warning: String { return "phone_number_fragment_offline_warning".localized }
    static var skip_phone_registration: String { return "skip_phone_registration".localized }
    static var display_test_results: String { return "display_test_results".localized }
    static var phone_registration_header: String { return "phone_registration_header".localized }
    static var border_test_results: String { return "border_test_results".localized }
    static var test_results_negative: String { return "test_results_negative".localized }
    static var exposure_details_body_text: String { return "exposure_details_body_text".localized }
    static var notification_body: String { return "notification_body".localized }
    static var notification_subject: String { return "notification_subject".localized }
    static var symptoms_onset_description: String { return "symptoms_onset_description".localized }
    static var agency_message: String { return "agency_message".localized }
    static var agency_display_name: String { return "agency_display_name".localized }
    static var classification_name: String { return "classification_name".localized }
    static var agency_privacy_policy: String { return "agency_privacy_policy".localized }
    static var traveled_question_text: String { return "traveled_question_text".localized }
    static var test_verification_intro_message: String { return "test_verification_intro_message".localized }
    static var revoked_details_body_text: String { return "revoked_details_body_text".localized }
    static var revoked_notification_body: String { return "revoked_notification_body".localized }
    static var revoked_notification_subject: String { return "revoked_notification_subject".localized }
    static var symptoms_start_date: String { "symptoms_start_date".localized }
 
    // MARK: - ERROR MESSAGES
    static var error_phone_registration_title: String { return "error_phone_registration_title".localized }
    static var error_phone_registration_content: String { return "error_phone_registration_content".localized }
    static var error_phone_verification_title: String { return "error_phone_verification_title".localized }
    static var error_phone_verification_content: String { return "error_phone_verification_content".localized }
    static var not_autorized_error_title: String { return "not_autorized_error_title".localized }
    static var not_autorized_error_content: String { return "not_autorized_error_content".localized }
    
    // MARK: - Phone re-registation
    static var phone_re_registration_title: String { return "phone_re_registration_title".localized }
    static var phone_re_registration_body: String { return "phone_re_registration_body".localized }
}
