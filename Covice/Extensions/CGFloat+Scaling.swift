//
//  CGFloat+Scaling.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

extension CGFloat {
    var scaled: CGFloat {
        // Design uses 12 Pro Max as base
        let baseHeight: CGFloat = 926.0
        let baseWidth: CGFloat = 428.0
        
        let heightFactor: CGFloat = UIScreen.main.bounds.height / baseHeight
        let widthFactor: CGFloat = UIScreen.main.bounds.width / baseWidth
        
        let factor = (heightFactor + widthFactor) / 2
        
        return self * factor
    }
}

extension Int {
    var scaled: CGFloat {
        return CGFloat(self).scaled
    }
}

extension Double {
    var scaled: CGFloat {
        return CGFloat(self).scaled
    }
}
