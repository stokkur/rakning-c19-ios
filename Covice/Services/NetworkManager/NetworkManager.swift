//
//  NetworkManager.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine
import ExposureNotification
import os

enum NetworkError: LocalizedError {
    case invalidRequest
    case serverNotReachable
    case invalidResponse
    case responseCached
    case serverError
    case resourceNotFound
    case encodingError
    case redirection
}

extension NetworkError {
    var errorDescription: String? {
        switch self {
        case .invalidRequest: return "Invalid Request Error"
        case .serverNotReachable: return "Server Not Reachable Error"
        case .invalidResponse: return "Invalid Response"
        case .responseCached: return "Response Cached"
        case .serverError: return "Server Error"
        case .resourceNotFound: return "Resource Not Found"
        case .encodingError: return "Encoding Error"
        case .redirection: return "Redirection Error"
        }
    }
}

enum NetworkResponseHandleError: Error {
    case cannotUnzip
    case invalidSignature
    case cannotDeserialize
    case deinitError
}

extension NetworkResponseHandleError {
    var asNetworkError: NetworkError {
        switch self {
        case .cannotDeserialize:
            return .invalidResponse
        case .cannotUnzip:
            return .invalidResponse
        case .invalidSignature:
            return .invalidResponse
        case .deinitError:
            return .invalidResponse
        }
    }
}

class NetworkManager {
    private let configuration: NetworkConfiguration
    private let session: URLSession
    private var disposeBag = Set<AnyHashable>()
    private let storageManager: StorageManager
    private let phoneRegistrationQueue = DispatchQueue( label: "PhoneRegistration",
                                          qos: .userInitiated,
                                          attributes: .concurrent)
    
    private lazy var jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromUpperCamelCase

        return decoder
    }()

    private lazy var jsonEncoder = JSONEncoder()
    
    init(configuration: NetworkConfiguration, session: URLSession, storageManager: StorageManager) {
        self.configuration = configuration
        self.session = session
        self.storageManager = storageManager
    }
    
    // MARK: - Methods
    func getPinNumber(request: PinNumberRequest, completion: @escaping (Result<PinNumberResponse,NetworkError>) -> Void ){
        os_log(.debug, "Start getting pin number")
        let constructedRequest = constructRequest(url: configuration.requestPinUrl, method: .POST, body: request, headers: [:])
        switch constructedRequest {
        case .success(let urlRequest):
            data(request: urlRequest) { [weak self] (result) in
                guard let this = self else {
                    completion(.failure(.serverNotReachable))
                    return
                }
                switch result {
                case .failure(let networkError):
                    completion(.failure(networkError))
                case .success(let (_,data)):
                    this.decodeJson(data: data, of: PinNumberResponse.self) { (result) in
                        switch result {
                        case .failure(let responseError):
                            completion(.failure(responseError.asNetworkError))
                        case .success(let pinNumberResponse):
                            completion(.success(pinNumberResponse))
                        }
                    }
                }
            }
        case .failure(let error):
            completion(.failure(error))
        }
    }
    
    @available(iOS 13.0, *)
    func getPinNumber(request: PinNumberRequest) -> AnyPublisher<PinNumberResponse,NetworkError>{
        os_log(.debug, "Start getting pin number")
        let constructedRequest = constructRequest(url: configuration.requestPinUrl, method: .POST, body: request, headers: [:])
        switch constructedRequest {
        case .success(let urlRequest):
            return session.dataTaskPublisher(for: urlRequest)
                .receive(on: phoneRegistrationQueue)
                .map({ (output) -> Data in
                    os_log(.debug, "Request responded with: %{public}s", output.response.debugDescription)
                    return output.data
                })
                .decode(type: PinNumberResponse.self, decoder: jsonDecoder)
                .mapError{ $0 as? NetworkError ?? .invalidResponse }
                .eraseToAnyPublisher()
        case .failure(let error):
            return Fail(error: error).eraseToAnyPublisher()
        }
    }
    
    func verifyPin(request: VerifyPinRequest, completion: @escaping (Result<VerifyPinResponse,NetworkError>) -> Void ) {
        os_log(.debug, "Start verify pin number")
        let headers = [HTTPHeaderKey.authorization: "Bearer \(request.token)"]
        let constructedRequest = constructRequest(url: configuration.pinVerificationUrl, method: .POST, body: request, headers: headers)
        switch constructedRequest {
        case .success(let urlRequest):
            data(request: urlRequest) { [weak self] (result) in
                guard let this = self else {
                    completion(.failure(.serverNotReachable))
                    return
                }
                switch result {
                case .failure(let networkError):
                    completion(.failure(networkError))
                case .success(let (_,data)):
                    this.decodeJson(data: data, of: VerifyPinResponse.self) { (result) in
                        switch result {
                        case .failure(let responseError):
                            completion(.failure(responseError.asNetworkError))
                        case .success(let verifyPinResponse):
                            completion(.success(verifyPinResponse))
                        }
                    }
                }
            }
        case .failure(let error):
            completion(.failure(error))
        }
    }
    @available(iOS 13.0, *)
    func verifyPin(request: VerifyPinRequest) -> AnyPublisher<VerifyPinResponse,NetworkError>{
        os_log(.debug, "Start verify pin number")
        let headers = [HTTPHeaderKey.authorization: "Bearer \(request.token)"]
        let contructionResult = constructRequest(url: configuration.pinVerificationUrl, method: .POST, body: request, headers: headers)
        switch contructionResult {
        case .success(let urlRequest):
            return session.dataTaskPublisher(for: urlRequest)
                .receive(on: phoneRegistrationQueue)
                .map({ (output) -> Data in
                    os_log(.debug, "Request responded with: %{public}s", output.response.debugDescription)
                    return output.data
                })
                .decode(type: VerifyPinResponse.self, decoder: jsonDecoder)
                .mapError{ $0 as? NetworkError ?? .invalidResponse }
                .eraseToAnyPublisher()
        case .failure(let error):
            return Fail(error: error).eraseToAnyPublisher()
        }
    }
    
    func storeFirebaseToken(request: StoreFirebaseTokenRequest, with authToken: String, completion: @escaping (Result<Bool, NetworkError>) -> Void){
        os_log(.debug, "Start store firabase token")
        let headers = [
            HTTPHeaderKey.authorization: "Bearer \(authToken)"
        ]
        
        let contructionResult = constructRequest(url: configuration.storeFirebaseToken, method: .PUT, body: request, headers: headers)
        
        switch contructionResult {
        case .success(let urlRequest):
            data(request: urlRequest) { (result) in
                switch result {
                case .failure(let networkError):
                    completion(.failure(networkError))
                case .success(_):
                    completion(.success(true))
                    break
                }
            }
        case .failure(let error):
            completion(.failure(error))
        }
    }
    
    @available(iOS 13.0, *)
    func storeFirebaseToken(request: StoreFirebaseTokenRequest, with authToken: String) -> AnyPublisher<Data,URLError>{
        let headers = [
            HTTPHeaderKey.authorization: "Bearer \(authToken)"
        ]
        let contructionResult = constructRequest(url: configuration.storeFirebaseToken, method: .PUT, body: request, headers: headers)
        switch contructionResult {
        case .success(let urlRequest):
            return session.dataTaskPublisher(for: urlRequest)
                .receive(on: phoneRegistrationQueue)
                .map({ (output) -> Data in
                    os_log(.debug, "Request responded with: %{public}s", output.response.debugDescription)
                    return output.data
                })
                .eraseToAnyPublisher()
        case .failure(_):
            return Fail(error: URLError(.unsupportedURL)).eraseToAnyPublisher()
        }
    }
        
    func getVerificationToken(request: VerificationRequest, completion: @escaping (Result<Verification, NetworkError>) -> ()) {
        os_log("Start get verification token", log: Log.networkManger, type: .debug)
        guard let key = Bundle.main.infoDictionary?["VerificationKey"] as? String else {
            completion(.failure(.invalidRequest))
            return
        }
        
        let headers = [HTTPHeaderKey.xApiKey: key]
        let urlRequest = constructRequest(url: configuration.verificationUrl,
                                          method: .POST,
                                          body: request,
                                          headers: headers)
        
        data(request: urlRequest) { result in
            if #available(iOS 13.0, *) {
                let sub = self.jsonResponseHandler(result: result)
                    .sink(
                        receiveCompletion: { result in
                            if case let .failure(error) = result {
                                os_log("Unable to handle response with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                                completion(.failure(error))
                            }
                        },
                        receiveValue: { value in
                            completion(.success(value))
                        })
                _ = self.disposeBag.insert(sub)
            } else {
                self.jsonResponseHandler(of: Verification.self, result: result) { (result) in
                    switch result {
                    case .failure(let error):
                        os_log("Unable to handle response with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                        completion(.failure(error))
                    case .success(let verification):
                        completion(.success(verification))
                    }
                }
            }
        }
    }
    
    func getCertificate(request: CertificateRequest, completion: @escaping (Result<Certificate, NetworkError>) -> ()) {
        os_log("Start get certificates.", log: Log.networkManger, type: .debug)
        guard let key = Bundle.main.infoDictionary?["VerificationKey"] as? String else {
            completion(.failure(.invalidRequest))
            return
        }
        
        let headers = [HTTPHeaderKey.xApiKey: key]
        let urlRequest = constructRequest(url: configuration.certificateUrl,
                                          method: .POST,
                                          body: request,
                                          headers: headers)
        
        data(request: urlRequest) { result in
            if #available(iOS 13.0, *) {
                let sub = self.jsonResponseHandler(result: result)
                    .sink(
                        receiveCompletion: { result in
                            if case let .failure(error) = result {
                                os_log("Unable to handle response with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                                completion(.failure(error))
                            }

                        },
                        receiveValue: { value in
                            completion(.success(value))
                        })
                _ = self.disposeBag.insert(sub)
            } else {
                self.jsonResponseHandler(of: Certificate.self, result: result) { (result) in
                    switch result {
                    case .failure(let error):
                        os_log("Unable to handle response with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                        completion(.failure(error))
                    case .success(let certificate):
                        completion(.success(certificate))
                    }
                }
            }
        }
    }
    
    func publishKeys(request: PublishRequest, completion: @escaping (Result<Publish, NetworkError>) -> ()) {
        os_log("Start publish keys", log: Log.networkManger, type: .debug)
        let urlRequest = constructRequest(url: configuration.publishKeysUrl,
                                          method: .POST,
                                          body: request)
        
        data(request: urlRequest) { result in
            if #available(iOS 13.0, *) {
                let sub = self.jsonResponseHandler(result: result)
                    .sink(
                        receiveCompletion: { result in
                            if case let .failure(error) = result {
                                os_log("Unable to handle response with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                                completion(.failure(error))
                            }

                        },
                        receiveValue: { value in
                            completion(.success(value))
                        })
                _ = self.disposeBag.insert(sub)
            } else {
                self.jsonResponseHandler(of: Publish.self, result: result) { (result) in
                    switch result {
                    case .failure(let error):
                        os_log("Unable to handle response with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                        completion(.failure(error))
                    case .success(let publish):
                        completion(.success(publish))
                    }
                }
            }
        }
    }
    
    func getKeySets(after filename: String?, completion: @escaping (Result<([ExposureKeySet]), NetworkError>) -> ()) {
        os_log("Start get key sets", log: Log.networkManger, type: .debug)
        let expectedContentType = HTTPContentType.text
        let headers = [HTTPHeaderKey.acceptedContentType: expectedContentType.rawValue]
        
        let urlRequest = constructRequest(url: configuration.exposureKeySetUrl,
                                          method: .GET,
                                          headers: headers)
        
        data(request: urlRequest) { result in
            switch result {
            case .success(let result):
                let urlsStrings = String(data: result.1, encoding: .utf8)?.components(separatedBy: .newlines) ?? []
                let urls = urlsStrings
                    .valuesAfter(filename)// We only want to download files newer than the newest we already have
                    .reduce([URL]()) {(results, nextURLStr) -> [URL] in
                        guard let nextURL = URL(string: nextURLStr) else {
                            os_log("Unable to convert string to url: %{public}s",log: Log.networkManger, type: .error, nextURLStr)
                            return results
                        }
                        return results + [nextURL]
                    } // URLS with the timestamps
                
                if #available(iOS 14.0, *) {
                    let sub = urls.publisher
                        .flatMap(self.downloadKeySet)
                        .collect()
                        .sink(receiveCompletion: { result in
                            if case let .failure(error) = result {
                                os_log("Unable to download key set with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                                completion(.failure(error))
                            }
                            
                        }, receiveValue: { keySets in
                            completion(.success(keySets))
                        })
                    _ = self.disposeBag.insert(sub)
                } else {
                    let dispatchGroup: DispatchGroup = DispatchGroup()
                    var downloadError: NetworkError? = nil
                    var keySets: [ExposureKeySet] = []
                    for url in urls {
                        DispatchQueue.global(qos: .default).async {
                            dispatchGroup.enter()
                            self.downloadKeySet(at: url) { (result) in
                                switch result{
                                case .failure(let error):
                                    os_log("Unable to download key set with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                                    downloadError = error
                                case .success(let keySet):
                                    keySets.append(keySet)
                                }
                                dispatchGroup.leave()
                            }
                        }
                    }
                    dispatchGroup.notify(queue: .global(qos: .default)) {
                        if downloadError != nil {
                            completion(.failure(downloadError!))
                        }
                        completion(.success(keySets))
                    }
                }
            case .failure(let error):
                os_log("Unable to get key sets with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                completion(.failure(error))
            }
        }
    }
    
    private func downloadKeySet(at url: URL, completion: @escaping (Result<ExposureKeySet, NetworkError>) -> Void) {
        DispatchQueue.global(qos: .utility).async { [weak self] in
            guard let this = self else {
                // TODO: Change to a notInitError
                completion(.failure(.resourceNotFound))
                return
            }
            os_log("Start download key set from url: %{public}s", log: Log.networkManger, type: .debug, url.absoluteString)
            let expectedContentType = HTTPContentType.zip
            let headers = [HTTPHeaderKey.acceptedContentType: expectedContentType.rawValue]
            let identifier = url.lastPathComponent.replacingOccurrences(of: ".zip", with: "")
            
            let urlRequest = this.constructRequest(
                url: this.configuration.exposureKeySet(identifier: identifier),
                method: .GET,
                headers: headers
            )
            this.download(request: urlRequest) { result in
                switch result {
                case let .failure(error):
                    os_log("Unable to dowload key set with error: %{public}s", log: Log.networkManger, type: .info, error.localizedDescription)
                    completion(.failure(error))
                case let .success((_, url)):
                    this.responseToExposureKeySet(url: url, identifier: identifier) { (result) in
                        switch result{
                        case .success(let keySet):
                            completion(.success(keySet))
                            break
                        case .failure(let error):
                            os_log("Unable to map response to key set with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                            completion(.failure(error.asNetworkError))
                            break
                        }
                    }
                }
            }
        }
    }
    
    @available(iOS 13.0, *)
    private func downloadKeySet(at url: URL) -> AnyPublisher<ExposureKeySet, NetworkError> {
        os_log("Start download key set from url: %{public}s", log: Log.networkManger, type: .debug, url.absoluteString)
        let expectedContentType = HTTPContentType.zip
        let headers = [HTTPHeaderKey.acceptedContentType: expectedContentType.rawValue]
        let identifier = url.lastPathComponent.replacingOccurrences(of: ".zip", with: "")
        
        let urlRequest = constructRequest(url: configuration.exposureKeySet(identifier: identifier),
                                          method: .GET,
                                          headers: headers)
        
        return Deferred {
            Future { promise in
            
                self.download(request: urlRequest) { result in
                    switch result {
                    case let .failure(error):
                        os_log("Unable to dowload key set with error: %{public}s", log: Log.networkManger, type: .info, error.localizedDescription)
                        promise(.failure(error))
                    case let .success(result):
                        let sub = self.responseToExposureKeySet(url: result.1, identifier: identifier)
                            .mapError { $0.asNetworkError }
                            .sink(
                                receiveCompletion: { result in
                                    if case let .failure(error) = result {
                                        os_log("Unable to map response to key set with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                                        promise(.failure(error))
                                    }
                                },
                                receiveValue: { keySet in
                                    promise(.success(keySet))
                                })
                        _ = self.disposeBag.insert(sub)
                    }
                }
            }
        }.eraseToAnyPublisher()
    }
    
    
    // MARK: - Download Data
    private func data(request: Result<URLRequest, NetworkError>, completion: @escaping (Result<(URLResponse, Data), NetworkError>) -> ()) {
        os_log("Start data request with result.", log: Log.storageManager, type: .debug)
        switch request {
        case let .success(request):
            data(request: request, completion: completion)
        case let .failure(error):
            os_log("Cannot make data request with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
            completion(.failure(error))
        }
    }

    private func data(request: URLRequest, completion: @escaping (Result<(URLResponse, Data), NetworkError>) -> ()) {
        os_log("Start data request.", log: Log.storageManager, type: .debug)
        session.dataTask(with: request) { data, response, error in
            self.handleNetworkResponse(data,
                                       response: response,
                                       error: error,
                                       completion: completion)
        }
        .resume()
    }
    
    // MARK: - Download Files
    fileprivate func download(request: Result<URLRequest, NetworkError>, completion: @escaping (Result<(URLResponse, URL), NetworkError>) -> ()) {
        os_log("Start dowload request with result.", log: Log.storageManager, type: .debug)
        switch request {
        case let .success(request):
            download(request: request, completion: completion)
        case let .failure(error):
            os_log("Cannot make download request with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
            completion(.failure(error))
        }
    }

    fileprivate func download(request: URLRequest, completion: @escaping (Result<(URLResponse, URL), NetworkError>) -> ()) {
        os_log("Start dowload request.", log: Log.storageManager, type: .debug)
        session.dataTask(with: request) { data, response, error in
            let localUrl: URL?

            if let data = data {
                localUrl = self.write(data: data)
            } else {
                localUrl = nil
            }

            self.handleNetworkResponse(localUrl, response: response, error: error, completion: completion)
        }.resume()
    }

    private func write(data: Data) -> URL? {
        os_log("Write data to temp file.", log: Log.storageManager, type: .debug)
        let uuid = UUID().uuidString
        let temporaryUrl = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(uuid)

        do {
            try data.write(to: temporaryUrl)
            return temporaryUrl
        } catch {
            os_log("Couldn't write data to temp file with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
            return nil
        }
    }
    
    // MARK: - Utility
    private func constructRequest(url: URL?,
                                  method: HTTPMethod = .GET,
                                  body: Encodable? = nil,
                                  headers: [HTTPHeaderKey: String] = [:]) -> Result<URLRequest, NetworkError> {
        os_log("Strat construct request", log: Log.networkManger, type: .debug)
        guard let url = url else {
            os_log("Unable to construct request, invalid url.", log: Log.networkManger, type: .error)
            return .failure(.invalidRequest)
        }

        var request = URLRequest(url: url,
                                 cachePolicy: .useProtocolCachePolicy,
                                 timeoutInterval: 10)
        request.httpMethod = method.rawValue

        let defaultHeaders = [
            HTTPHeaderKey.contentType: HTTPContentType.json.rawValue
        ]

        defaultHeaders.forEach { header, value in
            request.addValue(value, forHTTPHeaderField: header.rawValue)
        }

        headers.forEach { header, value in 
            request.addValue(value, forHTTPHeaderField: header.rawValue)
        }

        if let body = body.flatMap({ try? self.jsonEncoder.encode(AnyEncodable($0)) }) {
            request.httpBody = body
        }

        os_log("--REQUEST--", log: Log.networkManger, type: .debug)
        os_log("Type: %{public}s", log: Log.networkManger, type: .debug, method.rawValue)
        if let url = request.url { os_log("%{public}s", log: Log.networkManger, type: .debug, url.debugDescription) }
        if let allHTTPHeaderFields = request.allHTTPHeaderFields {
            os_log("%{public}s", log: Log.networkManger, type: .debug,allHTTPHeaderFields.debugDescription)
        }
        if let httpBody = request.httpBody {
            os_log("%{public}s", log: Log.networkManger, type: .debug,String(data: httpBody, encoding: .utf8)!)
        }
        os_log("--END REQUEST--", log: Log.networkManger, type: .debug)

        return .success(request)
    }
    
    private func jsonResponseHandler<Object: Decodable>(of type: Object.Type = Object.self, result: Result<(URLResponse, Data), NetworkError>, completion: (Result<Object, NetworkError>) -> Void ) {
        os_log("Start json response handler.", log: Log.networkManger, type: .debug)
        switch result {
        case let .success((_,data)):
            // Can't call decodeJson directly, the compiler couldn't inferr the type so It gets converted inside this function
            do {
                let object = try self.jsonDecoder.decode(Object.self, from: data)
                os_log("Response object: %{public}@", log: Log.networkManger, type: .debug, String(describing: object))
                completion(.success(object))
            } catch {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    os_log("Raw JSON: %{public}s", log: Log.networkManger, type: .debug, json)
                }
                os_log("Error deserializing with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                let error = NetworkResponseHandleError.cannotDeserialize
                completion(.failure(error.asNetworkError))
            }
            break
        case let .failure(error):
            os_log("Unable to decode json with error: %{public}s", log: Log.networkManger, type: .error)
            completion(.failure(error))
        }
    }
    
    @available(iOS 13.0, *)
    private func jsonResponseHandler<Object: Decodable>(result: Result<(URLResponse, Data), NetworkError>) -> AnyPublisher<Object, NetworkError> {
        os_log("Start json response handler.", log: Log.networkManger, type: .debug)
        switch result {
        case let .success(result):
            return decodeJson(data: result.1)
                .mapError { $0.asNetworkError }
                .eraseToAnyPublisher()
        case let .failure(error):
            os_log("Unable to decode json with error: %{public}s", log: Log.networkManger, type: .error)
            return Fail(error: error).eraseToAnyPublisher()
        }
    }
    
    private func decodeJson<Object: Decodable>(data: Data, of type: Object.Type = Object.self ,with completion: (Result<Object, NetworkResponseHandleError>) -> Void) {
        os_log("Start decoding json.", log: Log.networkManger, type: .debug)
        do {
            let object = try self.jsonDecoder.decode(Object.self, from: data)
            os_log("Response object: %{public}@", log: Log.networkManger, type: .debug, String(describing: object))
            completion(.success(object))
        } catch {
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                os_log("Raw JSON: %{public}s", log: Log.networkManger, type: .debug, json)
            }
            os_log("Error deserializing with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
            completion(.failure(.cannotDeserialize))
        }
    }
    
    @available(iOS 13.0,*)
    private func decodeJson<Object: Decodable>(data: Data) -> AnyPublisher<Object, NetworkResponseHandleError> {
        os_log("Start decoding json.", log: Log.networkManger, type: .debug)
        return Future { promise in
            do {
                let object = try self.jsonDecoder.decode(Object.self, from: data)
                os_log("Response object: %{public}@", log: Log.networkManger, type: .debug, String(describing: object))
                promise(.success(object))
            } catch {
                if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    os_log("Raw JSON: %{public}s", log: Log.networkManger, type: .debug, json)
                }
                os_log("Error deserializing with error: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
                promise(.failure(.cannotDeserialize))
            }
        }
        .share()
        .eraseToAnyPublisher()
    }
    
    private func handleNetworkResponse<Object>(_ object: Object?,
                                               response: URLResponse?,
                                               error: Error?,
                                               completion: @escaping (Result<(URLResponse, Object), NetworkError>) -> ()) {
        os_log("Start handle network response.", log: Log.networkManger, type: .debug)
        if error != nil {
            os_log("Unable to handle response with error: %{public}s", log: Log.networkManger, type: .error, error!.localizedDescription)
            completion(.failure(.invalidResponse))
            return
        }

        
        os_log("--RESPONSE--", log: Log.networkManger, type: .debug)
        if let response = response as? HTTPURLResponse {
            os_log("Finished response to URL: %{public}s with status %{public}i",
                   log: Log.networkManger,
                   type: .debug,
                   response.url != nil ? response.url!.absoluteString : "Not url found.",
                   response.statusCode)
            

            let headers = response.allHeaderFields.map { header, value in
                return String("\(header): \(value)")
            }.joined(separator: "\n")
            os_log("Response headers: \n%{public}s", log: Log.networkManger, type: .debug, headers)
        } else if let error = error {
            os_log("Error with response: %{public}s", log: Log.networkManger, type: .debug, error.localizedDescription)
        }
        os_log("--END RESPONSE--", log: Log.networkManger, type: .debug)

        guard let response = response,
            let object = object else {
            os_log("Nil object or response.", log: Log.networkManger, type: .error)
            completion(.failure(.invalidResponse))
            return
        }

        if let error = self.inspect(response: response) {
            os_log("Error while inspecting response: %{public}s", log: Log.networkManger, type: .error, error.localizedDescription)
            completion(.failure(error))
            return
        }

        completion(.success((response, object)))
    }
    
    private func responseToExposureKeySet(url: URL, identifier: String, completion: @escaping (Result<ExposureKeySet, NetworkResponseHandleError>)->Void){
        self.storageManager.createExposureKeySet(fromUrl: url, withIdentifier: identifier) { (result) in
            completion(result)
        }
    }
    
    @available(iOS 13.0, *)
    private func responseToExposureKeySet(url: URL, identifier: String) -> AnyPublisher<ExposureKeySet, NetworkResponseHandleError> {
        os_log("Start respone to exposure key set.", log: Log.networkManger, type: .debug)
        let keyset = Just(url)
            .setFailureType(to: NetworkResponseHandleError.self)
            .flatMap({ self.storageManager.createExposureKeySet(fromUrl: $0, withIdentifier: identifier) })
            .eraseToAnyPublisher()
        return keyset
    }
    
    private func inspect(response: URLResponse) -> NetworkError? {
        os_log("Start inspecting response.", log: Log.networkManger, type: .debug)
        guard let response = response as? HTTPURLResponse else {
            os_log("Invalid response", log: Log.networkManger, type: .error)
            return .invalidResponse
        }

        switch response.statusCode {
        case 200 ... 299:
            return nil
        case 304:
            return .responseCached
        case 300 ... 399:
            return .redirection
        case 400 ... 499:
            return .resourceNotFound
        case 500 ... 599:
            return .serverError
        default:
            return .invalidResponse
        }
    }
    
}

