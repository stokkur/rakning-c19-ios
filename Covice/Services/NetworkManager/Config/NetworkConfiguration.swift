//
//  NetworkConfiguration.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

struct NetworkConfiguration {
    struct EndpointConfiguration {
        let scheme: String
        let host: String
        let path: [String]
        let tokenParams: [String: String] = [:]
    }
    enum EndpointsConfigKey {
        case name
        case registrationScheme
        case registrationHost
        case registrationPath
        case verificationScheme
        case verificationHost
        case verificationPath
        case exposureScheme
        case exposureHost
        case exposurePath
        case keySetsScheme
        case keySetsHost
        case keySetsPath
        
        var key: String {
            switch self {
            case .name:
                return "ENDPOINT_CONFIG_NAME"
            case .registrationScheme:
                return "REGISTRATION_SCHEME"
            case .registrationHost:
                return "REGISTRATION_HOST"
            case .registrationPath:
                return "REGISTRATION_PATH"
            case .verificationScheme:
                return "VERIFICATION_SCHEME"
            case .verificationHost:
                return "VERIFICATION_HOST"
            case .verificationPath:
                return "VERIFICATION_PATH"
            case .exposureScheme:
                return "EXPOSURE_SCHEME"
            case .exposureHost:
                return "EXPOSURE_HOST"
            case .exposurePath:
                return "EXPOSURE_PATH"
            case .keySetsScheme:
                return "KEY_SETS_SCHEME"
            case .keySetsHost:
                return "KEY_SETS_HOST"
            case .keySetsPath:
                return "KEY_SETS_PATH"
            }
        }
        
    }

    let name: String
    let registration: EndpointConfiguration
    let verification: EndpointConfiguration
    let exposure: EndpointConfiguration
    let keySets: EndpointConfiguration
    
    static let networkConfiguration = NetworkConfiguration(
        name: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.name.key),
        registration: .init(
            scheme: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.registrationScheme.key),
            host: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.registrationHost.key),
            path: [EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.registrationPath.key)]
        ),
        verification: .init(
            scheme: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.verificationScheme.key),
            host: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.verificationHost.key),
            path: [EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.verificationPath.key)]
        ),
        exposure: .init(
            scheme: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.exposureScheme.key),
            host: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.exposureHost.key),
            path: [EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.exposurePath.key)]
        ),
        keySets: .init(
            scheme: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.keySetsScheme.key),
            host: EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.keySetsHost.key),
            path: [EnvironmentConf<String>.getConfig(for: EndpointsConfigKey.keySetsPath.key)]
        )
    )
    
    private var urlQueryEncodedCharacterSet: CharacterSet = {
        // WARNING: Do not remove this code, this will break signature validation on the backend.
        // specify characters which are allowed to be unespaced in the queryString, note the `inverted`
        let characterSet = CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted
        return characterSet
    }()
    
    private func combine(path: Path, config: EndpointConfiguration, params: [String: String] = [:]) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = config.scheme
        urlComponents.host = config.host
        urlComponents.path = "/" + (config.path + path.components).joined(separator: "/")

        if !params.isEmpty {
            urlComponents.percentEncodedQueryItems = params.compactMap { parameter in
                guard let name = parameter.key.addingPercentEncoding(withAllowedCharacters: urlQueryEncodedCharacterSet),
                    let value = parameter.value.addingPercentEncoding(withAllowedCharacters: urlQueryEncodedCharacterSet) else {
                    return nil
                }

                return URLQueryItem(name: name, value: value)
            }
        }

        return urlComponents.url
    }
}

extension NetworkConfiguration {
    
    var requestPinUrl: URL? {
        return self.combine(path: Endpoint.requestPin, config: registration, params: registration.tokenParams)
    }
    
    var pinVerificationUrl: URL? {
        return self.combine(path: Endpoint.verifyPin, config: registration, params: registration.tokenParams)
    }
    
    var storeFirebaseToken: URL? {
        return self.combine(path: Endpoint.storeFirebaseToken, config: registration, params: registration.tokenParams)
    }
    
    var verificationUrl: URL? {
        return self.combine(path: Endpoint.verify, config: verification, params: verification.tokenParams)
    }
    
    var certificateUrl: URL? {
        return self.combine(path: Endpoint.certificate, config: verification, params: verification.tokenParams)
    }
    
    var publishKeysUrl: URL? {
        return self.combine(path: Endpoint.publish, config: exposure, params: exposure.tokenParams)
    }
    
    var exposureKeySetUrl: URL? {
        return self.combine(path: Endpoint.keySets, config: keySets, params: keySets.tokenParams)
    }
    
    func exposureKeySet(identifier: String) -> URL? {
        return self.combine(path: Endpoint.keySet(identifier: identifier), config: keySets, params: keySets.tokenParams)
    }
}
