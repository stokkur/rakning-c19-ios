//
//  Endpoint.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

struct Endpoint {
    static let requestPin           = Path(components: "requestpin")
    static let verifyPin            = Path(components: "pin")
    static let storeFirebaseToken   = Path(components: "")
    static let verify               = Path(components: "verify")
    static let certificate          = Path(components: "certificate")
    static let publish              = Path(components: "publish")
    static let keySets              = Path(components: "index.txt")
    
    static func keySet(identifier: String) -> Path { Path(components: [identifier, "zip"].joined(separator: ".")) }
}
