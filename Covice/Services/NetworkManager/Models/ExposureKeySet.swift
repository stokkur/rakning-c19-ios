//
//  ExposureKeySet.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import ZIPFoundation

struct ExposureKeySet {

    static let EXPORT_BINARY = "export.bin"
    static let EXPORT_SIGNATURE = "export.sig"

    let identifier: String
    let signatureUrl: URL
    let binaryUrl: URL
    let creationDate = Date()
    
    static func fromUrl(_ url: URL, withIdentifier identifier: String, at storagePath: URL) -> ExposureKeySet? {
        let srcSignatureUrl = url.appendingPathComponent(EXPORT_SIGNATURE)
        let srcBinaryUrl = url.appendingPathComponent(EXPORT_BINARY)

        let dstSignatureFilename = [identifier, "sig"].joined(separator: ".")
        let dstSignatureUrl = storagePath.appendingPathComponent(dstSignatureFilename)
        let dstBinaryFilename = [identifier, "bin"].joined(separator: ".")
        let dstBinaryUrl = storagePath.appendingPathComponent(dstBinaryFilename)

        do {
            if FileManager.default.fileExists(atPath: dstSignatureUrl.path) {
                try FileManager.default.removeItem(atPath: dstSignatureUrl.path)
            }

            if FileManager.default.fileExists(atPath: dstBinaryUrl.path) {
                try FileManager.default.removeItem(atPath: dstBinaryUrl.path)
            }

            try FileManager.default.moveItem(at: srcSignatureUrl, to: dstSignatureUrl)
            try FileManager.default.moveItem(at: srcBinaryUrl, to: dstBinaryUrl)
        } catch {
            return nil
        }

        return ExposureKeySet(identifier: identifier,
                              signatureUrl: dstSignatureUrl,
                              binaryUrl: dstBinaryUrl)
        
    }
}
