//
//  VerificationRequest.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation


extension String {
    static func randomString(of length: Int) -> String {
        guard length > 0 else {
            return ""
        }
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        
        return String((0 ..< length).map { _ in letters.randomElement() ?? "a" })
    }
}

struct VerificationRequest: Encodable {
    let code: String
    let accept = ["confirmed", "likely", "negative"]
    var padding: String = ""
}

struct Verification: Decodable {
    enum TestType: String {
        case confirmed = "confirmed"
        case likely = "liklely"
        case negative = "negative"
        
        var localizedString: String {
            switch self {
            case .confirmed: return Localization.test_result_type_confirmed
            case .likely: return Localization.test_result_type_likely
            case .negative: return Localization.test_result_type_negative
            }
        }
    }
    
    var id = UUID()
    let token: String
    let testDate: Date
    let symptomDate: Date?
    let testType: TestType?
    let padding: String
    
    enum CodingKeys: String, CodingKey {
        case token, testDate, symptomDate, padding
        case testType = "testtype"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        
        token = try values.decode(String.self, forKey: .token)
        
        let testDateValue = try values.decode(String.self, forKey: .testDate)
        self.testDate = formatter.date(from: testDateValue) ?? Date()
        
        if let symptomsDateValue = try? values.decode(String.self, forKey: .symptomDate),
           let symptomsDate = formatter.date(from: symptomsDateValue)  {
            self.symptomDate = symptomsDate
        } else {
            self.symptomDate = nil
        }
        
        if let typeStr = try? values.decode(String.self, forKey: .testType) {
            self.testType = TestType(rawValue: typeStr)
        } else {
            self.testType = .confirmed
        }
        
        self.padding = try values.decode(String.self, forKey: .padding)
    }
    
    init(id: UUID, token: String, testDate: Date, symptomDate: Date?, testType: String, padding: String) {
        self.id = id
        self.token = token
        self.testDate = testDate
        self.symptomDate = symptomDate
        self.testType = TestType(rawValue: testType)
        self.padding = padding
    }
    
    var onsetInterval: Int? {
        if let timeInterval = symptomDate?.timeIntervalSince1970 {
            return Int(timeInterval)
        } else {
            return nil
        }
    }
}
