//
//  Publish.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

struct PublishRequest: Encodable {
    let temporaryExposureKeys: [DiagnosisKey]
    let healthAuthorityID: String = "is.landlaeknir.rakning"
    let hmacKey: String
    let verificationPayload: String
    let traveler: Bool
    let symptomOnsetInterval: Int?
    let revisionToken: String?
    let padding: String = ""
}

struct Publish: Decodable {
    var id = UUID()
    let revisionToken: String
    let insertedExposures: Int
    let padding: String
    
    init(id: UUID, revisionToken: String, insertedExposures: Int, padding: String) {
        self.id = id
        self.revisionToken = revisionToken
        self.insertedExposures = insertedExposures
        self.padding = padding
    }
    
    enum CodingKeys: String, CodingKey {
        case revisionToken, insertedExposures, padding
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.revisionToken = try values.decode(String.self, forKey: .revisionToken)
        self.insertedExposures = try values.decode(Int.self, forKey: .insertedExposures)
        self.padding = try values.decode(String.self, forKey: .padding)
    }
}
