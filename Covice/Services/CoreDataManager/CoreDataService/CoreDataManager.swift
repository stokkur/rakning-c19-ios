//
//  CoreDataEngine.swift
//  Covice

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import CoreData
import Combine

enum CoreDataManagerError: LocalizedError {
    case fetchError(Error)
    case updateError(Error)
    case deleteError(Error)
    
    var errorDescription: String? {
        switch self {
        case .fetchError: return "Fetch Error"
        case .updateError: return "Update Error"
        case .deleteError: return "Delete Error"
        }
    }
}


protocol CoreDataServiceProtocol {
    
    func fetch<Entity: ManagedObjectConvertible>(with predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?, fetchLimit: Int?, completion: @escaping (Result<[Entity], CoreDataManagerError>) -> Void)
    
    func update<Entity: ManagedObjectConvertible>(entities: [Entity], completion: @escaping (CoreDataManagerError?) -> Void)
    
    func delete<Entity: ManagedObjectConvertible>(entities: [Entity], completion: @escaping (CoreDataManagerError?) -> Void)
    
}

extension CoreDataServiceProtocol {
    func fetch<Entity: ManagedObjectConvertible>(with predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil, fetchLimit: Int? = nil, completion: @escaping (Result<[Entity], CoreDataManagerError>) -> Void) {
        fetch(with: predicate, sortDescriptors: sortDescriptors, fetchLimit: fetchLimit, completion: completion)
    }
}

class CoreDataManager: CoreDataServiceProtocol {
    
    let coreData: CoreDataStack
    
    init(coreData: CoreDataStack) {
        self.coreData = coreData
    }
    
    @available(iOS 13.0, *)
    func publisher<Entity>(for type: Entity.Type) -> AnyPublisher<[Entity], Never> where Entity : ManagedObjectConvertible {
        let context = coreData.viewContext
        
        var notification = Notification.Name.NSManagedObjectContextDidMergeChangesObjectIDs
        if #available(iOS 14.0, *) {
            notification = NSManagedObjectContext.didMergeChangesObjectIDsNotification
        }
        
        return NotificationCenter.default.publisher(for: notification, object: context)
            .compactMap({ notification in
                let updated = notification.userInfo?[NSUpdatedObjectIDsKey] as? Set<NSManagedObjectID> ?? Set()
                
                let objects = updated
                    .filter({ id in id.entity == type.ManagedObject.entity() })
                    .compactMap({ id in context.object(with: id) as? Entity.ManagedObject })
                    .compactMap({ managedObject in managedObject.toEntity() as? Entity })
                
                return objects
            })
            .eraseToAnyPublisher()
    }
    
    
    func fetch<Entity>(with predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil, fetchLimit: Int? = nil, completion: @escaping (Result<[Entity], CoreDataManagerError>) -> Void) where Entity : ManagedObjectConvertible {
        
        coreData.performForegroundTask { context in
            do {
                let fetchRequest = Entity.ManagedObject.fetchRequest()
                fetchRequest.predicate = predicate
                fetchRequest.sortDescriptors = sortDescriptors
                if let fetchLimit = fetchLimit {
                    fetchRequest.fetchLimit = fetchLimit
                }
                
                let results = try context.fetch(fetchRequest) as? [Entity.ManagedObject]
                let items = results?.compactMap { $0.toEntity() as? Entity } ?? []
                completion(.success(items))
            } catch {
                NSLog("Core Data fetch error: \(error)")
                completion(.failure(.fetchError(error)))
            }
        }
    }

    func update<Entity>(entities: [Entity], completion: @escaping (CoreDataManagerError?) -> Void) where Entity : ManagedObjectConvertible {
        coreData.performBackgroundTask { context in
            _ = entities.compactMap { $0.toManagedObject(context: context) }
            
            do {
                try context.save()
                completion(nil)
            } catch {
                NSLog("Core Data save error: \(error)")
                completion(.updateError(error))
            }
        }
    }
    
    func delete<Entity>(entities: [Entity], completion: @escaping (CoreDataManagerError?) -> Void) where Entity : ManagedObjectConvertible {
        
        coreData.performBackgroundTask { context in
            let objects = entities.compactMap { $0.toManagedObject(context: context) }
            
            for object in objects {
                context.delete(object)
            }
            
            do {
                try context.save()
                completion(nil)
            } catch {
                NSLog("Core Data delete error: \(error)")
                completion(.deleteError(error))
            }
        }
    }
    
}
