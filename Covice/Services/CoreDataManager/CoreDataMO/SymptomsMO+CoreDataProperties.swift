//
//  SymptomsMO+CoreDataProperties.swift
//  

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import CoreData


extension SymptomsMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SymptomsMO> {
        return NSFetchRequest<SymptomsMO>(entityName: "SymptomsMO")
    }

    @NSManaged public var id: UUID
    @NSManaged public var type: Int32
    @NSManaged public var date: Date?
}

extension SymptomsMO: ManagedObjectProtocol {
    func toEntity() -> Symptoms? {
        let option = SymptomsOption(rawValue: self.type)
        return Symptoms(id: self.id, type: option, onsetDate: self.date)
    }
}

extension Symptoms: ManagedObjectConvertible {
    func toManagedObject(context: NSManagedObjectContext) -> SymptomsMO? {
        let symptoms = SymptomsMO.getOrCreate(withId: self.id, in: context)
        symptoms.id = self.id
        symptoms.type = self.type?.rawValue ?? 99
        symptoms.date = self.onsetDate
        
        return symptoms
    }
}
