//
//  ExposureConfigMO+CSa.swift
//  Covice
//

import Foundation
import CoreData

extension ExposureConfigMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ExposureConfigMO> {
        return NSFetchRequest<ExposureConfigMO>(entityName: "ExposureConfigMO")
    }

    @NSManaged public var id: UUID
    @NSManaged public var configId: String
    @NSManaged public var immediateDurationWeight: Double
    @NSManaged public var immediateThreshold: Int16
    @NSManaged public var nearDurationWeight: Double
    @NSManaged public var nearThreshold: Int16
    @NSManaged public var mediumDurationWeight: Double
    @NSManaged public var mediumThreshold: Int16
    @NSManaged public var otherDurationWeight: Double
    @NSManaged public var infectiousnessHighWeight: Double
    @NSManaged public var infectiousnessStandardWeight: Double
    @NSManaged public var minimumRiskScore: Int16
}

extension ExposureConfigMO: ManagedObjectProtocol {
    func toEntity() -> ExposureConfig? {
        return ExposureConfig(
            id: self.id,
            configId: self.configId,
            immediateDurationWeight: self.immediateDurationWeight,
            immediateThreshold: self.immediateThreshold,
            nearDurationWeight: self.nearDurationWeight,
            nearThreshold: self.nearThreshold,
            mediumDurationWeight: self.mediumDurationWeight,
            mediumThreshold: self.mediumThreshold,
            otherDurationWeight: self.otherDurationWeight,
            infectiousnessHighWeight: self.infectiousnessHighWeight,
            infectiousnessStandardWeight: self.infectiousnessStandardWeight,
            minimumRiskScore: self.minimumRiskScore
        )
    }
}

extension ExposureConfig: ManagedObjectConvertible {
    func toManagedObject(context: NSManagedObjectContext) -> ExposureConfigMO? {
        let exposureConfig = ExposureConfigMO.getOrCreate(withId: self.id, in: context)
        exposureConfig.id = self.id
        exposureConfig.configId = self.configId
        exposureConfig.immediateDurationWeight = self.immediateDurationWeight
        exposureConfig.immediateThreshold = self.immediateThreshold
        exposureConfig.nearDurationWeight = self.nearDurationWeight
        exposureConfig.nearThreshold = self.nearThreshold
        exposureConfig.mediumDurationWeight = self.mediumDurationWeight
        exposureConfig.mediumThreshold = self.mediumThreshold
        exposureConfig.otherDurationWeight = self.otherDurationWeight
        exposureConfig.infectiousnessHighWeight = self.infectiousnessHighWeight
        exposureConfig.infectiousnessStandardWeight = self.infectiousnessStandardWeight
        exposureConfig.minimumRiskScore = self.minimumRiskScore
        return exposureConfig
    }
}
