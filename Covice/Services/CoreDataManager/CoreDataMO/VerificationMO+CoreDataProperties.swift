//
//  VerificationMO+CoreDataProperties.swift
//  

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import CoreData


extension VerificationMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VerificationMO> {
        return NSFetchRequest<VerificationMO>(entityName: "VerificationMO")
    }

    @NSManaged public var id: UUID
    @NSManaged public var token: String
    @NSManaged public var testDate: Date
    @NSManaged public var symptomDate: Date?
    @NSManaged public var testType: String?
    @NSManaged public var padding: String
}

extension VerificationMO: ManagedObjectProtocol {
    func toEntity() -> Verification? {
        return Verification(id: self.id, token: self.token, testDate: self.testDate, symptomDate: self.symptomDate, testType: self.testType ?? "", padding: self.padding)
    }
}

extension Verification: ManagedObjectConvertible {
    func toManagedObject(context: NSManagedObjectContext) -> VerificationMO? {
        let verification = VerificationMO.getOrCreate(withId: self.id, in: context)
        verification.id = self.id
        verification.token = self.token
        verification.testDate = self.testDate
        verification.symptomDate = self.symptomDate
        verification.testType = self.testType?.rawValue
        verification.padding = self.padding
        
        return verification
    }
}
