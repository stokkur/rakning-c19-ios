//
//  ExposureResportMO+CoreDataProperties.swift
//  

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import CoreData


extension ExposureReportMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ExposureReportMO> {
        return NSFetchRequest<ExposureReportMO>(entityName: "ExposureResportMO")
    }

    @NSManaged public var id: UUID
    @NSManaged public var date: Date
    @NSManaged public var shared: Bool
    @NSManaged public var verificationCode: String?
    @NSManaged public var verification: VerificationMO?
    @NSManaged public var travelingOption: TravelingMO?
    @NSManaged public var symptomsOption: SymptomsMO?
    @NSManaged public var publish: PublishMO?
}

extension ExposureReportMO: ManagedObjectProtocol {
    func toEntity() -> ExposureReport? {
        return ExposureReport(id: self.id,
                              date: self.date,
                              verificationCode: self.verificationCode,
                              verification: self.verification?.toEntity(),
                              symptoms: self.symptomsOption?.toEntity() ?? Symptoms(),
                              traveling: self.travelingOption?.toEntity() ?? Traveling(),
                              publish: self.publish?.toEntity())
    }
}

extension ExposureReport: ManagedObjectConvertible {
    func toManagedObject(context: NSManagedObjectContext) -> ExposureReportMO? {
        let exposureReport = ExposureReportMO.getOrCreate(withId: self.id, in: context)
        exposureReport.id = self.id
        exposureReport.date = self.date
        exposureReport.verificationCode = self.verificationCode
        exposureReport.verification = self.verification?.toManagedObject(context: context)
        exposureReport.travelingOption = self.traveling.toManagedObject(context: context)
        exposureReport.symptomsOption = self.symptoms.toManagedObject(context: context)
        exposureReport.publish = self.publish?.toManagedObject(context: context)
        return exposureReport
    }
}
