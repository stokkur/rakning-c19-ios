//
//  ExposureMO+CoreDataProperties.swift
//  

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import CoreData
import ExposureNotification

extension ExposureMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ExposureMO> {
        return NSFetchRequest<ExposureMO>(entityName: "ExposureMO")
    }

    @NSManaged public var date: Date?
    @NSManaged public var id: UUID
    @NSManaged public var infectiousness: Int32

}

extension ExposureMO: ManagedObjectProtocol {
    func toEntity() -> Exposure? {
        guard let date = self.date, let infectiousness = ENInfectiousness(rawValue: UInt32(self.infectiousness))
        else { return nil }
        
        return Exposure(date: date, infectiousness: infectiousness)
    }
}

extension Exposure: ManagedObjectConvertible {
    func toManagedObject(context: NSManagedObjectContext) -> ExposureMO? {
        let exposure = ExposureMO.getOrCreate(withId: self.id, in: context)
        exposure.id = self.id
        exposure.date = self.date
        exposure.infectiousness = Int32(infectiousness.rawValue)
        return exposure
    }
}
