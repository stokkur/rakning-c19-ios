//
//  ExposureManager.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import ExposureNotification
import UserNotifications
import Combine
import os
import UIKit

enum ExposureManagerStatus: Equatable {
    case active
    case inactive(ExposureManagerError)
    case notAuthorized
    case authorizationDenied
    case unsupported
}

enum ExposureManagerError: LocalizedError {
    case unknown
    case disabled
    case bluetoothOff
    case restricted
    case notAuthorized
    case authorizationDenied
    case rateLimited
    case signatureValidationFailed
    case internalTypeMismatch // programmers error
    
    var errorDescription: String? {
        switch self {
        case .unknown: return "Unknown"
        case .disabled: return "Disabled"
        case .restricted: return "Restricted"
        case .bluetoothOff: return "Bluetooth Off"
        case .notAuthorized: return "Not Authorized"
        case .rateLimited: return "Rate Limited Reached"
        case .internalTypeMismatch: return "Internal Type Mismatch"
        case .signatureValidationFailed: return "Signature Validation Failed"
        case .authorizationDenied: return "Authorization Denied By User"
        }
    }
}

enum ExposureDetectionError: LocalizedError {
    case detectionInProgress
    case unableToGetConfig
    case unableToGetKeys
    case noExposuresDetected
    case nilSummaries
    case nilExposureWindows
    case enFrameworkError(Error)
    case storageError(CoreDataManagerError)
    case internalDetectionError
    case exposureNotificationUnsupported
    
    var errorDescription: String? {
        switch self {
        case .detectionInProgress:
            return "Detection In Progress"
        case .unableToGetConfig:
            return "Unable To Get Config"
        case .unableToGetKeys:
            return "Unable To Get Keys"
        case .noExposuresDetected:
            return "No Exposures Detected"
        case .nilSummaries:
            return "Nil Summaries"
        case .nilExposureWindows:
            return "Nil Exposure Windows"
        case .enFrameworkError(let error):
            return "En Framework Error: \(error.localizedDescription)"
        case .storageError(let error):
            return "Storage Error: \(error.localizedDescription)"
        case .internalDetectionError:
            return "Internal Detection Error"
        case .exposureNotificationUnsupported:
            return "Exposure Notification Framewokr not supported by the device OS version."
        }
        
    }
}

extension Notification.Name {
    static var exposureNotificationStatus: Notification.Name {
        return .init(rawValue: "ExposureNotification.statusChanged")
    }
}

class ExposureManager {
    
    let manager = ENManager()

    private let notificationCenter = NotificationCenter.default
    private var observers = [NSKeyValueObservation]()
    
    var exposureNotificationStatus: ENStatus {
        return manager.exposureNotificationStatus
    }
    
    required init() {
        observers.append(manager.observe(\.exposureNotificationStatus, options: .new) { [unowned self] (manager, changed) in
            let status = getExposureNotificationStatus()
            self.notificationCenter.post(name: .exposureNotificationStatus, object: status)
        })
    }
    
    deinit {
        manager.invalidate()
        observers.forEach { $0.invalidate() }
    }
    
    func activate() {
        manager.activate { _ in
            if ENManager.authorizationStatus == .authorized && !self.manager.exposureNotificationEnabled {
                self.manager.setExposureNotificationEnabled(true) { _ in
                    // No error handling for attempts to enable on launch
                }
            }
        }
    }
    
    func getExposureNotificationStatus() -> ExposureManagerStatus {
        if getSupportedExposureNotificationsVersion() == .unsupported {
            return .unsupported
        }
        
        let authorisationStatus = type(of: manager).authorizationStatus
        let result: ExposureManagerStatus

        // iOS 14 returns unknown as authorizationStatus always
        let isiOS14OrHigher: Bool
        if #available(iOS 14, *) {
            isiOS14OrHigher = true
        } else {
            isiOS14OrHigher = false
        }

        switch authorisationStatus {
        case .unknown where isiOS14OrHigher:
            fallthrough
        case .authorized:
            let status = manager.exposureNotificationStatus
            switch status {
            case .active:
                result = .active
            case .bluetoothOff:
                result = .inactive(.bluetoothOff)
            case .disabled:
                result = .inactive(.disabled)
            case .restricted:
                result = .inactive(.restricted)
            default:
                result = .inactive(.unknown)
            }
        case .unknown:
            result = .notAuthorized
        case .notAuthorized:
            result = .authorizationDenied
        case .restricted:
            result = .inactive(.restricted)
        default:
            result = .inactive(.unknown)
        }

        return result
    }

    
    static let authorizationStatusChangeNotification = Notification.Name("ExposureManagerAuthorizationStatusChangedNotification")
   
    func detectExposures(configuration: ENExposureConfiguration,
                         exposureKeySets: [ExposureKeySet], completion: @escaping (Result<[Exposure], ExposureDetectionError>) -> Void) -> Progress {
        let progress = Progress()
        
        let urls = exposureKeySets.flatMap { [$0.binaryUrl, $0.signatureUrl] }
        os_log("Start detecting exposure throught ENManager", log: Log.exposureManager, type: .info)
        manager.detectExposures(configuration: configuration, diagnosisKeyURLs: urls) { [weak self] summary, error in
            guard let this = self else { return }
            
            if let error = error {
                os_log("Failed to detect exposures with error: %{public}s", log: Log.exposureManager, type: .error, ExposureDetectionError.enFrameworkError(error).localizedDescription)
                completion(.failure(.enFrameworkError(error)))
                return
            }
            
            guard let summary = summary else {
                os_log("No summaries retuned", log: Log.exposureManager, type: .fault)
                completion(.failure(.nilSummaries))
                return
            }
            
            os_log("Start getting exposure windows throught EnManager.", log: Log.exposureManager, type: .info)
            let getExposureProgress = this.getExposures(summary: summary) { (result) in
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let exposures):
                    completion(.success(exposures))
                }
            }
            
            
            if getExposureProgress != nil {
                progress.cancellationHandler = {
                    getExposureProgress!.cancel()
                }
            }
                
        }
        return progress
    }
    
    func getDiagnosisKeys(completion: @escaping (Result<[DiagnosisKey], ExposureManagerError>) -> Void) {
        os_log("Start getting diagnosis keys throught EnManager.", log: Log.exposureManager, type: .info)
        manager.getDiagnosisKeys { keys, error in
            if let error = error {
                os_log("Unable to get diagnosis keys with error: %{public}s", log: Log.exposureManager, type: .error, error.localizedDescription)
            }
            guard let keys = keys, keys.count > 0 else {
                completion(.failure(.notAuthorized))
                return
            }
            
            completion(.success(keys.asDiagnosisKeys.processedForSubmission()))
        }
    }
    
    func showBluetoothOffUserNotificationIfNeeded() {
        os_log("Start bluetooth user notification if needed.", log: Log.exposureManager, type: .info)
        os_log("Status: %{public}i.", log: Log.exposureManager, type: .info, manager.exposureNotificationStatus.rawValue)
        if ENManager.authorizationStatus == .authorized && manager.exposureNotificationStatus == .bluetoothOff {
            LocalNotification.sendLocalNotification(title: Localization.notification_bluetooth_off_title, body: Localization.notification_bluetooth_off_body, identifier: .BluetoothOff)
        } else {
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [LocalNotification.LocalIdentifier.BluetoothOff.identifier])
        }
    }
    
    func setExposureNotificationEnabled(_ enabled: Bool, completion: @escaping (ExposureManagerError?) -> ()) {
        os_log("Set exposure notification enabled to ENManager", log: Log.exposureManager, type: .info)
        
        if !self.manager.exposureNotificationEnabled {
            manager.setExposureNotificationEnabled(enabled) { error in
                guard let error = error.map({ $0.asExposureManagerError }) else {
                    completion(nil)
                    return
                }
                os_log("Unable to set exposure notification with error: %{public}s", log: Log.exposureManager, type: .error, error.localizedDescription)
                completion(error)
            }
        } else {
            completion(.notAuthorized)
        }
    }
    
    private func getExposures(summary: ENExposureDetectionSummary, completion: @escaping (Result<[Exposure], ExposureDetectionError>) -> Void ) -> Progress? {
        switch getSupportedExposureNotificationsVersion() {
        case .version1:
            // #aviliable needed to prevet compiler errors
            if #available(iOS 13.5, *) {
                let userExplanation = NSLocalizedString("USER_NOTIFICATION_EXPLANATION", comment: "User notification")
                return self.manager.getExposureInfo(summary: summary, userExplanation: userExplanation) { (exposuresInfo, error) in
                    if error != nil {
                        os_log("Failed to get exposure windows with error: %{public}s", log: Log.exposureManager, type: .error, ExposureDetectionError.enFrameworkError(error!).localizedDescription)
                        completion(.failure(.enFrameworkError(error!)))
                    }
                    guard let exposuresInfo = exposuresInfo else {
                        completion(.failure(.nilExposureWindows))
                        return
                    }
                    completion(.success(exposuresInfo.asExposures))
                }
            }
        case .version2:
            return self.manager.getExposureWindows(summary: summary) { windows, error in
                if let error = error {
                    os_log("Failed to get exposure windows with error: %{public}s", log: Log.exposureManager, type: .error, ExposureDetectionError.enFrameworkError(error).localizedDescription)
                    completion(.failure(.enFrameworkError(error)))
                    return
                }
                
                guard let windows = windows else {
                    os_log("Nil exposure windows returned.", log: Log.exposureManager, type: .fault)
                    completion(.failure(.nilExposureWindows))
                    return
                }
                
                completion(.success(windows.asExposures))
            }
            
        default:
            completion(.failure(.exposureNotificationUnsupported))
        }
        return nil
    }
    
}

extension Error {
    var asExposureManagerError: ExposureManagerError {
        if let error = self as? ENError {
            let status: ExposureManagerError

            switch error.code {
            case .bluetoothOff:
                status = .bluetoothOff
            case .restricted:
                status = .restricted
            case .notAuthorized:
                status = .notAuthorized
            case .notEnabled:
                status = .disabled
            case .rateLimited:
                status = .rateLimited
            case .unsupported:
                // usually when receiving unsupported something is off with the signature validation
                status = .signatureValidationFailed
            default:
                status = .unknown
            }

            return status
        }

        return .unknown
    }
}

