//
//  DiagnosisKey.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import ExposureNotification
import os

struct DiagnosisKey: Codable, Equatable {
    let key: Data
    let rollingPeriod: UInt32
    let rollingStartNumber: UInt32
    let transmissionRisk: UInt8
}

extension Array where Element == DiagnosisKey {
    func processedForSubmission(date: Date = Date()) -> [DiagnosisKey] {
        let diagnosisKeyByTimelapsSinceDate: [Int: Self] = Dictionary(grouping: self) { (key) -> Int in
            // The rollingStartNumber is the unix timestamp divided by 600, giving the amount of 10-minute-intervals that passed since 01.01.1970 00:00 UTC.
            let startDate = Date(timeIntervalSince1970: Double(key.rollingStartNumber) * 600 )
            
            var calendar = Calendar(identifier: .gregorian)
            guard let utcTimeZone = TimeZone(secondsFromGMT: 0) else {
                //fatalError("Couldn't get the time zone, this is odd to append !!")
                os_log("Couldn't get the time zone, this is odd to append !!", log: Log.diagnosisKeys, type: .fault)
                return -1
            }
            calendar.timeZone = utcTimeZone
            
            guard let daysPassedSinceStartDate = calendar.dateComponents([.day], from: startDate, to: date).day else {
                os_log("Could't get the timelaps from the start date to the current date.", log: Log.diagnosisKeys, type: .fault)
                return -1
            }
            
            return daysPassedSinceStartDate
        }
        
        var processedDiagnosisKeys = [DiagnosisKey]()
        for (daysSinceDate, diagnosisKeys) in diagnosisKeyByTimelapsSinceDate where daysSinceDate >= 0 && daysSinceDate <= 14 {
            let sortedDKs = diagnosisKeys.sorted { (key1, key2) -> Bool in
                return key1.rollingPeriod > key2.rollingPeriod
            }
            var count = 0
            for key in sortedDKs {
                if count == 3 {
                    break
                }
                let startDate = Date(timeIntervalSince1970: Double(key.rollingStartNumber) * 600 )
                print(key.key.hexEncodedString())
                print(startDate)
                print(key)
                // The default transmission risk level to 1 is in android but aparently is overwritten to 0.
//                processedDiagnosisKeys.append(DiagnosisKey(key: key.key, rollingPeriod: key.rollingPeriod, rollingStartNumber: key.rollingStartNumber, transmissionRisk: 1))
                processedDiagnosisKeys.append(key)
                count += 1
            }
        }
        
        return processedDiagnosisKeys
    }
}

extension Array where Element == DiagnosisKey {
    
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return self.map { String(format: format, $0) }.joined()
    }
}

extension Sequence where Iterator.Element == DiagnosisKey {
    var asHMACsignature: Signature {
        let str = self
            .map({ ["\($0.key.base64EncodedString())",
                    "\($0.rollingStartNumber)",
                    "\($0.rollingPeriod)",
                    "\($0.transmissionRisk)"] })
            .map({ $0.joined(separator: ".") })
            .sorted()
            .joined(separator: ",")
            
        let data = str.data(using: .utf8) ?? Data()
        
        let signature = Crypto
            .signature(forData: data)
        
        return signature
    }
}

extension ENTemporaryExposureKey {
    var asDiagnosisKey: DiagnosisKey {
        return DiagnosisKey(key: keyData,
                            rollingPeriod: rollingPeriod,
                            rollingStartNumber: rollingStartNumber,
                            transmissionRisk: transmissionRiskLevel)
    }
}

extension Sequence where Iterator.Element == ENTemporaryExposureKey {
    var asDiagnosisKeys: [DiagnosisKey] {
        return self.map { $0.asDiagnosisKey }
    }
}

