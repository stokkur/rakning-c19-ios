//
//  ExposureConfiguration.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import ExposureNotification

struct ExposureConfiguration {
    public static let EXPOSURE_CONFIG_ID: String = "EXPOSURE_CONFIG_ID"
    // API V2 Keys
    let immediateDurationWeight: Double = 100
    let nearDurationWeight: Double = 55
    let mediumDurationWeight: Double = 0
    let otherDurationWeight: Double = 0
    let infectiousnessForDaysSinceOnsetOfSymptoms: [String: Int] = [
        "unknown":1,
        "-14":0,
        "-13":0,
        "-12":0,
        "-11":0,
        "-10":0,
        "-9":0,
        "-8":0,
        "-7":0,
        "-6":0,
        "-5":0,
        "-4":0,
        "-3":0,
        "-2":1,
        "-1":1,
        "0":1,
        "1":1,
        "2":1,
        "3":1,
        "4":1,
        "5":1,
        "6":1,
        "7":1,
        "8":1,
        "9":1,
        "10":0,
        "11":0,
        "12":0,
        "13":0,
        "14":0,
    ]
    let infectiousnessStandardWeight: Double = 100
    let infectiousnessHighWeight: Double = 100
    let reportTypeConfirmedTestWeight: Double = 100
    let reportTypeConfirmedClinicalDiagnosisWeight: Double = 100
    let reportTypeSelfReportedWeight: Double = 100
    let reportTypeRecursiveWeight: Double = 100
    let reportTypeNoneMap: Int = 1
    // API V1 Keys
    let minimumRiskScore: ENRiskScore = 1
    let attenuationDurationThresholds: [NSNumber] = [53, 63, 70]
    let attenuationLevelValues: [ENRiskLevelValue] = [1, 2, 3, 4, 5, 6, 7, 8]
    let daysSinceLastExposureLevelValues: [ENRiskLevelValue] = [1, 2, 3, 4, 5, 6, 7, 8]
    let durationLevelValues: [ENRiskLevelValue] = [1, 2, 3, 4, 5, 6, 7, 8]
    let transmissionRiskLevelValues: [ENRiskLevelValue] = [1, 2, 3, 4, 5, 6, 7, 8]
}

extension ExposureConfiguration {
    var asENExposureConfiguration: ENExposureConfiguration {
        let exposureConfiguration = ENExposureConfiguration()
        if ENManagerIsAvailable() {
            exposureConfiguration.immediateDurationWeight = self.immediateDurationWeight
            exposureConfiguration.nearDurationWeight = self.nearDurationWeight
            exposureConfiguration.mediumDurationWeight = self.mediumDurationWeight
            exposureConfiguration.otherDurationWeight = self.otherDurationWeight
            var infectiousnessForDaysSinceOnsetOfSymptoms = [Int: Int]()
            for (stringDay, infectiousness) in self.infectiousnessForDaysSinceOnsetOfSymptoms {
                if stringDay == "unknown" {
                    if #available(iOS 14.0, *) {
                        infectiousnessForDaysSinceOnsetOfSymptoms[ENDaysSinceOnsetOfSymptomsUnknown] = infectiousness
                    } else {
                        // ENDaysSinceOnsetOfSymptomsUnknown is not available
                        // in earlier versions of iOS; use an equivalent value
                        infectiousnessForDaysSinceOnsetOfSymptoms[NSIntegerMax] = infectiousness
                    }
                } else if let day = Int(stringDay) {
                    infectiousnessForDaysSinceOnsetOfSymptoms[day] = infectiousness
                }
            }
            exposureConfiguration.infectiousnessForDaysSinceOnsetOfSymptoms = infectiousnessForDaysSinceOnsetOfSymptoms as [NSNumber: NSNumber]
            exposureConfiguration.infectiousnessStandardWeight = self.infectiousnessStandardWeight
            exposureConfiguration.infectiousnessHighWeight = self.infectiousnessHighWeight
            exposureConfiguration.reportTypeConfirmedTestWeight = self.reportTypeConfirmedTestWeight
            exposureConfiguration.reportTypeConfirmedClinicalDiagnosisWeight = self.reportTypeConfirmedClinicalDiagnosisWeight
            exposureConfiguration.reportTypeSelfReportedWeight = self.reportTypeSelfReportedWeight
            exposureConfiguration.reportTypeRecursiveWeight = self.reportTypeRecursiveWeight
            if let reportTypeNoneMap = ENDiagnosisReportType(rawValue: UInt32(self.reportTypeNoneMap)) {
                exposureConfiguration.reportTypeNoneMap = reportTypeNoneMap
            }
        }
        exposureConfiguration.minimumRiskScore = self.minimumRiskScore
        exposureConfiguration.attenuationLevelValues = self.attenuationLevelValues as [NSNumber]
        exposureConfiguration.daysSinceLastExposureLevelValues = self.daysSinceLastExposureLevelValues as [NSNumber]
        exposureConfiguration.durationLevelValues = self.durationLevelValues as [NSNumber]
        exposureConfiguration.transmissionRiskLevelValues = self.transmissionRiskLevelValues as [NSNumber]
        exposureConfiguration.attenuationDurationThresholds = self.attenuationDurationThresholds
        exposureConfiguration.metadata = ["attenuationDurationThresholds": self.attenuationDurationThresholds]
        return exposureConfiguration
    }
    func withCutsomValuesFrom(coreDataManager: CoreDataManager, completion: @escaping (ENExposureConfiguration)->()){
        self.getPersistedExposureConfig(coreDataManager: coreDataManager) { exposureConfig in
            guard let expConfig = exposureConfig else {
                completion(asENExposureConfiguration)
                return
            }
            let newENConfig = asENExposureConfiguration
            newENConfig.immediateDurationWeight = expConfig.immediateDurationWeight
            newENConfig.nearDurationWeight = expConfig.nearDurationWeight
            newENConfig.mediumDurationWeight = expConfig.mediumDurationWeight
            newENConfig.otherDurationWeight = expConfig.otherDurationWeight

            let attenuationDurationThresholds: [NSNumber] = [
                NSNumber(value: expConfig.immediateThreshold),
                NSNumber(value: expConfig.nearThreshold),
                NSNumber(value: expConfig.mediumThreshold),
            ]

            newENConfig.minimumRiskScore = ENRiskScore(expConfig.minimumRiskScore)
            
            newENConfig.attenuationDurationThresholds = attenuationDurationThresholds
            newENConfig.metadata = ["attenuationDurationThresholds": attenuationDurationThresholds]
            completion(newENConfig)
        }
    }
    
    func createPersistedExposureConfig(coreDataManager: CoreDataManager, completion: @escaping (ExposureConfig?)->()){
        let newExposureConfig = ExposureConfig(
            id: UUID(),
            configId: ExposureConfiguration.EXPOSURE_CONFIG_ID,
            immediateDurationWeight: self.immediateDurationWeight,
            immediateThreshold: Int16(truncating: self.attenuationDurationThresholds[0]),
            nearDurationWeight: self.nearDurationWeight,
            nearThreshold: Int16(truncating: self.attenuationDurationThresholds[1]),
            mediumDurationWeight: self.mediumDurationWeight,
            mediumThreshold: Int16(truncating: self.attenuationDurationThresholds[2]),
            otherDurationWeight: self.otherDurationWeight,
            infectiousnessHighWeight: self.infectiousnessHighWeight,
            infectiousnessStandardWeight: self.infectiousnessStandardWeight,
            minimumRiskScore: Int16(self.minimumRiskScore)
        )
        coreDataManager.update(entities: [newExposureConfig]) { error in
            if let error = error {
                print("couldn't save new exposure config")
                print(error)
                completion(nil)
            } else {
                completion(newExposureConfig)
            }
        }
    }
    
    func getPersistedExposureConfig(coreDataManager: CoreDataManager, completion: @escaping (ExposureConfig?)->()) {
        coreDataManager.fetch(with: NSPredicate(format: "configId == %@", ExposureConfiguration.EXPOSURE_CONFIG_ID)) { (result: Result<[ExposureConfig], CoreDataManagerError>) in
            switch result {
            case .success(let exposureConfigs):
                guard let expConfig = exposureConfigs.first else {
                    print("Unable to get persisted EN config.")
                    self.createPersistedExposureConfig(coreDataManager: coreDataManager) { newExpConfig in
                        completion(newExpConfig)
                    }
                    return
                }
                completion(expConfig)
            case .failure( _):
                // try creating the EN config
                self.createPersistedExposureConfig(coreDataManager: coreDataManager) { _ in }
                completion(nil)
            }   
        }
    }
}
