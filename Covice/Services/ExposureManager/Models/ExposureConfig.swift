//
//  ExposureConfig.swift
//  Covice
//

import Foundation

struct ExposureConfig {
    var id: UUID
    var configId: String
    var immediateDurationWeight: Double
    var immediateThreshold: Int16
    var nearDurationWeight: Double
    var nearThreshold: Int16
    var mediumDurationWeight: Double
    var mediumThreshold: Int16
    var otherDurationWeight: Double
    var infectiousnessHighWeight: Double
    var infectiousnessStandardWeight: Double
    var minimumRiskScore: Int16
}
