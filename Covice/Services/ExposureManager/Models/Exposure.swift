//
//  Exposure.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import ExposureNotification
import UIKit

// TODO: Define the exposure data to save, what does the ui needs work correctly ?
struct Exposure {
    let id: UUID = UIDevice.current.identifierForVendor ?? UUID()
    let date: Date
    let infectiousness: ENInfectiousness
}

extension Sequence where Iterator.Element == ENExposureWindow {
    var asExposures: [Exposure] {
        return self.map { $0.asExposure }
    }
}
extension Sequence where Iterator.Element == ENExposureInfo {
    var asExposures: [Exposure] {
        return self.map { $0.asExposure }
    }
}

extension ENExposureWindow {
    var asExposure: Exposure {
        return Exposure(date: self.date, infectiousness: self.infectiousness)
    }
}
extension ENExposureInfo {
    var asExposure: Exposure {
        return Exposure(date: self.date, infectiousness: .standard)
    }
}
