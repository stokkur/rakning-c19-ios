//
//  StorageManageriOS12.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import os

class StorageManageriOS12: StorageManagerProtocol {
    typealias ExposureReturnType = Result<ExposureKeySet, NetworkResponseHandleError>
    
    var fileManager: FileManager
    required init(fileManager:FileManager = FileManager.default) {
        self.fileManager = fileManager
    }
    
    func createExposureKeySet(fromUrl url: URL, withIdentifier identifier: String) -> Result<ExposureKeySet, NetworkResponseHandleError> {
        os_log("Start create exposure key set", log: Log.storageManager, type: .info)
        guard let localUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(UUID().uuidString) else {
            os_log("Unable to create local url.", log: Log.storageManager, type: .error)
            return .failure(.cannotUnzip)
        }

        do {
            try fileManager.createDirectory(at: localUrl, withIntermediateDirectories: true, attributes: nil)
            try fileManager.unzipItem(at: url, to: localUrl)
        } catch let error {
            os_log("Unable to create directory and unzip file with error: %{public}s", log: Log.storageManager, type: .error, error.localizedDescription)
            return .failure(.cannotUnzip)
        }
        
        guard let storagePath = path(for: .exposureKeySets) else {
            os_log("Unable to get storage path for exposure key sets.", log: Log.storageManager, type: .error)
            return .failure(.cannotUnzip)
        }
        
        guard let keySet = ExposureKeySet.fromUrl(localUrl, withIdentifier: identifier, at: storagePath) else {
            os_log("Unable to get key set from url.", log: Log.storageManager, type: .error)
            return .failure(.cannotUnzip)
        }

        return .success(keySet)
    }
    
}
