//
//  FileManager.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Combine
import os

class StorageManager {
    enum LocalFolder {
        case temporary
        case documents
        case cache
        case exposureKeySets
    }
    
    private let fileManager: FileManager
    
    
    required init(fileManager:FileManager = FileManager.default) {
        self.fileManager = fileManager
    }
    
    func createExposureKeySet(fromUrl url: URL, withIdentifier identifier: String, _ completion: @escaping (Result<ExposureKeySet, NetworkResponseHandleError>) -> Void) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let this = self else {
                completion(.failure(.deinitError))
                return
            }
            
            os_log("Start create exposure key set", log: Log.storageManager, type: .info)
            guard let localUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(UUID().uuidString) else {
                os_log("Unable to create local url.", log: Log.storageManager, type: .error)
                completion(.failure(.cannotUnzip))
                return
            }

            do {
                try this.fileManager.createDirectory(at: localUrl, withIntermediateDirectories: true, attributes: nil)
                try this.fileManager.unzipItem(at: url, to: localUrl)
            } catch let error {
                os_log("Unable to create directory and unzip file with error: %{public}s", log: Log.storageManager, type: .error, error.localizedDescription)
                completion(.failure(.cannotUnzip))
                return
            }
            
            guard let storagePath = this.path(for: .exposureKeySets) else {
                os_log("Unable to get storage path for exposure key sets.", log: Log.storageManager, type: .error)
                completion(.failure(.cannotUnzip))
                return
            }
            
            guard let keySet = ExposureKeySet.fromUrl(localUrl, withIdentifier: identifier, at: storagePath) else {
                os_log("Unable to get key set from url.", log: Log.storageManager, type: .error)
                completion(.failure(.cannotUnzip))
                return
            }

            completion(.success(keySet))
        }
    }
    
    @available(iOS 13.0, *)
    func createExposureKeySet(fromUrl url: URL, withIdentifier identifier: String) -> AnyPublisher<ExposureKeySet, NetworkResponseHandleError> {
        os_log("Start create exposure key set", log: Log.storageManager, type: .info)
        guard let localUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(UUID().uuidString) else {
            os_log("Unable to create local url.", log: Log.storageManager, type: .error)
            return Fail(error: .cannotUnzip).eraseToAnyPublisher()
        }

        do {
            try fileManager.createDirectory(at: localUrl, withIntermediateDirectories: true, attributes: nil)
            try fileManager.unzipItem(at: url, to: localUrl)
        } catch let error {
            os_log("Unable to create directory and unzip file with error: %{public}s", log: Log.storageManager, type: .error, error.localizedDescription)
            return Fail(error: .cannotUnzip).eraseToAnyPublisher()
        }
        
        guard let storagePath = path(for: .exposureKeySets) else {
            os_log("Unable to get storage path for exposure key sets.", log: Log.storageManager, type: .error)
            return Fail(error: .cannotUnzip).eraseToAnyPublisher()
        }
        
        guard let keySet = ExposureKeySet.fromUrl(localUrl, withIdentifier: identifier, at: storagePath) else {
            os_log("Unable to get key set from url.", log: Log.storageManager, type: .error)
            return Fail(error: .cannotUnzip).eraseToAnyPublisher()
        }

        return Just(keySet)
            .setFailureType(to: NetworkResponseHandleError.self)
            .eraseToAnyPublisher()
    }
    
    
    
    private func path(for folder: LocalFolder) -> URL? {
        let path: URL?
        os_log("Get path for folder.", log: Log.storageManager, type: .info)
        switch folder {
        case .cache:
            path = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first
        case .documents:
            path = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
        case .temporary:
            path = fileManager.urls(for: .itemReplacementDirectory, in: .userDomainMask).first
        case .exposureKeySets:
            path = self.path(for: .documents)?.appendingPathComponent("exposureKeySets")
        }

        guard let finalPath = path else {
            os_log("Unable to get path.", log: Log.storageManager, type: .fault)
            return nil
        }

        return createFolder(with: finalPath) ? finalPath : nil
    }
    
    private func createFolder(with url: URL) -> Bool {
        os_log("Create folder with url: %{public}s", log: Log.storageManager, type: .info, url.absoluteString)
        do {
            try fileManager.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
            return true
        } catch let error {
            os_log("Unable to create direcotry with error: %{public}s", log: Log.storageManager, type: .error, error.localizedDescription)
            return false
        }
    }
    
    func deleteDiagnosisKeyFilesInTempFolder() {
        os_log("Delete diagnosis key files.", log: Log.storageManager, type: .info)
        do {
            let filesPaths = try FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory())
            filesPaths.forEach { (filePath) in
                try? FileManager.default.removeItem(atPath: filePath)
            }
        } catch let error {
            os_log("Unable to remove files with error: %{public}s", log: Log.storageManager, type: .fault, error.localizedDescription)
        }
    }
}

