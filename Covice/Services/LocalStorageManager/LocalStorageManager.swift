//
//  LocalSotrageManager.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import CoreData
import os

class LocalStorageManager {
    
    var lastDateExposureDetectionPerformed: Persisted<Date?> = Persisted<Date?>(userDefaultsKey: "lastDateExposureDetectionPerformed", notificationName: .init("LocalStoreDateLastPerformedExposureDetectionDidChange"), defaultValue: nil)
    
    var lastDiagnosisKeySet: Persisted<String?> = Persisted<String?>(userDefaultsKey: "lastDiagnosisKeySetKey", notificationName: .init("LocalStoreDateLastPerformedExposureDetectionDidChange"), defaultValue: nil)
    
    var revisionToken: Persisted<String?> = Persisted<String?>(userDefaultsKey: "revisionToken", notificationName: .init("RevisionTokenDidChange"), defaultValue: nil)
    
    var registeredPhoneNumber: Persisted<String?> = Persisted<String?>(userDefaultsKey: "registeredPhoneNumber", notificationName: .init("RegisteredPhoneNumberDidChange"), defaultValue: nil)
    
    var onboardingCurrentScreen: Persisted<Int> = Persisted<Int>(userDefaultsKey: "onboardingCurrentScreen", notificationName: .init("OnboardingCurrentScreenDidChane"), defaultValue: 0)
    
    var isOnboarded: Persisted<Bool> = Persisted<Bool>(userDefaultsKey: "isOnboarded", notificationName: .init("LocalStoreIsOnboardedDidChange"), defaultValue: false)
    
    var hasPhoneBeenReset: Persisted<Bool> = Persisted<Bool>(userDefaultsKey: "hasPhoneBeenResetNew", notificationName: .init("PhoneResetDidChange"), defaultValue: false)
    
    var showNegativeTestResult: Persisted<Bool> = Persisted<Bool>(userDefaultsKey: "showNegativeTestResult", notificationName: .init("ShowNegativeTestResult"), defaultValue: false)
    
}


