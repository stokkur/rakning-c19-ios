//
//  ExposureBGTask.swift
//  Covice

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import BackgroundTasks
import ExposureNotification
import os
import UserNotifications

extension AppDelegate {

    func iOS12DetectExposure(){
        os_log("Starting background fetching ios 12", log: Log.backgroundTask, type: .info)
        self.exposureManager.showBluetoothOffUserNotificationIfNeeded()
        self.handleExposureKeySets() { result in
            switch result {
            case .success(let exposure):
                os_log("Found %{public}i exposures", log: Log.backgroundTask, type: .info)
                if exposure != nil {
                    LocalNotification.sendLocalNotification(title: Localization.notification_exposure_title, body: Localization.notification_exposure_body)
                }
            case .failure(let error):
                os_log("Failed to handle exposure keysets with error: %{public}s", log: Log.exposureManager, type: .error, error.localizedDescription)
            }
        }
    }
    
    func createBackgroundTaskIfNeeded() {
        if #available(iOS 13.0, *) {
            os_log("Registering background task", log: Log.backgroundTask, type: .info)
            BGTaskScheduler.shared.register(
                forTaskWithIdentifier: backgroundTaskIdentifier,
                using: .main)
            { [weak self] (bgTask) in
                guard let this = self else {
                    os_log("Unable to get self reference.", log: Log.backgroundTask, type: .info)
                    bgTask.setTaskCompleted(success: false)
                    return
                }
                os_log("Starting background task", log: Log.backgroundTask, type: .info)
                this.exposureManager.showBluetoothOffUserNotificationIfNeeded()
                this.handleExposureKeySets() { result in
                    switch result {
                    case .success(let exposure):
                        os_log("Found %{public}i exposure", log: Log.backgroundTask, type: .info)
                        if exposure != nil {
                            LocalNotification.sendLocalNotification(title: Localization.notification_exposure_title, body: Localization.notification_exposure_body)
                        }
                        bgTask.setTaskCompleted(success: true)
                    case .failure(let error):
                        os_log("Failed to handle exposure keysets with error: %{public}s", log: Log.exposureManager, type: .error, error.localizedDescription)
                        bgTask.setTaskCompleted(success: false)
                    }
                }
                    
                bgTask.expirationHandler = {
                    os_log("The detection background task has timed out.", log: Log.backgroundTask, type: .fault)
                }
                
                this.scheduleBackgroundTaskIfNeeded()
            }
        }
    }

    func scheduleBackgroundTaskIfNeeded() {
        if #available(iOS 13.5, *) {
            guard ENManager.authorizationStatus == .authorized else { return }
            let taskRequest = BGProcessingTaskRequest(identifier: backgroundTaskIdentifier)
            taskRequest.requiresNetworkConnectivity = true
            do {
                try BGTaskScheduler.shared.submit(taskRequest)
            } catch {
                print("Unable to schedule background task: \(error)")
            }
        }
    }
    
    func handleExposureKeySets(completionHandler: @escaping (Result<Exposure?, ExposureDetectionError>) -> Void) {
        let newestDiagnosedKeySet = self.localStorageManager.lastDiagnosisKeySet.wrappedValue
        
        self.networkManager.getKeySets(after: newestDiagnosedKeySet) { [weak self] result in
            guard let self = self else { return }
            
            self.storageManager.deleteDiagnosisKeyFilesInTempFolder()
            self.localStorageManager.lastDateExposureDetectionPerformed.wrappedValue = Date()
        
            switch result {
            case .success(let keySets):
                ExposureConfiguration().withCutsomValuesFrom(coreDataManager: self.coreDataManager) { config in
                    // Ignore Progress returned, can be used to cancel when BG task timeouts
                    print(config)
                    let _ = self.exposureManager.detectExposures(configuration: config, exposureKeySets: keySets) { [weak self] (result) in
                        guard let this = self else {
                            completionHandler(.failure(.internalDetectionError))
                            return
                        }
                        switch result {
                        case .success(let detectedExposures):
                            this.storageManager.deleteDiagnosisKeyFilesInTempFolder()
                            this.localStorageManager.lastDateExposureDetectionPerformed.wrappedValue = Date()
                            this.saveExposures(detectedExposures) { (result) in
                                    switch result {
                                    case .success(let newExposure):
                                        if let lastDiagnosisKeySet = keySets.last?.identifier {
                                            this.localStorageManager.lastDiagnosisKeySet.wrappedValue = lastDiagnosisKeySet
                                        }
                                        completionHandler(.success(newExposure))
                                        break
                                    case .failure(let error):
                                        completionHandler(.failure(error))
                                        break
                                    }
                            }
                        case .failure(let error):
                            completionHandler(.failure(error))
                        }
                    }
                }
            case .failure(_):
                completionHandler(.failure(.unableToGetKeys))
                break
            }
        }
    }
    
    private func saveExposures(_ exposures: [Exposure], completionHandler: @escaping (Result<Exposure?, ExposureDetectionError>) -> Void) {
        self.coreDataManager.fetch() { (result: Result<[Exposure], CoreDataManagerError>) in
            switch result {
            case .success(let storedExposures):
                guard let newest = (storedExposures + exposures).max(by: { $0.date < $1.date }) else {
                    completionHandler(.success(nil))
                    return
                }
                
                if storedExposures.isEmpty || storedExposures.contains(where: { Calendar.current.compare($0.date, to: newest.date, toGranularity: .hour) == .orderedAscending }) {
                    self.coreDataManager.update(entities: [newest]) { error in
                        if let error = error {
                            completionHandler(.failure(.storageError(error)))
                            return
                        }
                        
                        completionHandler(.success(newest))
                    }
                } else {
                    completionHandler(.success(nil))
                }
            case .failure(let error):
                completionHandler(.failure(.storageError(error)))
            }
        }
    }
}

// MARK: - iOS 12.5 support configuration for background fetching in oldest version supported.
/// Activities that occurred while the app wasn't running.
struct ENActivityFlags: OptionSet {
    let rawValue: UInt32

    /// App launched to perform periodic operations.
    static let periodicRun = ENActivityFlags(rawValue: 1 << 2)
}

/// Invoked after the app is launched to report activities that occurred while the app wasn't running.
typealias ENActivityHandler = (ENActivityFlags) -> Void

extension ENManager {
    
    /// On iOS 12.5 only, this will ensure the app receives 3.5 minutes of background processing
    /// every 4 hours. This function is needed on iOS 12.5 because the BackgroundTask framework, used
    /// for Exposure Notifications background processing in iOS 13.5+ does not exist in iOS 12.
    func setLaunchActivityHandler(activityHandler: @escaping ENActivityHandler) {
        let proxyActivityHandler: @convention(block) (UInt32) -> Void = {integerFlag in
            activityHandler(ENActivityFlags(rawValue: integerFlag))
        }
        
        setValue(proxyActivityHandler, forKey: "activityHandler")
    }
}
