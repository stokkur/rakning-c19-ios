//
//  Log.swift
//  Covice

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import os

private let subsystem = "is.landlaeknir.rakning"

struct Log {
    static let networkManger = OSLog(subsystem: subsystem, category: "networkManager")
    static let exposureManager = OSLog(subsystem: subsystem, category: "exposure manager")
    static let localStorage = OSLog(subsystem: subsystem, category: "local storage")
    static let backgroundTask = OSLog(subsystem: subsystem, category: "background task")
    static let storageManager = OSLog(subsystem: subsystem, category: "storage manager")
    static let registration = OSLog(subsystem: subsystem, category: "registration")
    static let diagnosisKeys = OSLog(subsystem: subsystem, category: "diagnosis keys")
}
