//
//  LocalNotification.swift
//  Covice

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UserNotifications

class LocalNotification {
    
    enum LocalIdentifier {
        case ExposureDetected
        case BluetoothOff
        case PhoneReregistration
        
        var identifier: String {
            switch self {
            case .ExposureDetected:
                return "exposure_detected"
            case .BluetoothOff:
                return "bluetoothOff"
            case .PhoneReregistration:
                return "userInfo"
            }
        }
    }
    
    static func sendLocalNotification(title: String, body: String, identifier: LocalIdentifier = .ExposureDetected) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = .defaultCritical
        content.categoryIdentifier = identifier.identifier
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request)
    }
}
