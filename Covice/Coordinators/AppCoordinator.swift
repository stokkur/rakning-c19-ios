//
//  AppCoordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import UIKit
import ExposureNotification
import Firebase

class AppCoordinator: Coordinator {
    required init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.navigationController = navigationController
        self.theme = theme
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.storageManager = storageManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
    }

    var coreDataManager: CoreDataManager    
    
    var networkManager: NetworkManager
    
    var exposureManager: ExposureManager
    
    var storageManager: StorageManager
    
    var localStorageManager: LocalStorageManager
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    var theme: Theme
    
    // MARK: - Public
    @discardableResult func start() -> UINavigationController {
        if !localStorageManager.isOnboarded.wrappedValue && navigationController.presentedViewController == nil {
            onboardingFlow()
        } else {
            mainFlow()
        }
        
        checkForUpdates()
    
        return self.navigationController
    }
}


extension AppCoordinator {
    fileprivate func checkForUpdates() {
        let appDistribution = AppDistribution.appDistribution()
        
        guard appDistribution.isTesterSignedIn else { return }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            appDistribution.checkForUpdate(completion: { [weak self] release, error in
                guard let release = release, let controller = self?.navigationController.viewControllers.last, let this = self else {
                    return
                }
                
                // Customize your alerts here.
                let title = Localization.update_version_title
                let message = String(format: Localization.upadte_version_body, release.displayVersion, release.buildVersion)
                let alert = CustomAlertController(theme: this.theme, title: title, content: message)
                
                let cancel =  Button(title: Localization.btn_cancel, theme: this.theme)
                cancel.style = .ghost
                alert.addAction(button: cancel)
                
                let update = Button(title: Localization.update_button, theme: this.theme)
                update.action = {
                    UIApplication.shared.open(release.downloadURL)
                }
                alert.addAction(button: update)
                
                controller.present(alert, animated: true)
            })
        }
        
        

    }
}

// MARK: - Onboarding Flow
extension AppCoordinator: OnboardingCoordinatorDelegate {
    fileprivate func onboardingFlow() {
        let coordinator = OnboardingCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        coordinator.delegate = self
        coordinator.start()
        
        self.addChildCoordinator(coordinator)
    }
    
    func didFinish(_ onboardingCoordinator: OnboardingCoordinator) {
        localStorageManager.isOnboarded.wrappedValue = true

        mainFlow()

        self.removeChildCoordinator(onboardingCoordinator)
    }
}

// MARK: - Main Flow
extension AppCoordinator: MainCoordinatorDelegate {
    fileprivate func mainFlow() {
        let coordinator = MainCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        coordinator.delegate = self
        coordinator.start()
        
        self.addChildCoordinator(coordinator)
    }

    func didFinish(_ mainCoordinator: MainCoordinator) {
        onboardingFlow()

        self.removeChildCoordinator(mainCoordinator)
    }
}

