//
//  Coordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol Coordinator: class {
    init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager)
    
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    var theme: Theme { get set }
    var networkManager: NetworkManager { get set }
    var exposureManager: ExposureManager { get set }
    var storageManager: StorageManager { get set }
    var localStorageManager: LocalStorageManager { get set }
    var coreDataManager: CoreDataManager { get set }

    @discardableResult func start() -> UINavigationController
}

extension Coordinator {
    func addChildCoordinator(_ childCoordinator: Coordinator) {
        self.childCoordinators.append(childCoordinator)
    }
    
    func removeChildCoordinator(_ childCoordinator: Coordinator){
        self.childCoordinators = self.childCoordinators.filter{ $0 !== childCoordinator }
    }
}
