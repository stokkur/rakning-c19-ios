//
//  OnboardingConsentAndPrivacyView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol OnboardingConsentAndPrivacyViewDelegate {
    func didPressLanguageChange(_ onboardingConsentAndPrivacyView: OnboardingConsentAndPrivacyView)
    func didPressNotNow(_ onboardingConsentAndPrivacyView: OnboardingConsentAndPrivacyView)
    func didPressTurnOn(_ onboardingConsentAndPrivacyView: OnboardingConsentAndPrivacyView)
}

class OnboardingConsentAndPrivacyView: View {
    
    lazy var gradientView: GradientView = {
        let view  = GradientView(theme: theme)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.colors = [
            theme.colors.lightOrange.withAlphaComponent(0),
            theme.colors.lightOrange,
            
        ]
        view.locations = [0, 0.7]
        
        return view
    }()
    
    lazy var backgroundDecoration: UIImageView = {
        let view = UIImageView(image: UIImage.backgroundDecoration)
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = .pi * 2.0
        rotateAnimation.duration = 20
        rotateAnimation.repeatCount = .greatestFiniteMagnitude
        rotateAnimation.isRemovedOnCompletion = false

        view.layer.add(rotateAnimation, forKey: nil)
        
        return view
    }()
    
    lazy var appLogo: UIImageView = {
        let view = UIImageView(image: UIImage.appLogo)
        view.contentMode = .scaleAspectFit
        view.layer.shadowColor = theme.colors.darkOrange.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .init(width: 0, height: 10.scaled)
        view.layer.shadowRadius = 10.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var languageSelectionView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = 0
        let background = UIView(frame: view.bounds)
        background.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        background.backgroundColor = .clear
        background.layer.borderColor = theme.colors.darkGray.cgColor
        background.layer.borderWidth = 1
        background.layer.cornerRadius = 10.scaled
        view.addSubview(background)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var languageLabelView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var languageLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14.scaled, weight: .regular)
        view.text = Localization.language
        view.clipsToBounds = true
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var languagePickerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var languageFlag: UIImageView = {
        let view = UIImageView(image: self.selectedLanguage.icon)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var languagePicker: Picker = {
        let view = Picker(theme: theme)
        view.backgroundColor = .clear
        view.font = .systemFont(ofSize: 14.scaled, weight: .regular)
        view.textColor = theme.colors.darkBlue
        view.text = self.selectedLanguage.asText
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    lazy var notNow: Button = {
        let view = Button(title: Localization.btn_not_now, theme: theme)
        view.style = .info
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressNotNow(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var consent: Button = {
        let view = Button(title: Localization.btn_consent, theme: theme)
        view.style = .primary
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressTurnOn(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    lazy var tableView = OnboardingConsentAndPrivacyTableView()
    
    var selectedLanguage: Bundle.SupportedLanguage
    var delegate: OnboardingConsentAndPrivacyViewDelegate?
    
    init(theme: Theme, with selectedLanguage: Bundle.SupportedLanguage) {
        self.selectedLanguage = selectedLanguage
        print("Selected Lanugage: \(self.selectedLanguage)")
        super.init(theme: theme)
        tableView.isUserInteractionEnabled = true
    }
    
    override func build() {
        super.build()
        
        addSubview(backgroundDecoration)
        addSubview(tableView)
        addSubview(appLogo)
        addSubview(notNow)
        addSubview(consent)
        addSubview(languageSelectionView)
        
        languageSelectionView.addArrangedSubview(languageLabelView)
        languageSelectionView.addArrangedSubview(separatorView)
        languageSelectionView.addArrangedSubview(languagePickerView)
        
        languageLabelView.addSubview(languageLabel)
        
        languagePickerView.addSubview(languageFlag)
        languagePickerView.addSubview(languagePicker)
        addSubview(gradientView)
    }

    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            backgroundDecoration.topAnchor.constraint(equalTo: self.topAnchor, constant: -100.scaled),
            backgroundDecoration.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: -145.scaled),
        ])

        NSLayoutConstraint.activate([
            appLogo.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            appLogo.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 58.scaled),
            appLogo.heightAnchor.constraint(equalToConstant: 74.scaled),
            appLogo.widthAnchor.constraint(equalToConstant: 74.scaled),
        ])
        
        NSLayoutConstraint.activate([
            languageSelectionView.topAnchor.constraint(equalTo: appLogo.bottomAnchor, constant: 24.scaled),
            languageSelectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            languageSelectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            languageSelectionView.heightAnchor.constraint(greaterThanOrEqualToConstant: 54.scaled),
            languageSelectionView.heightAnchor.constraint(lessThanOrEqualToConstant: 58.scaled),
        ])
        NSLayoutConstraint.activate([
            languageLabelView.widthAnchor.constraint(equalToConstant: 117.scaled)
        ])
        NSLayoutConstraint.activate([
            languageLabel.centerYAnchor.constraint(equalTo: languageLabelView.centerYAnchor),
            languageLabel.leadingAnchor.constraint(equalTo: languageLabelView.leadingAnchor, constant: 10.scaled),
            languageLabel.trailingAnchor.constraint(equalTo: languageLabelView.trailingAnchor, constant: -20.scaled)
        ])
        NSLayoutConstraint.activate([
            separatorView.widthAnchor.constraint(equalToConstant: 1.scaled),
            separatorView.heightAnchor.constraint(equalTo: languageSelectionView.heightAnchor)
        ])
        NSLayoutConstraint.activate([
            languagePickerView.widthAnchor.constraint(greaterThanOrEqualToConstant: 200.scaled)
        ])
        NSLayoutConstraint.activate([
            languageFlag.widthAnchor.constraint(equalTo: languageFlag.heightAnchor, multiplier: 7/5),
            languageFlag.leadingAnchor.constraint(equalTo: languagePickerView.leadingAnchor, constant: 14.scaled),
            languageFlag.heightAnchor.constraint(equalToConstant: 20.scaled),
            languageFlag.centerYAnchor.constraint(equalTo: languagePickerView.centerYAnchor)
        ])
        NSLayoutConstraint.activate([
            languagePicker.centerYAnchor.constraint(equalTo: languagePickerView.centerYAnchor),
            languagePicker.leadingAnchor.constraint(equalTo: languageFlag.trailingAnchor, constant: 14.scaled),
            languagePicker.trailingAnchor.constraint(equalTo: languagePickerView.trailingAnchor, constant: -14.scaled)
        ])
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: languageSelectionView.bottomAnchor,constant: 5.scaled),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            tableView.bottomAnchor.constraint(equalTo: notNow.topAnchor, constant: -10.scaled)
        ])

        NSLayoutConstraint.activate([
            gradientView.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: -20.scaled),
            gradientView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            gradientView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            gradientView.heightAnchor.constraint(equalToConstant: 30.scaled)
        ])
        
        NSLayoutConstraint.activate([
            notNow.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            notNow.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
        
        NSLayoutConstraint.activate([
            consent.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            consent.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
    }
    
    func reloadView(with newSelectedLanguage: Bundle.SupportedLanguage) {
        self.selectedLanguage = newSelectedLanguage
        self.setNeedsDisplay()
        languageLabel.text = Localization.language
        languageFlag.image = self.selectedLanguage.icon
        languagePicker.text = self.selectedLanguage.asText
        languagePicker.selectRow(self.selectedLanguage.rawValue)
        languagePicker.reloadView()
        consent.title = Localization.btn_consent
        notNow.title = Localization.btn_not_now
        tableView.reloadData()
    }
}

extension OnboardingConsentAndPrivacyView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        /*if #available(iOS 13, *){
            self.delegate?.didPressLanguageChange(self)
            return false
        }*/
        return true
    }
}
