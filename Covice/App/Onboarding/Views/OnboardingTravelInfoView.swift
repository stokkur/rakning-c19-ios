//
//  OnboardingTravelInfoView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

// MARK: - Removed for now


//protocol OnboardingTravelInfoViewDelegate {
//    func didPressPhoneSingUp()
//    func didPressSkip()
//}
//
//class OnboardingTravelInfoView: View {
//    init(theme: Theme, onboardingGenericInfoType: OnboardingGenericInfoType) {
//        self.onboardingGenericInfoType = onboardingGenericInfoType
//        super.init(theme: theme)
//    }
//
//    lazy var logo: UIImageView = {
//        let view = UIImageView(image: onboardingGenericInfoType.image)
//        view.contentMode = .scaleAspectFit
//        view.layer.shadowColor = theme.colors.darkOrange.cgColor
//        view.layer.shadowOpacity = 0.4
//        view.layer.shadowOffset = .init(width: 0, height: 10)
//        view.layer.shadowRadius = 10
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//
//    lazy var title: UILabel = {
//        let view = UILabel()
//        view.text = onboardingGenericInfoType.title
//        view.textColor = theme.colors.darkBlue
//        view.font = .systemFont(ofSize: 9.scaled, weight: .bold)
//        view.translatesAutoresizingMaskIntoConstraints = false
//        view.numberOfLines = 0
//        view.lineBreakMode = .byWordWrapping
//        view.textAlignment = .center
//        return view
//    }()
//
//    lazy var body: UILabel = {
//        let view = UILabel()
//        view.text = onboardingGenericInfoType.content
//        view.textColor = theme.colors.darkBlue
//        view.font = .systemFont(ofSize: 5.scaled, weight: .regular)
//        view.numberOfLines = 0
//        view.lineBreakMode = .byWordWrapping
//        view.textAlignment = .center
//        view.translatesAutoresizingMaskIntoConstraints = false
//
//        return view
//    }()
//
//
//    lazy var phoneSignUpBtn: Button = {
//        let view = Button(title: Localization.onboarding_travel_add_phone, theme: theme)
//        view.style = .ghost
//        view.titleLabel?.font = .systemFont(ofSize: 9.scaled, weight: .bold)
//        view.action = { [weak self] in
//            guard let this = self else { return }
//            this.delegate?.didPressPhoneSingUp()
//        }
//        view.translatesAutoresizingMaskIntoConstraints = false
//
//        return view
//    }()
//
//    lazy var skipBtn: Button = {
//        let view = Button(title: Localization.onboarding_travel_skip, theme: theme)
//        view.style = .primary
//        view.titleLabel?.font = .systemFont(ofSize: 9.scaled, weight: .bold)
//        view.action = { [weak self] in
//            guard let this = self else { return }
//            this.delegate?.didPressSkip()
//        }
//        view.translatesAutoresizingMaskIntoConstraints = false
//
//        return view
//    }()
//
//    var delegate: OnboardingTravelInfoViewDelegate?
//    let onboardingGenericInfoType: OnboardingGenericInfoType
//
//    override func build() {
//        super.build()
//
//        addSubview(logo)
//        addSubview(title)
//        addSubview(body)
//        addSubview(phoneSignUpBtn)
//        addSubview(skipBtn)
//    }
//
//    override func setupConstraints() {
//        super.setupConstraints()
//
//        NSLayoutConstraint.activate([
//            logo.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 45.scaled),
//            logo.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 30.scaled),
//            logo.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -30.scaled),
//            logo.heightAnchor.constraint(equalTo: logo.widthAnchor, constant: 0.scaled)
//        ])
//
//        NSLayoutConstraint.activate([
//            title.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 15.scaled),
//            title.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 54.scaled),
//            title.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -54.scaled)
//        ])
//
//        NSLayoutConstraint.activate([
//            body.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 23.scaled),
//            body.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 54.scaled),
//            body.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -54.scaled)
//        ])
//
//        NSLayoutConstraint.activate([
//            phoneSignUpBtn.bottomAnchor.constraint(equalTo: skipBtn.topAnchor, constant: -20.scaled),
//            phoneSignUpBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 70.scaled),
//            phoneSignUpBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -70.scaled)
//        ])
//
//        NSLayoutConstraint.activate([
//            skipBtn.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -35.scaled),
//            skipBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 70.scaled),
//            skipBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -70.scaled)
//        ])
//
//    }
//}
