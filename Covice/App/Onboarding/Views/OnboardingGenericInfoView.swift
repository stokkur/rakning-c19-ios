//
//  OnboardingHelpInfoView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol OnboardingGenericInfoViewDelegate {
    func didPressContinue()
}

class OnboardingGenericInfoView: View {
    
    lazy var genericInfoLogo: UIImageView = {
        let view = UIImageView(image: onboardingGenericInfoType.image)
        view.contentMode = .scaleAspectFit
        view.layer.shadowColor = theme.colors.darkOrange.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .init(width: 0, height: 10.scaled)
        view.layer.shadowRadius = 10.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var genericTitle: UILabel = {
        let view = UILabel()
        view.text = onboardingGenericInfoType.title
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 18.scaled, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .center
        return view
    }()
    
    lazy var genericBody: UILabel = {
        let view = UILabel()
        view.text = onboardingGenericInfoType.content
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 18.scaled, weight: .regular)
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var continueBtn: Button = {
        let view = Button(title: Localization.btn_continue, theme: theme)
        view.style = .primary
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressContinue()
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var delegate: OnboardingGenericInfoViewDelegate?
    let onboardingGenericInfoType: OnboardingGenericInfoType
    
    init(theme: Theme, onboardingGenericInfoType: OnboardingGenericInfoType) {
        self.onboardingGenericInfoType = onboardingGenericInfoType
        super.init(theme: theme)
    }
    
    override func build() {
        super.build()
        
        addSubview(genericInfoLogo)
        addSubview(genericTitle)
        addSubview(genericBody)
        addSubview(continueBtn)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            genericInfoLogo.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 40.scaled),
            genericInfoLogo.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            genericInfoLogo.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4),
            genericInfoLogo.widthAnchor.constraint(equalTo: self.heightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            genericTitle.topAnchor.constraint(equalTo: genericInfoLogo.bottomAnchor, constant: 40.scaled),
            genericTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            genericTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled)
        ])
        
        NSLayoutConstraint.activate([
            genericBody.topAnchor.constraint(equalTo: genericTitle.bottomAnchor, constant: 20.scaled),
            genericBody.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            genericBody.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled)
        ])
        
        NSLayoutConstraint.activate([
            continueBtn.topAnchor.constraint(greaterThanOrEqualTo: genericBody.bottomAnchor, constant: 30.scaled),
            continueBtn.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -20.scaled),
            continueBtn.centerXAnchor.constraint(equalTo: self.centerXAnchor),
        ])   
    }
}
