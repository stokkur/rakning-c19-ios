//
//  OnboardingTravelInfoController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

// MARK: - Removed for now

//protocol OnboardingTravelInfoControllerDelegate {
//    func didFinish(_ pressingSkip: Bool)
//}
//
//class OnboardingTravelInfoController: ViewController {
//
//    override init(theme: Theme) {
//        self.onboardingGenericInfoType = .travel
//        super.init(theme: theme)
//    }
//
//    var delegate: OnboardingTravelInfoControllerDelegate?
//    let onboardingGenericInfoType: OnboardingGenericInfoType
//
//    override func loadView() {
//        let onboardingGenericInfoView = OnboardingTravelInfoView(theme: theme,onboardingGenericInfoType: onboardingGenericInfoType)
//
//        onboardingGenericInfoView.delegate = self
//
//        self.internalView = onboardingGenericInfoView
//    }
//
//}
//
//extension OnboardingTravelInfoController: OnboardingTravelInfoViewDelegate {
//    func didPressPhoneSingUp() {
//        // TODO: present next phone sign up screen
//        self.delegate?.didFinish(false)
//    }
//
//    func didPressSkip() {
//        // TODO: Skip phone sign up.
//        self.delegate?.didFinish(true)
//    }
//
//}
