//
//  OnboardingConsentAndPrivacyController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import ExposureNotification

extension Bundle.SupportedLanguage {
    var icon: UIImage? {
        switch self {
        case .english: return UIImage.englishFlag
        case .icelandic: return UIImage.icelandicFlag
        case .polish: return UIImage.polskiFlag
        }
    }
    
    var asText: String {
        switch self {
        case .english: return Localization.language_english
        case .icelandic: return Localization.language_icelandic
        case .polish: return Localization.language_polish
        }
    }
}

protocol OnboardingConsentAndPrivacyControllerDelegate {
    func didFinish(_ OnboardingConsentAndPrivacyController: OnboardingConsentAndPrivacyController)
}

class OnboardingConsentAndPrivacyController: ViewController {
    enum OnboardingConsentAndPrivacyRow: Int, CaseIterable {
        case consentPrivacy
                
        var icon: UIImage? {
            switch self {
                case .consentPrivacy: return UIImage.exposureOnboarding
            }
        }
                
        var title: String {
            switch self {
            case .consentPrivacy: return  Localization.onboarding_consent_title
            }
        }
            
        var bold1: String { return Localization.onboarding_consent_bold_1 }
        var body1: String { return Localization.onboarding_consent_body_1 }
        var body2: String { return Localization.onboarding_consent_body_2 }
        var body3: String { return Localization.onboarding_consent_body_3 }
        var body4: String { return Localization.onboarding_consent_body_4 }
        var body5: String { return Localization.onboarding_consent_body_5 }
        var body6: String { return Localization.onboarding_consent_body_6 }
        var body7: String { return Localization.onboarding_consent_body_7 }
        var body8: String { return Localization.onboarding_consent_body_8 }
        var body9: String { return Localization.onboarding_consent_body_9 }
        var body10: String { return Localization.onboarding_consent_body_10 }
        var body11: String { return Localization.onboarding_consent_body_11 }
        var link1: String { return Localization.onboarding_consent_link_1 }
        var link2: String { return Localization.onboarding_consent_link_2 }
        var subtitle1: String { return Localization.onboarding_consent_subtitle_1 }
        var subtitle2: String { return Localization.onboarding_consent_subtitle_2 }
        var subtitle3: String { return Localization.onboarding_consent_subtitle_3 }
        var subtitle4: String { return Localization.onboarding_consent_subtitle_4 }
        var subtitle5: String { return Localization.onboarding_consent_subtitle_5 }
        var subtitle6: String { return Localization.onboarding_consent_subtitle_6 }
        var subtitle7: String { return Localization.onboarding_consent_subtitle_7 }
        
    }
    
    let supportedLanguages: [Bundle.SupportedLanguage] = Bundle.SupportedLanguage.allCases
    let exposureManager: ExposureManager
    let localStorageManager: LocalStorageManager
    var selectedLanguage: Bundle.SupportedLanguage
    var delegate: OnboardingConsentAndPrivacyControllerDelegate?
    
    init(theme: Theme, exposureManager: ExposureManager, localStorageManager: LocalStorageManager) {
        self.exposureManager = exposureManager
        self.localStorageManager = localStorageManager
        let appLangCode = Bundle.getSelectedAppLangCode()
        self.selectedLanguage = Bundle.SupportedLanguage.mapLanguageCode(appLangCode)
        super.init(theme: theme)
        
        NotificationCenter.default.addObserver(self, selector: #selector(localeChanged), name: Bundle.languageChangedNotificatioName, object: nil)
    }
    
    override func loadView() {
        let onboardingConsentAndPrivacyView = OnboardingConsentAndPrivacyView(theme: theme, with: self.selectedLanguage)
        onboardingConsentAndPrivacyView.delegate = self
        onboardingConsentAndPrivacyView.tableView.delegate = self
        onboardingConsentAndPrivacyView.tableView.dataSource = self
        onboardingConsentAndPrivacyView.languagePicker.pickerDelegate = self
        onboardingConsentAndPrivacyView.languagePicker.pickerDataSource = self
        onboardingConsentAndPrivacyView.languagePicker.selectRow(self.selectedLanguage.rawValue)
        
        self.internalView = onboardingConsentAndPrivacyView
    }
    
    func reloadView() {
        guard let onboardingConsentAndPrivacyView = self.view as? OnboardingConsentAndPrivacyView else {
            return
        }
        onboardingConsentAndPrivacyView.reloadView(with: self.selectedLanguage)
    }
    
    @objc func localeChanged() {
        reloadView()
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension OnboardingConsentAndPrivacyController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return OnboardingConsentAndPrivacyRow.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let row = OnboardingConsentAndPrivacyRow(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        let cell = OnboardingConsentAndPrivacyCell(theme: theme)
        
        cell.configure(with: row)
        cell.delegate = self
        self.internalView?.bringSubviewToFront(cell.consentAndPrivacyLink)
        cell.isUserInteractionEnabled = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let spacing = UIView()
        return spacing
    }
}

// MARK: - OnboardingNotificationSettingsViewDelegate
extension OnboardingConsentAndPrivacyController: OnboardingConsentAndPrivacyViewDelegate {
    func didPressLanguageChange(_ onboardingConsentAndPrivacyView: OnboardingConsentAndPrivacyView) {
        // Here we can open the settings for the language selection. But as now it is managed inside the application.
    }
    
    func didPressNotNow(_ onboardingConsentAndPrivacyView: OnboardingConsentAndPrivacyView) {
        delegate?.didFinish(self)
    }
    
    func didPressTurnOn(_ onboardingConsentAndPrivacyView: OnboardingConsentAndPrivacyView) {
        let customAlert = CustomAlertController(theme: theme, title: Localization.turn_on_exposure_notification_title, content: Localization.turn_on_exposure_notification_detail)
        
        let cancel = Button(title: Localization.btn_cancel, theme: theme)
        cancel.style = .ghost
        customAlert.addAction(button: cancel)
        
        let turnOn = Button(title: Localization.btn_consent, theme: theme)
        turnOn.action = {
            self.exposureManager.setExposureNotificationEnabled(true) { error in
                NotificationCenter.default.post(name: ExposureManager.authorizationStatusChangeNotification, object: nil)
                if let error = error {
                    switch error {
                    case .authorizationDenied:
                        fallthrough
                    case .notAuthorized:
                        let notAuthorizedAlert = CustomAlertController(theme: self.theme, title: Localization.not_autorized_error_title, content: Localization.not_autorized_error_content)
                        let cancel = Button(title: Localization.btn_done, theme: self.theme)
                        cancel.style = .ghost
                        notAuthorizedAlert.addAction(button: cancel)
                        self.present(notAuthorizedAlert, animated: true)
                        break
                    default:
                        self.showError(error)
                    }
                } else {
                    self.delegate?.didFinish(self)
                }
            }
        }
        customAlert.addAction(button: turnOn)
        
        customAlert.delegate = self
        present(customAlert, animated: true)
    }
}

extension OnboardingConsentAndPrivacyController: PickerDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return supportedLanguages[row].asText
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedLanguage = supportedLanguages[row]
        Bundle.setLanguage(lang: selectedLanguage.code)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            self.reloadView()
        }
    }
}
extension OnboardingConsentAndPrivacyController: PickerDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return supportedLanguages.count
    }
}

extension OnboardingConsentAndPrivacyController: CustomAlertControllerDelegate {
    func didDismissView(_ customAlertController: CustomAlertController) {
        customAlertController.dismiss(animated: true)
    }
}

extension OnboardingConsentAndPrivacyController: OnboardingConsentAndPrivacyCellDelegate {
    func didPressPrivacyLink(_ url: String?) {
        guard let url = url else { return }
        if let pivacyURL = URL(string: url) {
            UIApplication.shared.open(pivacyURL)
        }
    }
}
