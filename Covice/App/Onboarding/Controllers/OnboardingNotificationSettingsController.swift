//
//  OnboardingNotificationSettingsController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import ExposureNotification

protocol OnboardingNotificationSettingsControllerDelegate {
    func didFinish(_ onboardingNotificationSettingsController: OnboardingNotificationSettingsController)
}

class OnboardingNotificationSettingsController: ViewController {
    enum OnboardingNotificationSettingsRow: Int, CaseIterable {
        case turnOnEn
        case secureBluetooth
        case ministyOfHealth
        
        var icon: UIImage? {
            switch self {
            case .turnOnEn: return UIImage.exposureOnboarding
            case .secureBluetooth: return UIImage.bluetooth
            case .ministyOfHealth: return UIImage.coatOfArms
            }
        }
        
        var title: String {
            switch self {
            case .turnOnEn: return Localization.onboarding_exposure_notifications_title
            case .secureBluetooth: return Localization.onboarding_exposure_bluetooth_title
            case .ministyOfHealth: return Localization.onboarding_exposure_info_title
            }
        }
        
        var subtitle: String? {
            switch self {
            case .ministyOfHealth: return Localization.onboarding_exposure_info_sub_title
            default: return nil
            }
        }
        
        var body: String {
            switch self {
            case .turnOnEn: return Localization.onboarding_exposure_notifications_detail
            case .secureBluetooth: return Localization.onboarding_exposure_bluetooth_detail
            case .ministyOfHealth: return Localization.onboarding_exposure_info_detail
            }
        }
    }
    
    let supportedLanguages: [Bundle.SupportedLanguage] = Bundle.SupportedLanguage.allCases
    let exposureManager: ExposureManager
    let localStorageManager: LocalStorageManager
    var selectedLanguage: Bundle.SupportedLanguage
    var delegate: OnboardingNotificationSettingsControllerDelegate?
    
    init(theme: Theme, exposureManager: ExposureManager, localStorageManager: LocalStorageManager) {
        self.exposureManager = exposureManager
        self.localStorageManager = localStorageManager
        let appLangCode = Bundle.getSelectedAppLangCode()
        self.selectedLanguage = Bundle.SupportedLanguage.mapLanguageCode(appLangCode)
        super.init(theme: theme)
        
        NotificationCenter.default.addObserver(self, selector: #selector(localeChanged), name: Bundle.languageChangedNotificatioName, object: nil)
    }
    
    override func loadView() {
        let onboardingNotificationSettingsView = OnboardingNotificationSettingsView(theme: theme, with: self.selectedLanguage)
        onboardingNotificationSettingsView.delegate = self
        onboardingNotificationSettingsView.tableView.delegate = self
        onboardingNotificationSettingsView.tableView.dataSource = self
        onboardingNotificationSettingsView.languagePicker.pickerDelegate = self
        onboardingNotificationSettingsView.languagePicker.pickerDataSource = self
        onboardingNotificationSettingsView.languagePicker.selectRow(self.selectedLanguage.rawValue)
        
        self.internalView = onboardingNotificationSettingsView
    }
    
    func reloadView() {
        guard let onboardingNotificationSettingsView = self.view as? OnboardingNotificationSettingsView else {
            return
        }
        onboardingNotificationSettingsView.reloadView(with: self.selectedLanguage)
    }
    
    @objc func localeChanged() {
        reloadView()
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension OnboardingNotificationSettingsController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return OnboardingNotificationSettingsRow.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let row = OnboardingNotificationSettingsRow(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        let cell = OnboardingInformationCell(theme: theme)
        let title = row.title
        let body = row.body
        let subtitle = row.subtitle
        cell.configure(icon: row.icon, title: title, body: body, subtitle: subtitle, separetorFlag: indexPath.section != 0)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let spacing = UIView()
        return spacing
    }
}

// MARK: - OnboardingNotificationSettingsViewDelegate
extension OnboardingNotificationSettingsController: OnboardingNotificationSettingsViewDelegate {
    func didPressLanguageChange(_ onboardingNotificationSettingsView: OnboardingNotificationSettingsView) {
        // Here we can open the settings for the language selection. But as now it is managed inside the application.
    }
    
    func didPressNotNow(_ onboardingNotificationSettingsView: OnboardingNotificationSettingsView) {
        delegate?.didFinish(self)
    }
    
    func didPressTurnOn(_ onboardingNotificationSettingsView: OnboardingNotificationSettingsView) {
        let customAlert = CustomAlertController(theme: theme, title: Localization.turn_on_exposure_notification_title, content: Localization.turn_on_exposure_notification_detail)
        
        let cancel = Button(title: Localization.btn_cancel, theme: theme)
        cancel.style = .ghost
        customAlert.addAction(button: cancel)
        
        let turnOn = Button(title: Localization.btn_turn_on, theme: theme)
        turnOn.action = {
            self.exposureManager.setExposureNotificationEnabled(true) { error in
                NotificationCenter.default.post(name: ExposureManager.authorizationStatusChangeNotification, object: nil)
                if let error = error {
                    self.showError(error)
                } else {
                    self.delegate?.didFinish(self)
                }
            }
        }
        customAlert.addAction(button: turnOn)
        
        customAlert.delegate = self
        present(customAlert, animated: true)
    }
}

extension OnboardingNotificationSettingsController: PickerDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return supportedLanguages[row].asText
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedLanguage = supportedLanguages[row]
        Bundle.setLanguage(lang: selectedLanguage.code)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            self.reloadView()
        }
    }
}
extension OnboardingNotificationSettingsController: PickerDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return supportedLanguages.count
    }
}

extension OnboardingNotificationSettingsController: CustomAlertControllerDelegate {
    func didDismissView(_ customAlertController: CustomAlertController) {
        customAlertController.dismiss(animated: true)
    }
}
