//
//  OnboardingHelpInfoController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

enum OnboardingGenericInfoType  {
    case app
    case notification
    case bluetooth
    case shortExposure
    case longExposure
    
    var asOnboardingRoute: OnboardingCoordinator.OnboardingRoutes {
        switch self {
        case .app: return .explainApp
        case .bluetooth: return .explainBluetooth
        case .longExposure: return .explainLongExposure
        case .notification: return .explainNotifications
        case .shortExposure: return .explainShortExposure
        }
    }
    
    var image: UIImage? {
        switch self {
        case .app: return UIImage.helpInfo
        case .bluetooth: return UIImage.bluetoothInfo
        case .longExposure: return UIImage.longExposureInfo
        case .notification: return UIImage.notificationInfo
        case .shortExposure: return UIImage.shortExposureInfo
        }
    }
    
    var title: String {
        switch self {
        case .app: return Localization.onboarding_help_title
        case .bluetooth: return Localization.onboarding_bluetooth_title
        case .longExposure: return Localization.onboarding_long_exposure_title
        case .notification: return Localization.onboarding_notification_title
        case .shortExposure: return Localization.onboarding_short_exposure_title
        }
    }
    
    var content: String {
        switch self {
        case .app: return Localization.onboarding_help_detail
        case .bluetooth: return Localization.onboarding_bluetooth_detail
        case .longExposure: return Localization.onboarding_long_exposure_detail
        case .notification: return Localization.onboarding_notification_detail
        case .shortExposure: return Localization.onboarding_short_exposure_detail
        }
    }
    
}

protocol OnboardingGenericInfoControllerDelegate {
    func didFinish(_ onboardingGenericInfoType: OnboardingGenericInfoType )
}

class OnboardingGenericInfoController: ViewController {
    var delegate: OnboardingGenericInfoControllerDelegate?
    let onboardingGenericInfoType: OnboardingGenericInfoType
    
    init(theme: Theme, type: OnboardingGenericInfoType) {
        self.onboardingGenericInfoType = type
        super.init(theme: theme)
    }
    
    override func loadView() {
        let onboardingGenericInfoView = OnboardingGenericInfoView(theme: theme,onboardingGenericInfoType: onboardingGenericInfoType)
        
        onboardingGenericInfoView.delegate = self
        self.internalView = onboardingGenericInfoView
    }
}

// MARK: - OnboardingHelpInfoViewDelegate
extension OnboardingGenericInfoController: OnboardingGenericInfoViewDelegate {
    func didPressContinue() {
        delegate?.didFinish(onboardingGenericInfoType)
    }
}
