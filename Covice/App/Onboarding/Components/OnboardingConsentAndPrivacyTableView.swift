//
//  OnboardingConsentAndPrivacyTableView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


class OnboardingConsentAndPrivacyTableView: UITableView {
    init() {
        super.init(frame: .zero, style: .plain)
        translatesAutoresizingMaskIntoConstraints = false

        separatorStyle = .none
        backgroundColor = .clear

        isScrollEnabled = true
        isUserInteractionEnabled = true

        estimatedRowHeight = 100.scaled
        rowHeight = UITableView.automaticDimension

        allowsSelection = true
        
        tableFooterView = UIView()
        tableHeaderView = UIView()
        
        register(OnboardingConsentAndPrivacyCell.self)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol OnboardingConsentAndPrivacyCellDelegate {
    func didPressPrivacyLink(_ url: String?)
}
class OnboardingConsentAndPrivacyCell: UITableViewCell, UITextViewDelegate {
    private let theme: Theme
    
    lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 25.scaled, weight: .bold)
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var body: UITextView = {
        let view = UITextView()
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 17.scaled, weight: .regular)
        view.isScrollEnabled = false
        view.isEditable = false
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    lazy var consentAndPrivacyLink: LinkLabel = {
        let view = LinkLabel(theme: theme, underlined: true)
        view.font = .systemFont(ofSize: 14.scaled, weight: .regular)
        view.numberOfLines = 0
        view.isUserInteractionEnabled = true
        view.action = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didPressPrivacyLink(self.consentAndPrivacyLink.text)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var bodyAfterLink: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled, weight: .regular)
        view.numberOfLines = 0
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var delegate: OnboardingConsentAndPrivacyCellDelegate?
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: OnboardingInformationCell.reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        isUserInteractionEnabled = true
        contentView.isUserInteractionEnabled = true
        build()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        addSubview(icon)
        addSubview(title)
        addSubview(body)
        addSubview(bodyAfterLink)
        addSubview(consentAndPrivacyLink)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: self.topAnchor),
            icon.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            icon.heightAnchor.constraint(equalToConstant: 36.scaled),
            icon.widthAnchor.constraint(equalToConstant: 36.scaled),
        ])

        NSLayoutConstraint.activate([
            title.centerYAnchor.constraint(equalTo: icon.centerYAnchor),
            title.leadingAnchor.constraint(equalTo:  icon.trailingAnchor, constant: 24.scaled),
            title.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
        

        NSLayoutConstraint.activate([
            body.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 35.scaled),
            body.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            body.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            body.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -30.scaled)
        ])
        
        /*NSLayoutConstraint.activate([
            consentAndPrivacyLink.topAnchor.constraint(equalTo: body.bottomAnchor),
            consentAndPrivacyLink.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            consentAndPrivacyLink.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            bodyAfterLink.topAnchor.constraint(equalTo: consentAndPrivacyLink.bottomAnchor),
            bodyAfterLink.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            bodyAfterLink.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bodyAfterLink.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -30.scaled)
        ])*/
        
    }
    
    func configure(with row: OnboardingConsentAndPrivacyController.OnboardingConsentAndPrivacyRow) {
        self.icon.image = row.icon
        self.title.text = row.title
        self.setupBodyAttributedText(bold: row.bold1, subtitle1: row.subtitle1, subtitle2: row.subtitle2, subtitle3: row.subtitle3, subtitle4: row.subtitle4, subtitle5: row.subtitle5, subtitle6: row.subtitle6, subtitle7: row.subtitle7, link1: row.link1, link2: row.link2, body1: row.body1, body2: row.body2, body3: row.body3, body4: row.body4, body5: row.body5, body6: row.body6, body7: row.body7, body8: row.body8, body9: row.body9, body10: row.body10, body11: row.body11)
        //self.body.text = body
        //self.consentAndPrivacyLink.text = link
        //self.bodyAfterLink.text = bodyAfterLink
    }
    func setupBodyAttributedText(bold: String, subtitle1: String, subtitle2: String, subtitle3: String, subtitle4: String, subtitle5: String, subtitle6: String, subtitle7: String, link1: String, link2: String, body1: String, body2: String, body3: String, body4: String, body5: String, body6: String, body7: String, body8: String, body9: String, body10: String, body11: String){
        
        var finalBody = body1
        
        var startLoc = finalBody.endIndex.distance(in: finalBody)
        let boldRange: NSRange = NSRange(location: startLoc, length: bold.count)
        finalBody += bold
        finalBody += body2
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        let subtitle1Range: NSRange = NSRange(location: startLoc, length: subtitle1.count)
        finalBody += subtitle1
        finalBody += body3
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        let subtitle2Range: NSRange = NSRange(location: startLoc, length: subtitle2.count)
        finalBody += subtitle2
        finalBody += body4
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        let subtitle3Range = NSRange(location: startLoc, length: subtitle3.count)
        finalBody += subtitle3
        finalBody += body5
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        let subtitle4Range = NSRange(location: startLoc, length: subtitle4.count)
        finalBody += subtitle4
        finalBody += body6
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        let subtitle5Range = NSRange(location: startLoc, length: subtitle5.count)
        finalBody += subtitle5
        finalBody += body7
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        let subtitle6Range = NSRange(location: startLoc, length: subtitle6.count)
        finalBody += subtitle6
        finalBody += body8
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        let subtitle7Range = NSRange(location: startLoc, length: subtitle7.count)
        finalBody += subtitle7
        finalBody += body9
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        var splittedLink = link1.split(separator: "|")
        let link1Str = splittedLink[0]
        link1Action = String(splittedLink[1])
        link1Range = NSRange(location: startLoc, length: link1Str.count)
        finalBody += link1Str
        finalBody += body10
        
        startLoc = finalBody.endIndex.distance(in: finalBody)
        splittedLink = link2.split(separator: "|")
        let link2Str = splittedLink[0]
        link2Action = String(splittedLink[1])
        link2Range = NSRange(location: startLoc, length: link2Str.count)
        finalBody += link2Str
        finalBody += body11
        
        
        let attrStr = NSMutableAttributedString(string: finalBody)
        attrStr.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.scaled, weight: .bold)], range: boldRange)
        attrStr.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 21.scaled, weight: .regular)], range: subtitle1Range)
        attrStr.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 21.scaled, weight: .regular)], range: subtitle2Range)
        attrStr.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 21.scaled, weight: .regular)], range: subtitle3Range)
        attrStr.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 21.scaled, weight: .regular)], range: subtitle4Range)
        attrStr.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 21.scaled, weight: .regular)], range: subtitle5Range)
        attrStr.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 21.scaled, weight: .regular)], range: subtitle6Range)
        attrStr.addAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 21.scaled, weight: .regular)], range: subtitle7Range)
        attrStr.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemBlue], range: link1Range!)
        attrStr.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: link1Range!)
        attrStr.addAttribute(.link, value: link1Action!, range: link1Range!)
        attrStr.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemBlue], range: link2Range!)
        attrStr.addAttribute(.link, value: link2Action!, range: link2Range!)
        
        body.attributedText = attrStr
    }
    
    var link1Action: String?
    var link1Range: NSRange?
    var link2Action: String?
    var link2Range: NSRange?
    
    
    /*@objc func tapLabel(gesture: UITapGestureRecognizer) {
        var openStrURL: String?
        if let link1R = link1Range, gesture.didTapAttributedTextInLabel(label: body, targetRange: link1R) {
            openStrURL = link1Action
            print("link 1 pressed")
        } else if let link2R = link2Range, gesture.didTapAttributedTextInLabel(label: body, targetRange: link2R) {
            openStrURL = link2Action
            print("link 2 pressed")
        } else {
            return
        }
        
        guard let urlStr = openStrURL else { return }
        if let openURL = URL(string: urlStr) {
            UIApplication.shared.open(openURL)
        }
    }*/
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
            return false
        }
    
}

extension Collection {
    func distance(to index: Index) -> Int { distance(from: startIndex, to: index) }
}

extension String.Index {
    func distance<S: StringProtocol>(in string: S) -> Int { string.distance(to: self) }
}

extension UITapGestureRecognizer {
    func didTapAttributedTextInLabel(label: UILabel, targetRange: NSRange) -> Bool {
        guard let attributedString = label.attributedText else { return false }
        //guard let attributedString = label.attributedText, let lblText = label.text else { return false }
        /*let targetRange = (lblText as NSString).range(of: targetText)*/
        //IMPORTANT label correct font for NSTextStorage needed
        let mutableAttribString = NSMutableAttributedString(attributedString: attributedString)
        mutableAttribString.addAttributes(
            [NSAttributedString.Key.font: label.font ?? UIFont.smallSystemFontSize],
            range: NSRange(location: 0, length: attributedString.length)
        )
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: mutableAttribString)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y:
            locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        print("Touch")
        print(targetRange)
        print(locationOfTouchInTextContainer)
        print(indexOfCharacter)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}
