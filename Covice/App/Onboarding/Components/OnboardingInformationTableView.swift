//
//  OnboardingInformationCell.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


class OnboardingInformationTableView: UITableView {
    init() {
        super.init(frame: .zero, style: .plain)
        translatesAutoresizingMaskIntoConstraints = false

        separatorStyle = .none
        backgroundColor = .clear

        isScrollEnabled = false

        estimatedRowHeight = 100.scaled
        rowHeight = UITableView.automaticDimension

        allowsSelection = false
        
        tableFooterView = UIView()
        tableHeaderView = UIView()
        
        register(OnboardingInformationCell.self)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class OnboardingInformationCell: UITableViewCell {
    private let theme: Theme
    
    lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 16.scaled, weight: .bold)
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var subtitle: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.gray
        view.font = .systemFont(ofSize: 14.scaled, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    lazy var body: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.numberOfLines = 0
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: OnboardingInformationCell.reuseIdentifier)
        backgroundColor = .clear
        build()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        addSubview(icon)
        addSubview(title)
        addSubview(subtitle)
        addSubview(body)
        addSubview(separator)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: self.topAnchor),
            icon.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            icon.heightAnchor.constraint(equalToConstant: 36.scaled),
            icon.widthAnchor.constraint(equalToConstant: 36.scaled),
        ])

        NSLayoutConstraint.activate([
            title.centerYAnchor.constraint(equalTo: icon.centerYAnchor),
            title.leadingAnchor.constraint(equalTo:  icon.trailingAnchor, constant: 24.scaled),
            title.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            subtitle.leadingAnchor.constraint(equalTo: title.leadingAnchor),
            subtitle.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            subtitle.topAnchor.constraint(greaterThanOrEqualTo: title.bottomAnchor, constant: 1.scaled),
            subtitle.bottomAnchor.constraint(lessThanOrEqualTo: body.topAnchor, constant: -5.scaled)
        ])

        NSLayoutConstraint.activate([
            body.topAnchor.constraint(greaterThanOrEqualTo: title.bottomAnchor, constant: 15.scaled),
            body.topAnchor.constraint(lessThanOrEqualTo: title.bottomAnchor, constant: 24.scaled),
            body.leadingAnchor.constraint(equalTo: title.leadingAnchor),
            body.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            body.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            separator.leadingAnchor.constraint(equalTo: body.leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            separator.topAnchor.constraint(equalTo: self.topAnchor, constant: -11.scaled),
            separator.heightAnchor.constraint(equalToConstant: 1.scaled)
        ])
    }
    
    func configure(icon: UIImage?, title: String?, body: String?, subtitle: String?, separetorFlag: Bool) {
        self.icon.image = icon
        self.title.text = title
        self.body.text = body
        self.subtitle.text = subtitle
        if subtitle != nil {
            self.subtitle.isHidden = false
        }
        if !separetorFlag {
            separator.isHidden = true
        }
    }
}
