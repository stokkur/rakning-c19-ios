//
//  OnboardingCoordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol OnboardingCoordinatorDelegate {
    func didFinish(_ onboardingCoordinator: OnboardingCoordinator)
}

class OnboardingCoordinator: Coordinator {
    enum OnboardingRoutes: Int, CaseIterable {
        case exposureNotificationsSettings = 0
        case explainApp
        case explainNotifications
        case explainBluetooth
        case explainShortExposure
        case explainLongExposure
    }
    
    required init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.navigationController = navigationController
        self.theme = theme
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.storageManager = storageManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
    }
    
    var coreDataManager: CoreDataManager
    
    var networkManager: NetworkManager
    
    var exposureManager: ExposureManager
    
    var storageManager: StorageManager
    
    var localStorageManager: LocalStorageManager
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    var theme: Theme
    
    var delegate: OnboardingCoordinatorDelegate?
    
    @discardableResult func start() -> UINavigationController {
        var controller: ViewController
        switch OnboardingRoutes(rawValue: localStorageManager.onboardingCurrentScreen.wrappedValue) {
        case .exposureNotificationsSettings:
            let auxController = OnboardingConsentAndPrivacyController(theme: theme, exposureManager: exposureManager, localStorageManager: localStorageManager)
            auxController.delegate = self
            controller = auxController
            break
        case .explainApp:
            let auxController = OnboardingGenericInfoController(theme: theme, type: .app)
            auxController.delegate = self
            controller = auxController
            break
        case .explainNotifications:
            let auxController = OnboardingGenericInfoController(theme: theme, type: .notification)
            auxController.delegate = self
            controller = auxController
            break
        case .explainBluetooth:
            let auxController = OnboardingGenericInfoController(theme: theme, type: .bluetooth)
            auxController.delegate = self
            controller = auxController
            break
        case .explainShortExposure:
            let auxController = OnboardingGenericInfoController(theme: theme, type: .shortExposure)
            auxController.delegate = self
            controller = auxController
            break
        case .explainLongExposure:
            let auxController = OnboardingGenericInfoController(theme: theme, type: .longExposure)
            auxController.delegate = self
            controller = auxController
            break
        case .none:
            let auxController = OnboardingConsentAndPrivacyController(theme: theme, exposureManager: exposureManager, localStorageManager: localStorageManager)
            auxController.delegate = self
            controller = auxController
            break
        }
        navigationController.pushViewController(controller, animated: false)
        
        return self.navigationController
    }
    
    private func didFinishOnboarding(){
        self.delegate?.didFinish(self)
    }
}

// MARK: - OnboardingNotificationSettingsControllerDelegate
extension OnboardingCoordinator: OnboardingConsentAndPrivacyControllerDelegate {
    func didFinish(_ OnboardingConsentAndPrivacyController: OnboardingConsentAndPrivacyController) {
        localStorageManager.onboardingCurrentScreen.wrappedValue = OnboardingRoutes.explainApp.rawValue
        let controller = OnboardingGenericInfoController(theme: theme, type: .app)
        controller.delegate = self
        navigationController.pushViewController(controller, animated: true)
    }
}

// MARK: - OnboardingHelpInfoControllerDelegate
extension OnboardingCoordinator: OnboardingGenericInfoControllerDelegate {
    func didFinish(_ type: OnboardingGenericInfoType) {
        localStorageManager.onboardingCurrentScreen.wrappedValue = type.asOnboardingRoute.rawValue + 1
        
        switch type {
        case .app:
            let notification = OnboardingGenericInfoController(theme: theme, type: .notification)
            notification.delegate = self
            navigationController.pushViewController(notification, animated: true)
            
        case .notification:
            let bluetooth = OnboardingGenericInfoController(theme: theme, type: .bluetooth)
            bluetooth.delegate = self
            navigationController.pushViewController(bluetooth, animated: true)
            
        case .bluetooth:
            let shortExposure = OnboardingGenericInfoController(theme: theme, type: .shortExposure)
            shortExposure.delegate = self
            navigationController.pushViewController(shortExposure, animated: true)
            
        case .shortExposure:
            let longExposure =  OnboardingGenericInfoController(theme: theme, type: .longExposure)
            longExposure.delegate = self
            navigationController.pushViewController(longExposure, animated: true)
            
        case .longExposure:
            self.phoneRegistrationFlow()
        }
    }
}

extension OnboardingCoordinator: PhoneRegistrationCoordinatorDelegate {
    func phoneRegistrationFlow() {
        let registrationCoordinator = PhoneRegistrationCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        registrationCoordinator.delegate = self
        
        registrationCoordinator.start()
        addChildCoordinator(registrationCoordinator)
    }
    
    func didFinish(_ onboardingPhoneRegistrationCoordinator: PhoneRegistrationCoordinator) {
        removeChildCoordinator(onboardingPhoneRegistrationCoordinator)
        didFinishOnboarding()
    }
    
    func didClose(_ onboardingPhoneRegistrationCoordinator: PhoneRegistrationCoordinator) {
        removeChildCoordinator(onboardingPhoneRegistrationCoordinator)
        didFinishOnboarding()
    }
}

