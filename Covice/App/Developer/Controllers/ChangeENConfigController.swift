//
//  ChangeENConfigController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import ExposureNotification

protocol ChangeENConfigControllerDelegate {
    func didFinish(_ controller: ChangeENConfigController)
}

class ChangeENConfigController: ViewController {
    
    
    private let cdManager: CoreDataManager
    private let notificationCenter = NotificationCenter.default
    
    var delegate: ChangeENConfigControllerDelegate?
    
    init(theme: Theme, coreDataManager: CoreDataManager) {
        self.cdManager = coreDataManager
        
        
        super.init(theme: theme)
        
        
    }
    
    override func loadView() {
        self.title = "Change EN Config"
        let view = ChangeENConfigView(theme: theme)
        view.delegate = self
        view.isUserInteractionEnabled = true
        setupConfigValues(view: view)
        self.internalView = view
        
    }
    
    func setupConfigValues(view: ChangeENConfigView){
        ExposureConfiguration().getPersistedExposureConfig(coreDataManager: self.cdManager) { expConfig in
            guard let expConfig = expConfig else {
                self.delegate?.didFinish(self)
                return
            }
            view.setupActualValues(enConfig: expConfig)
        }
    }
    
}

extension ChangeENConfigController: ChangeENConfigViewDelegate {
    func saveNewConfig(_ view: ChangeENConfigView, minimumRiskScore: String, immeadiateWeight: String, immeadiateThreashold: String, nearWeight: String, nearThreashold: String, mediumWeight: String, mediumThreashold: String, otherWeight: String) {
        self.cdManager.fetch(with: NSPredicate(format: "configId == %@", ExposureConfiguration.EXPOSURE_CONFIG_ID)) { (result: Result<[ExposureConfig], CoreDataManagerError>) in
            switch result {
            case .failure(let error):
                self.showError(error)
                break
            case .success(let exposureConfigs):
                guard var expConfig = exposureConfigs.first else {
                    print("Couldn't get the exposure config")
                    return
                }
                if let newVal = Double(immeadiateWeight) {
                    expConfig.immediateDurationWeight = newVal
                }
                if let newVal = Int16(immeadiateThreashold) {
                    expConfig.immediateThreshold = newVal
                }
                
                if let newVal = Double(nearWeight) {
                    expConfig.nearDurationWeight = newVal
                }
                if let newVal = Int16(nearThreashold) {
                    expConfig.nearThreshold = newVal
                }
                
                if let newVal = Double(mediumWeight) {
                    expConfig.mediumDurationWeight = newVal
                }
                if let newVal = Int16(mediumThreashold) {
                    expConfig.mediumThreshold = newVal
                }
                
                if let newVal = Double(otherWeight) {
                    expConfig.otherDurationWeight = newVal
                }
                
                if let newVal = Int16(minimumRiskScore) {
                    expConfig.minimumRiskScore = newVal
                }
                
                self.cdManager.update(entities: [expConfig]) { error in
                    DispatchQueue.main.async {
                        if let error = error {
                            self.showError(error)
                            return
                        }
                        self.delegate?.didFinish(self)
                    }
                }
                break
            }
        }
    }
}
