//
//  ProtoTypeController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import ExposureNotification

protocol DeveloperControllerDelegate {
    func didFinish(_ developerController: DeveloperController)
    func showDiagnosisKeys(_ developerController: DeveloperController)
    func showExposures(_ developerController: DeveloperController)
    func showEndpointConfig(_ developerController: DeveloperController)
    func showChangeENConfig(_ developerController: DeveloperController)
}

class DeveloperController: ViewController {
    let networkManager: NetworkManager
    let exposureManager: ExposureManager
    let localStorageManager: LocalStorageManager
    let coreDataManager: CoreDataManager
    
    init(theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
        super.init(theme: theme)
    }
    
    var delegate: DeveloperControllerDelegate?
    
    override func loadView() {
        let developerView = DeveloperView(theme: theme)
        developerView.delegate = self
        self.internalView = developerView
        
        self.title = "Developer Menu"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


// MARK: - DeveloperViewDelegate
extension DeveloperController: DeveloperViewDelegate {
    func sendLocalNotification(_ developerView: DeveloperView) {
        LocalNotification.sendLocalNotification(title: "Possible exposure detected!", body: "A possible exposure to Covid-19 has been detected for more information please open the Ranking C-19 app.")
    }
    
    func addMockExposure(_ developerView: DeveloperView) {
        let exposure = Exposure(date: Date(), infectiousness: .high)
        
        coreDataManager.update(entities: [exposure]) { error in
            if let error = error {
                DispatchQueue.main.async { [weak self] in
                    self?.showError(error)
                }
            }
        }
    }
    
    func detectExposures(_ developerView: DeveloperView) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let _ = appDelegate.handleExposureKeySets() { (result) in
            switch result {
            case .success(let exposure):
                print("Successfully detected exposures")
                if (exposure == nil) {
                    print("No new exposures found")
                } else {
                    print("New exposure found!")
                }
            case .failure(let error):
                print("Unable to detect exposures with Error: \(error.localizedDescription)")
            }
        }
    }
    
    func sendExposureKeySets(_ developerView: DeveloperView) {
        let alert = UIAlertController(title:"Verification Token\n",
                                      message:
                                        """
                                        Input the Code from Embætti Landlæknis to recieve a verification token.
                                        """,
                                      preferredStyle: .alert)
        alert.addTextField() { (textfield) in
            textfield.placeholder = "0000"
            textfield.textAlignment = .center
            textfield.keyboardType = .numberPad
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "Send", style: .default) { (_) in
            guard let textfield = alert.textFields?.first,
                  let code = textfield.text
            else { return }
            
            self.networkManager.getVerificationToken(request: .init(code: code))
            { result in
                switch result {
                case .success(let verification):
                    self.exposureManager.getDiagnosisKeys() { result in
                        
                        switch result {
                        case .success(let keys):
                            let token = verification.token
                            let signature = keys.asHMACsignature

                            self.networkManager.getCertificate(request: .init(token:
                                                                                token,
                                                                              ekeyhmac: signature.ekeyhmac))
                            { result in
                                switch result {
                                case .success(let certificate):
                                    let revisionToken = self.localStorageManager.revisionToken
                                    self.networkManager.publishKeys(request: .init(temporaryExposureKeys: keys,
                                                                                   hmacKey: signature.hmackey,
                                                                                   verificationPayload: certificate.certificate,
                                                                                   traveler: false,
                                                                                   symptomOnsetInterval: nil,
                                                                                   revisionToken: revisionToken.wrappedValue))
                                    { [weak self] result in
                                        
                                        guard let self = self else { return }
                                        
                                        switch result {
                                        case .success(let publish):
                                            self.localStorageManager.revisionToken.wrappedValue = publish.revisionToken
                                            let alert = UIAlertController(title: "Success",
                                                                          message: "Publish successful",
                                                                          preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: .default))
                                            
                                            DispatchQueue.main.async {
                                                self.present(alert, animated: true)
                                            }
                                            
                                        case .failure(let error):
                                            DispatchQueue.main.async {
                                                self.showError(error)
                                            }
                                        }
                                    }
                                case .failure(let error):
                                    DispatchQueue.main.async {
                                        self.showError(error)
                                    }
                                }
                            }
                        case .failure(let error):
                            DispatchQueue.main.async {
                                self.showError(error)
                            }
                        }
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self.showError(error)
                    }
                }
            }
        })
    
        present(alert, animated: true)
    }
    
    func showOnboarding(_ developerView: DeveloperView) {
        localStorageManager.onboardingCurrentScreen.wrappedValue = OnboardingCoordinator.OnboardingRoutes.exposureNotificationsSettings.rawValue
        delegate?.didFinish(self)
    }
    
    func resetOnboarded(_ developerView: DeveloperView) {
        localStorageManager.onboardingCurrentScreen.wrappedValue = OnboardingCoordinator.OnboardingRoutes.exposureNotificationsSettings.rawValue
        localStorageManager.isOnboarded.wrappedValue = false
    }
    
    func showExposureScreen(_ developerView: DeveloperView) {
        delegate?.showExposures(self)
    }
    
    func changeENConfig(_ developerView: DeveloperView){
        delegate?.showChangeENConfig(self)
    }
    
    func showEndpointConfig(_ developerView: DeveloperView) {
        delegate?.showEndpointConfig(self)
    }
    
}
