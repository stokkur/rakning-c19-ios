//
//  DeveloperEndpointConfig.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
class DeveloperEndpointController: ViewController {
    
    init(theme: Theme) {
        super.init(theme: theme)
    }
    
    var delegate: DeveloperControllerDelegate?
    
    override func loadView() {
        let developerView = DeveloperEndpointsView(theme: theme)
        
        let netConf = NetworkConfiguration.networkConfiguration
        developerView.configure(
            registrationEp:netConf.requestPinUrl!.absoluteString,
            verificationEp: NetworkConfiguration.networkConfiguration.verificationUrl!.absoluteString,
            exposureEp: NetworkConfiguration.networkConfiguration.publishKeysUrl!.absoluteString,
            keySetsEp: NetworkConfiguration.networkConfiguration.exposureKeySetUrl!.absoluteString
        )
        self.internalView = developerView
        
        self.title = "Endpoints Config"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
