//
//  DeveloperCoordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import ExposureNotification

protocol DeveloperCoordinatorDelegate {
    func didFinish(_ developerCoordinator: DeveloperCoordinator)
    // func didSelectSettingsView(_ developerCoordinator: DeveloperCoordinator)
}

class DeveloperCoordinator: Coordinator {
    required init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.navigationController = NavigationController(theme: theme)
        self.theme = theme
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.storageManager = storageManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
    }
    
    var coreDataManager: CoreDataManager
    
    var networkManager: NetworkManager
    
    var exposureManager: ExposureManager
    
    var storageManager: StorageManager
    
    var localStorageManager: LocalStorageManager
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    var theme: Theme
    
    var delegate: DeveloperCoordinatorDelegate?
    
    @discardableResult func start() -> UINavigationController {
        let developer = DeveloperController(theme: theme, networkManager: networkManager, exposureManager: exposureManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        developer.delegate = self
        
        navigationController.pushViewController(developer, animated: false)
        
        return self.navigationController
    }
}

// MARK: - DeveloperControllerDelegate
extension DeveloperCoordinator: DeveloperControllerDelegate {
    func showSettings(_ developerController: DeveloperController) {
        //let controller = SettingsCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager)
        //navigationController.pushViewController(controller, animated: true)
        //delegate?.didSelectSettingsView(self)
    }
    
    func showExposures(_ developerController: DeveloperController) {
        let controller = ExposureController(theme: theme, exposureManager: exposureManager, coreDataManager: coreDataManager)
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showDiagnosisKeys(_ developerController: DeveloperController) {
        let controller = DiagnosisKeysController(theme: AppTheme())
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showChangeENConfig(_ developerController: DeveloperController) {
        let controller = ChangeENConfigController(theme: theme, coreDataManager: coreDataManager)
        controller.delegate = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showEndpointConfig(_ developerController: DeveloperController) {
        let controller = DeveloperEndpointController(theme: AppTheme())
        navigationController.pushViewController(controller, animated: true)
    }
    
    func didFinish(_ developerController: DeveloperController) {
        navigationController.popViewController(animated: false)
        delegate?.didFinish(self)
    }
    
}

extension DeveloperCoordinator: ChangeENConfigControllerDelegate{
    func didFinish(_ controller: ChangeENConfigController) {
        navigationController.popViewController(animated: true)
    }
}
