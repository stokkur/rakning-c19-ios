//
//  DeveloperEndpointsView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


class DeveloperEndpointsView: View {
    
    lazy var registrationLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.font = .boldSystemFont(ofSize: 12.scaled)
        view.text = "Registration:"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var registrationEndpoint: UILabel = {
        let view = UILabel(frame: .zero)
        view.font = .systemFont(ofSize: 10.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines=0
        return view
    }()
    
    lazy var verificationLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.font = .boldSystemFont(ofSize: 12.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Verification:"
        return view
    }()
    lazy var verificationEndpoint: UILabel = {
        let view = UILabel(frame: .zero)
        view.font = .systemFont(ofSize: 10.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines=0
        return view
    }()
    
    lazy var exposureLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.font = .boldSystemFont(ofSize: 12.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Exposure:"
        return view
    }()
    lazy var exposureEndpoint: UILabel = {
        let view = UILabel(frame: .zero)
        view.font = .systemFont(ofSize: 10.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines=0
        return view
    }()
    
    lazy var keySetsLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.font = .boldSystemFont(ofSize: 12.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Key Sets:"
        return view
    }()
    lazy var keySetsEndpoint: UILabel = {
        let view = UILabel(frame: .zero)
        view.font = .systemFont(ofSize: 10.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines=0
        return view
    }()
    
    override func build() {
        super.build()
        
        addSubview(registrationLabel)
        addSubview(registrationEndpoint)
        addSubview(verificationLabel)
        addSubview(verificationEndpoint)
        addSubview(exposureLabel)
        addSubview(exposureEndpoint)
        addSubview(keySetsLabel)
        addSubview(keySetsEndpoint)
    }
    
    func configure(registrationEp: String, verificationEp: String, exposureEp: String, keySetsEp: String){
        registrationEndpoint.text = registrationEp
        verificationEndpoint.text = verificationEp
        exposureEndpoint.text = exposureEp
        keySetsEndpoint.text = keySetsEp
    }

    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            registrationLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 15.scaled),
            registrationLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 15.scaled),
            registrationEndpoint.leftAnchor.constraint(equalTo: registrationLabel.rightAnchor, constant: 10.scaled),
            registrationEndpoint.firstBaselineAnchor.constraint(equalTo: registrationLabel.firstBaselineAnchor),
            registrationEndpoint.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: 15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            verificationLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 15.scaled),
            verificationLabel.topAnchor.constraint(equalTo: registrationEndpoint.bottomAnchor, constant: 35.scaled),
            verificationEndpoint.leftAnchor.constraint(equalTo: verificationLabel.rightAnchor, constant: 10.scaled),
            verificationEndpoint.firstBaselineAnchor.constraint(equalTo: verificationLabel.firstBaselineAnchor),
            verificationEndpoint.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: 15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            exposureLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 15.scaled),
            exposureLabel.topAnchor.constraint(equalTo: verificationEndpoint.bottomAnchor, constant: 35.scaled),
            exposureEndpoint.leftAnchor.constraint(equalTo: exposureLabel.rightAnchor, constant: 10.scaled),
            exposureEndpoint.firstBaselineAnchor.constraint(equalTo: exposureLabel.firstBaselineAnchor),
            exposureEndpoint.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: 15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            keySetsLabel.leftAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor, constant: 15.scaled),
            keySetsLabel.topAnchor.constraint(equalTo: exposureEndpoint.bottomAnchor, constant: 35.scaled),
            keySetsEndpoint.leftAnchor.constraint(equalTo: keySetsLabel.rightAnchor, constant: 10.scaled),
            keySetsEndpoint.firstBaselineAnchor.constraint(equalTo: keySetsLabel.firstBaselineAnchor),
            keySetsEndpoint.rightAnchor.constraint(equalTo: self.safeAreaLayoutGuide.rightAnchor, constant: 15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            registrationLabel.widthAnchor.constraint(equalToConstant: 80.scaled),
            verificationLabel.widthAnchor.constraint(equalTo: registrationLabel.widthAnchor),
            exposureLabel.widthAnchor.constraint(equalTo: registrationLabel.widthAnchor),
            keySetsLabel.widthAnchor.constraint(equalTo: registrationLabel.widthAnchor),
        ])
        
    }
}
