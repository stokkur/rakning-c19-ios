//
//  ChangeENConfigView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


protocol ChangeENConfigViewDelegate {
    func saveNewConfig(_ view:ChangeENConfigView,
                       minimumRiskScore: String,
                       immeadiateWeight: String, immeadiateThreashold: String,
                       nearWeight: String, nearThreashold: String,
                       mediumWeight: String, mediumThreashold: String,
                       otherWeight: String)
}

class ChangeENConfigView: View {
    
    var delegate: ChangeENConfigViewDelegate?
    
    lazy var minimumRiskScoreLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Minimum Risk Score:"
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var immediateWeightLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Immediate Weight:"
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var immediateThresholdLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Immediate Threshold:"
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var nearWeightLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Near Weight:"
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var nearThresholdLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Near Threshold:"
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var mediumWeightLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Medium Weight:"
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var mediumThresholdLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Medium Threshold:"
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var otherWeightLabel: UILabel = {
        let view = UILabel(frame: .zero)
        view.text = "Other Weight:"
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 14.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // Text Fields
    lazy var minimumRiskScoreText: UITextField = {
        let view = UITextField(frame: .zero)
        view.borderStyle = .line
        view.keyboardType = .numberPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var immediateWeightText: UITextField = {
        let view = UITextField(frame: .zero)
        view.borderStyle = .line
        view.keyboardType = .numberPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var immediateThresholdText: UITextField = {
        let view = UITextField(frame: .zero)
        view.borderStyle = .line
        view.keyboardType = .numberPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var nearWeightText: UITextField = {
        let view = UITextField(frame: .zero)
        view.borderStyle = .line
        view.keyboardType = .numberPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var nearThresholdText: UITextField = {
        let view = UITextField(frame: .zero)
        view.borderStyle = .line
        view.keyboardType = .numberPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var mediumWeightText: UITextField = {
        let view = UITextField(frame: .zero)
        view.borderStyle = .line
        view.keyboardType = .numberPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var mediumThresholdText: UITextField = {
        let view = UITextField(frame: .zero)
        view.borderStyle = .line
        view.keyboardType = .numberPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var otherWeightText: UITextField = {
        let view = UITextField(frame: .zero)
        view.borderStyle = .line
        view.keyboardType = .numberPad
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var saveBtn: Button = {
        let view = Button(title: "Save Config", theme: theme)
        view.style = .primary
        view.action = {
            self.delegate?.saveNewConfig(self,
                                         minimumRiskScore: self.minimumRiskScoreText.text ?? "1",
                                         immeadiateWeight: self.immediateWeightText.text ?? "100",
                                         immeadiateThreashold: self.immediateThresholdText.text ?? "55",
                                         nearWeight: self.nearWeightText.text ?? "55",
                                         nearThreashold: self.nearThresholdText.text ?? "63",
                                         mediumWeight: self.mediumWeightText.text ?? "0",
                                         mediumThreashold: self.mediumThresholdText.text ?? "70",
                                         otherWeight: self.otherWeightText.text ?? "0"
            )
        }
        view.titleLabel?.font = .systemFont(ofSize: 16.scaled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var scroll: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    var saveBtnConstriaint: NSLayoutConstraint?
    
    init(theme: Theme) {
        super.init(theme: theme)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func build() {
        super.build()
        
        scroll.addSubview(minimumRiskScoreLabel)
        scroll.addSubview(immediateWeightLabel)
        scroll.addSubview(immediateThresholdLabel)
        scroll.addSubview(nearWeightLabel)
        scroll.addSubview(nearThresholdLabel)
        scroll.addSubview(mediumWeightLabel)
        scroll.addSubview(mediumThresholdLabel)
        scroll.addSubview(otherWeightLabel)
        
        scroll.addSubview(minimumRiskScoreText)
        scroll.addSubview(immediateWeightText)
        scroll.addSubview(immediateThresholdText)
        scroll.addSubview(nearWeightText)
        scroll.addSubview(nearThresholdText)
        scroll.addSubview(mediumWeightText)
        scroll.addSubview(mediumThresholdText)
        scroll.addSubview(otherWeightText)
        
        addSubview(scroll)
        addSubview(saveBtn)
        
    }

    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            scroll.topAnchor.constraint(equalTo: self.topAnchor),
            scroll.leftAnchor.constraint(equalTo: self.leftAnchor),
            scroll.rightAnchor.constraint(equalTo: self.rightAnchor),
            scroll.bottomAnchor.constraint(equalTo: saveBtn.topAnchor,constant: -30.scaled)
        ])
        
        NSLayoutConstraint.activate([
            saveBtn.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15.scaled),
            saveBtn.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            minimumRiskScoreLabel.topAnchor.constraint(equalTo: scroll.topAnchor, constant: 25.scaled),
            minimumRiskScoreLabel.leftAnchor.constraint(equalTo: scroll.leftAnchor, constant: 15.scaled),
            minimumRiskScoreText.lastBaselineAnchor.constraint(equalTo: minimumRiskScoreLabel.lastBaselineAnchor),
            minimumRiskScoreText.leftAnchor.constraint(equalTo: minimumRiskScoreLabel.rightAnchor, constant: 30),
            minimumRiskScoreText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            immediateWeightLabel.topAnchor.constraint(equalTo: minimumRiskScoreLabel.bottomAnchor, constant: 25.scaled),
            immediateWeightLabel.leftAnchor.constraint(equalTo: scroll.leftAnchor, constant: 15.scaled),
            immediateWeightText.lastBaselineAnchor.constraint(equalTo: immediateWeightLabel.lastBaselineAnchor),
            immediateWeightText.leftAnchor.constraint(equalTo: immediateWeightLabel.rightAnchor, constant: 30),
            immediateWeightText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            nearWeightLabel.topAnchor.constraint(equalTo: immediateWeightLabel.bottomAnchor, constant: 25.scaled),
            nearWeightLabel.leftAnchor.constraint(equalTo: immediateWeightLabel.leftAnchor),
            nearWeightText.lastBaselineAnchor.constraint(equalTo: nearWeightLabel.lastBaselineAnchor),
            nearWeightText.leftAnchor.constraint(equalTo: nearWeightLabel.rightAnchor, constant: 30),
            nearWeightText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            mediumWeightLabel.topAnchor.constraint(equalTo: nearWeightLabel.bottomAnchor, constant: 25.scaled),
            mediumWeightLabel.leftAnchor.constraint(equalTo: nearWeightLabel.leftAnchor),
            mediumWeightText.lastBaselineAnchor.constraint(equalTo: mediumWeightLabel.lastBaselineAnchor),
            mediumWeightText.leftAnchor.constraint(equalTo: mediumWeightLabel.rightAnchor, constant: 30),
            mediumWeightText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            otherWeightLabel.topAnchor.constraint(equalTo: mediumWeightLabel.bottomAnchor, constant: 25.scaled),
            otherWeightLabel.leftAnchor.constraint(equalTo: mediumWeightLabel.leftAnchor),
            otherWeightText.lastBaselineAnchor.constraint(equalTo: otherWeightLabel.lastBaselineAnchor),
            otherWeightText.leftAnchor.constraint(equalTo: otherWeightLabel.rightAnchor, constant: 30),
            otherWeightText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            immediateThresholdLabel.topAnchor.constraint(equalTo: otherWeightLabel.bottomAnchor, constant: 25.scaled),
            immediateThresholdLabel.leftAnchor.constraint(equalTo: otherWeightLabel.leftAnchor),
            immediateThresholdText.lastBaselineAnchor.constraint(equalTo: immediateThresholdLabel.lastBaselineAnchor),
            immediateThresholdText.leftAnchor.constraint(equalTo: immediateThresholdLabel.rightAnchor, constant: 30),
            immediateThresholdText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            nearThresholdLabel.topAnchor.constraint(equalTo: immediateThresholdLabel.bottomAnchor, constant: 25.scaled),
            nearThresholdLabel.leftAnchor.constraint(equalTo: immediateThresholdLabel.leftAnchor),
            nearThresholdText.lastBaselineAnchor.constraint(equalTo: nearThresholdLabel.lastBaselineAnchor),
            nearThresholdText.leftAnchor.constraint(equalTo: nearThresholdLabel.rightAnchor, constant: 30),
            nearThresholdText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            mediumThresholdLabel.topAnchor.constraint(equalTo: nearThresholdLabel.bottomAnchor, constant: 25.scaled),
            mediumThresholdLabel.leftAnchor.constraint(equalTo: nearThresholdLabel.leftAnchor),
            mediumThresholdText.lastBaselineAnchor.constraint(equalTo: mediumThresholdLabel.lastBaselineAnchor),
            mediumThresholdText.leftAnchor.constraint(equalTo: mediumThresholdLabel.rightAnchor, constant: 30),
            mediumThresholdText.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15.scaled)
            
        ])
        
        
        NSLayoutConstraint.activate([
            minimumRiskScoreLabel.widthAnchor.constraint(equalToConstant: 140.scaled),
            immediateWeightLabel.widthAnchor.constraint(equalToConstant: 140.scaled),
            immediateThresholdLabel.widthAnchor.constraint(equalToConstant: 140.scaled),
            nearWeightLabel.widthAnchor.constraint(equalToConstant: 140.scaled),
            nearThresholdLabel.widthAnchor.constraint(equalToConstant: 140.scaled),
            mediumWeightLabel.widthAnchor.constraint(equalToConstant: 140.scaled),
            mediumThresholdLabel.widthAnchor.constraint(equalToConstant: 140.scaled),
            otherWeightLabel.widthAnchor.constraint(equalToConstant: 140.scaled)
        ])
        
        saveBtnConstriaint = saveBtn.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled)
        saveBtnConstriaint?.isActive = true
        
    }
    
    func setupActualValues(enConfig expConfig: ExposureConfig){
        self.minimumRiskScoreText.text = String(expConfig.minimumRiskScore)
        
        self.immediateWeightText.text = String(expConfig.immediateDurationWeight)
        self.immediateThresholdText.text = String(expConfig.immediateThreshold)
        
        self.nearWeightText.text = String(expConfig.nearDurationWeight)
        self.nearThresholdText.text = String(expConfig.nearThreshold)
        
        self.mediumWeightText.text = String(expConfig.mediumDurationWeight)
        self.mediumThresholdText.text = String(expConfig.mediumThreshold)
        
        self.otherWeightText.text = String(expConfig.otherDurationWeight)
        
        /*minimumRiskScoreText.text = String(localStorageManager.minimumRiskScore.wrappedValue)
        
        immediateWeightText.text = String(localStorageManager.immediateConfigWeight.wrappedValue)
        immediateThresholdText.text = String(localStorageManager.immediateConfigThreshold.wrappedValue)
        
        nearWeightText.text = String(localStorageManager.nearConfigWeight.wrappedValue)
        nearThresholdText.text = String(localStorageManager.nearConfigThreshold.wrappedValue)
        
        mediumWeightText.text = String(localStorageManager.mediumConfigWeight.wrappedValue)
        mediumThresholdText.text = String(localStorageManager.mediumConfigThreshold.wrappedValue)
        
        otherWeightText.text = String(localStorageManager.otherConfigWeight.wrappedValue)*/
//        coreDataManager.fetch(with: NSPredicate(format: "id == %@", ExposureConfiguration.EXPOSURE_CONFIG_ID)) { (result: Result<[ExposureConfig],CoreDataManagerError>) in
//            switch result {
//            case .failure(let error):
//
//                break
//            case .success(let expConfigs):
//                guard let expConfig = expConfigs.first else {
//                    return
//                }
//                self.minimumRiskScoreText.text = String(expConfig.minimumRiskScore)
//
//                self.immediateWeightText.text = String(expConfig.immediateDurationWeight)
//                self.immediateThresholdText.text = String(expConfig.immediateThreshold)
//
//                self.nearWeightText.text = String(expConfig.nearDurationWeight)
//                self.nearThresholdText.text = String(expConfig.nearThreshold)
//
//                self.mediumWeightText.text = String(expConfig.mediumDurationWeight)
//                self.mediumThresholdText.text = String(expConfig.mediumThreshold)
//
//                self.otherWeightText.text = String(expConfig.otherDurationWeight)
//
//                break
//            }
//
//        }
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        saveBtnConstriaint?.constant = -13.scaled - keyboardHeight
        

        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        saveBtnConstriaint?.constant = -13.scaled
        

        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
        }
    }
}

