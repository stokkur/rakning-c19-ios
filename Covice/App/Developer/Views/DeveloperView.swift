//
//  PrototypeView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


protocol DeveloperViewDelegate {
    func showOnboarding(_ developerView: DeveloperView)
    func resetOnboarded(_ developerView: DeveloperView)
    func showExposureScreen(_ developerView: DeveloperView)
    func showEndpointConfig(_ developerView: DeveloperView)
    func sendExposureKeySets(_ developerView: DeveloperView)
    func detectExposures(_ developerView: DeveloperView)
    func addMockExposure(_ developerView: DeveloperView)
    func sendLocalNotification(_ developerView: DeveloperView)
    func changeENConfig(_ developerView: DeveloperView)
}

class DeveloperView: View {
    enum Section: Int {
        case ui
        case exposureKeys
        
        var title: String {
            switch self {
            case .ui: return "UI"
            case .exposureKeys: return "Exposure Keys"
            }
        }
    }
    
    enum GeneralRow: Int, CaseIterable {
        case showOnboarding
        case resetOnboarded
        case showExposureScreen
        case showEndpointConfiguration
        

        var title: String {
            switch self {
            case .showOnboarding: return "Show Onboarding"
            case .resetOnboarded: return "Reset Onboarded"
            case .showExposureScreen: return "Show Exposure Screen"
            case .showEndpointConfiguration: return "Show Endpoint Configuration"
            }
        }
    }
    
    enum ExposureKeysRow: Int, CaseIterable {
        case sendExposureKeys
        case testExposureDetection
        case addMockExposure
        case sendLocalNotfication
        case changeENConfig
        
        var title: String {
            switch self {
            case .sendExposureKeys: return "Submit Temporary Exposure Keys"
            case .testExposureDetection: return "Test Exposure Detection"
            case .addMockExposure: return "Add Mock Exposure"
            case .sendLocalNotfication: return "Send Local Notification"
            case .changeENConfig: return "Change EN Config"
            }
        }
    }
    
    var delegate: DeveloperViewDelegate?
    
    lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero)
        view.backgroundColor = UIColor.clear
        view.delegate = self
        view.dataSource = self
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    
    override func build() {
        super.build()
        
        addSubview(tableView)
    }

    override func setupConstraints() {
        super.setupConstraints()
        
        tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    }
}


// MARK: - UITableViewDelegate & UITableViewDataSource
extension DeveloperView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let developmentSection = Section(rawValue: section)!
        return developmentSection.title
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let developmentSection = Section(rawValue: section)!
        
        switch developmentSection {
        case .ui: return GeneralRow.allCases.count
        case .exposureKeys: return ExposureKeysRow.allCases.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Section(rawValue: indexPath.section)!
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell") ?? UITableViewCell(style: .default, reuseIdentifier: "UITableViewCell")
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = theme.colors.darkBlue
        
        switch section {
        case .ui:
            let category = GeneralRow(rawValue: indexPath.row)!
            cell.textLabel?.text = category.title
            return cell
        
        case .exposureKeys:
            let category = ExposureKeysRow(rawValue: indexPath.row)!
            cell.textLabel?.text = category.title
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let section = Section(rawValue: indexPath.section)!
        
        switch section {
        case .ui:
            switch GeneralRow(rawValue: indexPath.row)! {
            case .showOnboarding: delegate?.showOnboarding(self)
            case .resetOnboarded: delegate?.resetOnboarded(self)
            case .showExposureScreen: delegate?.showExposureScreen(self)
            case .showEndpointConfiguration: delegate?.showEndpointConfig(self)
            }
            
        case .exposureKeys:
            switch ExposureKeysRow(rawValue: indexPath.row)! {
            case .sendExposureKeys: delegate?.sendExposureKeySets(self)
            case .testExposureDetection: delegate?.detectExposures(self)
            case .addMockExposure: delegate?.addMockExposure(self)
            case .sendLocalNotfication: delegate?.sendLocalNotification(self)
            case .changeENConfig: delegate?.changeENConfig(self)
            }
        }
    }
}
