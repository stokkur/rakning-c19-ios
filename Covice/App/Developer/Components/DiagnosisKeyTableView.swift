//
//  DiagnosisKeyTableView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class DiagnosisKeyTableView: UITableView {
    init() {
        super.init(frame: .zero, style: .plain)
        translatesAutoresizingMaskIntoConstraints = false
        
        separatorStyle = .none
        backgroundColor = .clear
        
        estimatedRowHeight = 100.scaled
        rowHeight = UITableView.automaticDimension
        
        tableFooterView = UIView()
        tableHeaderView = UIView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DiagnosisKeyCell: UITableViewCell {
    private let theme: Theme
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 18.scaled, weight: .semibold)
        view.textColor = theme.colors.darkBlue
        view.translatesAutoresizingMaskIntoConstraints = false
        view.adjustsFontSizeToFitWidth = true
        
        return view
    }()

    lazy var subtitle: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 16.scaled, weight: .regular)
        view.textColor = theme.colors.darkBlue
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: DiagnosisKeyCell.reuseIdentifier)
        backgroundColor = .clear
        build()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        addSubview(title)
        addSubview(subtitle)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: self.topAnchor),
            title.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            title.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 20.scaled),
        ])

        NSLayoutConstraint.activate([
            subtitle.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 10.scaled),
            subtitle.leadingAnchor.constraint(equalTo: title.leadingAnchor),
            subtitle.trailingAnchor.constraint(equalTo: title.trailingAnchor),
            subtitle.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
    
    func configure(title: String?, subtitle: String?) {
        self.title.text = title
        self.subtitle.text = subtitle
    }

}
