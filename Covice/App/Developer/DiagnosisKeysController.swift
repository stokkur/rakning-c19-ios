//
//  DiagnosisKeysController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import ExposureNotification

protocol DiagnosisKeysControllerDelegate {
    func didFinish(_ diagnosisKeysController: DiagnosisKeysController)
}

class DiagnosisKeysController: ViewController {
    var delegate: DeveloperControllerDelegate?
    
    var keys: [ENTemporaryExposureKey] = [] {
        didSet {
            
        }
    }
    
    override func loadView() {
        let diagnosisKeysView = DiagnosisKeysView(theme: theme)
        diagnosisKeysView.tableView.delegate = self
        diagnosisKeysView.tableView.dataSource = self
        self.internalView = diagnosisKeysView
        
        ENManager().getTestDiagnosisKeys() { keys, error in
            self.keys = keys ?? []
            diagnosisKeysView.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension DiagnosisKeysController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let key = keys[indexPath.section]
        let cell = DiagnosisKeyCell(theme: theme)
        cell.configure(title: "\(key.rollingStartNumber)", subtitle: "\(key.rollingPeriod)")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let spacing = UIView()
        return spacing
    }
}
