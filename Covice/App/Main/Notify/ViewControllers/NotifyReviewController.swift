//
//  NotifyReviewController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol NotifyReviewControllerDelegate {
    func didFinish(_ notifyReviewController: NotifyReviewController)
    func didPressBack(_ notifyReviewController: NotifyReviewController)
    func didShare(_ notifyReviewController: NotifyReviewController)
}

class NotifyReviewController: ViewController {
    var delegate: NotifyReviewControllerDelegate?
    
    private let exposureManager: ExposureManager
    private let coreDataManager: CoreDataManager
    private let networkManager: NetworkManager
    private let localStorageManager: LocalStorageManager
    private var exposureReport: ExposureReport
    
    required init(theme: Theme, exposureManager: ExposureManager, coreDataManager: CoreDataManager, networkManager: NetworkManager, localStorageManager: LocalStorageManager, report: ExposureReport) {
        self.exposureManager = exposureManager
        self.coreDataManager = coreDataManager
        self.networkManager = networkManager
        self.localStorageManager = localStorageManager
        self.exposureReport = report
        
        super.init(theme: theme)
    }
    
    override func loadView() {
        self.title = Localization.share_review_title
        self.navigationItem.leftBarButtonItem = .init(image: UIImage.close, style: .done, target: self, action: #selector(close))
        
        let review = NotifyReviewView(theme: theme)
        
        // Update UI, if user has shared the report 
        review.hasShared = exposureReport.hasShared
        
        // Set Status text
        review.covidStatus = exposureReport.verification?.testType?.localizedString
        
        // Set symptoms information text
        if let onsetInterval = exposureReport.onsetInterval {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            let date = Date(timeIntervalSince1970: Double(onsetInterval))
            let dateStr = formatter.string(from: date)
            
            review.symptomInfo = Localization.share_review_onset_date + " \(dateStr)"
        } else {
            switch exposureReport.symptoms.type {
            case .no:
                review.symptomInfo = Localization.share_review_onset_no_symptoms
            
            default:
                review.symptomInfo = Localization.share_review_onset_no_answer
            }
        } 
        
        // Set traveling information text
        switch exposureReport.traveling.type {
        case .yes:
            review.travelHistory = Localization.share_review_travel_confirmed
        
        case .no:
            review.travelHistory = Localization.share_review_travel_no_travel
        
        default:
            review.travelHistory = Localization.share_review_travel_no_answer
        }
        
        review.delegate = self
        
        self.internalView = review
    }
    
    @objc func close() {
        delegate?.didFinish(self)
    }
}

// MARK: - NotifyReviewViewDelegate
extension NotifyReviewController: NotifyReviewViewDelegate {
    func didPressBack(_ notifyReviewView: NotifyReviewView) {
        delegate?.didPressBack(self)
    }
    
    func didPressDelete(_ notifyReviewView: NotifyReviewView) {
        let alert = CustomAlertController(theme: theme, title: Localization.delete_test_result_title, content: Localization.delete_test_result_detail)
        
        let cancel = Button(title: Localization.btn_cancel, theme: theme)
        cancel.style = .ghost
        alert.addAction(button: cancel)
        
        let delete = Button(title: Localization.btn_delete, theme: theme)
        delete.action = { [weak self] in
            guard let this = self else { return }
            
            this.coreDataManager.delete(entities: [this.exposureReport]) { [weak self] error in
                guard let this = self else { return }
                
                if let error = error {
                    DispatchQueue.main.async {
                        self?.showError(error)
                    }
                } else {
                    DispatchQueue.main.async {
                        self?.delegate?.didFinish(this)
                    }
                }
            }
        }
        alert.addAction(button: delete)
        present(alert, animated: true)
    }
    
    func didPressShare(_ notifyReviewView: NotifyReviewView) {
        notifyReviewView.setPublishStatus(to: .loading)
        
        exposureManager.getDiagnosisKeys() { result in
            switch result {
            case .success(let keys):
                guard let token = self.exposureReport.verification?.token else { return }
                let signature = keys.asHMACsignature
                
                self.networkManager.getCertificate(request: .init(token: token,
                                                                  ekeyhmac: signature.ekeyhmac)) { result in
                    switch result {
                    case .success(let certificate):
                        let revisionToken = self.localStorageManager.revisionToken
                        let hasTraveled = self.exposureReport.hasTraveled
                        let onsetInterval = self.exposureReport.onsetInterval
                        
                        self.networkManager.publishKeys(request: .init(temporaryExposureKeys: keys,
                                                                       hmacKey: signature.hmackey,
                                                                       verificationPayload: certificate.certificate,
                                                                       traveler: hasTraveled,
                                                                       symptomOnsetInterval: onsetInterval,
                                                                       revisionToken: revisionToken.wrappedValue)) { result in
                            switch result {
                            case .success(let publish):
                                self.localStorageManager.revisionToken.wrappedValue = publish.revisionToken
                                self.exposureReport.publish = publish
                                
                                self.coreDataManager.update(entities: [self.exposureReport]) { [weak self] error in
                                    guard let this = self else { return }
                                    if let error = error {
                                        DispatchQueue.main.async {
                                            self?.showError(error)
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            notifyReviewView.setPublishStatus(to: .success)
                                            self?.delegate?.didShare(this)
                                        }
                                    }
                                }
                            case .failure(let error):
                                DispatchQueue.main.async {
                                    notifyReviewView.setPublishStatus(to: .error)
                                    self.showError(error)
                                }
                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            notifyReviewView.setPublishStatus(to: .error)
                            self.showError(error)
                        }
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    notifyReviewView.setPublishStatus(to: .error)
                    self.showError(error)
                }
            }
        }
    }
}
