//
//  NotifyVerificationController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import Combine
import CoreData

protocol NotifyVerificationControllerDelegate {
    func didFinish(_ notifyVerificationController: NotifyVerificationController)
    func didPressBack(_ notifyVerificationController: NotifyVerificationController)
    func didPressContinue(_ notifyVerificationController: NotifyVerificationController, report: ExposureReport)
}

class NotifyVerificationController: ViewController {
    var delegate: NotifyVerificationControllerDelegate?
    
    private let networkManager: NetworkManager
    private let coreDataManager: CoreDataManager
    private var exposureReport: ExposureReport 
    private var cancellables = Set<AnyHashable>()
    
    required init(theme: Theme, networkManager: NetworkManager, coreDataManager: CoreDataManager, report: ExposureReport) {
        self.networkManager = networkManager
        self.exposureReport = report
        self.coreDataManager = coreDataManager
        
        super.init(theme: theme)
        
        if #available(iOS 13.0, *) {
            let sub = coreDataManager.publisher(for: ExposureReport.self).sink(receiveValue: { [weak self] reports in
                guard let updatedReport = reports.first(where: { $0.id == self?.exposureReport.id }) else { return }
                
                self?.exposureReport = updatedReport
            })//.store(in: &cancellables)
            _ = cancellables.insert(sub)
        } else {
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(didMergeChangesObjectsIDs),
                name: Notification.Name.NSManagedObjectContextDidMergeChangesObjectIDs,
                object: self.coreDataManager.coreData.viewContext
            )
        }
    }
    
    override func loadView() {
        self.title = Localization.verify_test_result_title
        self.navigationItem.leftBarButtonItem = .init(image: UIImage.close, style: .done, target: self, action: #selector(close))
    
        let verification = NotifyVerificationView(theme: theme)
        verification.delegate = self
        
        let status: VerificationRequestStatus = exposureReport.verification?.token == nil
            ? .none
            : .success
        verification.verificationCode.text = exposureReport.verificationCode
        verification.setVerificationStatus(to: status)
        
        self.internalView = verification
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            let view = self?.internalView as? NotifyVerificationView
            view?.verificationCode.becomeFirstResponder()
        }
        
    }
    
    @objc func close() {
        delegate?.didFinish(self)
    }
    
    @objc func didMergeChangesObjectsIDs(notification: NSNotification) {
        let updated = notification.userInfo?[NSUpdatedObjectIDsKey] as? Set<NSManagedObjectID> ?? Set()
        
        let reports = updated
            .filter({ id in id.entity == ExposureReport.ManagedObject.entity() })
            .compactMap({ id in self.coreDataManager.coreData.viewContext.object(with: id) as? ExposureReport.ManagedObject })
            .compactMap({ managedObject in managedObject.toEntity() })
        guard let updatedReport = reports.first(where: { $0.id == self.exposureReport.id }) else { return }
        self.exposureReport = updatedReport
    }
}

// MARK: - NotifyVerificationViewDelegate
extension NotifyVerificationController: NotifyVerificationViewDelegate {
    func didPressBack(_ notifyVerificationView: NotifyVerificationView) {
        delegate?.didPressBack(self)
    }
    
    func didPressVerify(_ notifyVerificationView: NotifyVerificationView, withVerificationCode code: String) {
        notifyVerificationView.setVerificationStatus(to: .loading)
        
        let request = VerificationRequest(code: code)
        networkManager.getVerificationToken(request: request) { [weak self] result in
            guard let this = self else { return }
            
            switch result {
            case .success(let verification):
                this.exposureReport.verification = verification
                this.exposureReport.verificationCode = code
                this.coreDataManager.update(entities: [this.exposureReport]) { error in
                    if let error = error {
                        DispatchQueue.main.async {
                            this.showError(error)
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    notifyVerificationView.setVerificationStatus(to: .success)
                }
            case .failure(_ ):
                DispatchQueue.main.async {
                    notifyVerificationView.setVerificationStatus(to: .error)
                }
            }
        }
    }
    
    func didPressContinue(_ notifyVerificationView: NotifyVerificationView) {
        delegate?.didPressContinue(self, report: exposureReport)
    }
}
