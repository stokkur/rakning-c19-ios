//
//  NotifyController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import Combine

protocol NotifyControllerDelegate {
    func presentModal(_ notifyController: NotifyController)
    func presentModal(_ notifyController: NotifyController, forReport report: ExposureReport)
}

class NotifyController: ViewController {
    private var exposureReports: [ExposureReport] = [] {
        didSet {
            let notify = self.internalView as? NotifyView
            notify?.tableView.reloadData()
        }
    }
    
    private let coreDataManager: CoreDataManager
    private let notificationCenter = NotificationCenter.default
    var delegate: NotifyControllerDelegate?
    
    init(theme: Theme, coreDataManager: CoreDataManager) {
        self.coreDataManager = coreDataManager
        super.init(theme: theme,relocalizable: true)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(fetchExposureReports),
                                       name: .NSManagedObjectContextDidSave,
                                       object: nil)
    }
    
    @objc func fetchExposureReports() {
        coreDataManager.fetch() { (result: Result<[ExposureReport], CoreDataManagerError>) in
            switch result {
            case .success(let reports):
                DispatchQueue.main.async { [ weak self] in
                    self?.exposureReports = reports
                }
            case .failure(let error):
                DispatchQueue.main.async { [ weak self] in
                    self?.showError(error)
                }
            }
        }
    }
    
    override func loadView() {
        self.title = Localization.home_tab_notify_text
        
        let notify = NotifyView(theme: theme)
        notify.tableView.delegate = self
        notify.tableView.dataSource = self
        
        self.internalView = notify
        
        fetchExposureReports()
    }
    
    override func relocalizeContent() {
        self.title = Localization.home_tab_notify_text
        guard let notifyView = self.internalView as? NotifyView else {
            return
        }
        notifyView.tableView.reloadData()
    }
    
}

extension NotifyController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exposureReports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = NotifyExposureReportCell(theme: theme)
        
        let report = exposureReports[indexPath.row]
        let status = report.verification?.testType?.localizedString
        
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        let subtitle = formatter.string(from: report.date)
        
        let shared = report.hasShared
        
        cell.configure(status: status, subtitle: subtitle, shared: shared)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = NotifyHeaderView(theme: theme)
        header.delegate = self
        
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let report = exposureReports[indexPath.row]
        
        delegate?.presentModal(self, forReport: report)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.view.bounds.height
    }
}


extension NotifyController: NotifyHeaderViewDelegate {
    func shareTestResultsPressed(_ notifyHeaderView: NotifyHeaderView) {
        delegate?.presentModal(self)
    }
    
    func exposureReportPressed(_ notifyHeaderView: NotifyHeaderView, exposureReport report: ExposureReport) {
        delegate?.presentModal(self, forReport: report)
    }
}
