//
//  NotifyTravelerController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import Combine
import CoreData

protocol NotifyTravelerControllerDelegate {
    func didFinish(_ notifyTravelerController: NotifyTravelerController)
    func didPressBack(_ notifyTravelerController: NotifyTravelerController)
    func didPressContinue(_ notifyTravelerController: NotifyTravelerController, report: ExposureReport)
}

class NotifyTravelerController: ViewController {
    var delegate: NotifyTravelerControllerDelegate?
    
    private var exposureReport: ExposureReport
    private let coreDataManager: CoreDataManager
    private var cancellables = Set<AnyHashable>()
    
    required init(theme: Theme, coreDataManager: CoreDataManager, report: ExposureReport) {
        self.coreDataManager = coreDataManager
        self.exposureReport = report
        
        super.init(theme: theme)
        
        if #available(iOS 13.0, *) {
            let sub = coreDataManager.publisher(for: ExposureReport.self).sink(receiveValue: { [weak self] reports in
                guard let updatedReport = reports.first(where: { $0.id == self?.exposureReport.id }) else { return }
                
                self?.exposureReport = updatedReport
            })
            _ = cancellables.insert(sub)
        } else {
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(didMergeChangesObjectsIDs),
                name: Notification.Name.NSManagedObjectContextDidMergeChangesObjectIDs,
                object: self.coreDataManager.coreData.viewContext
            )
        }
    }
    
    override func loadView() {
        self.title = Localization.share_travel_title
        
        self.navigationItem.leftBarButtonItem = .init(image: UIImage.close, style: .done, target: self, action: #selector(close))
        
        let traveler = NotifyTravelerView(theme: theme, initialSelection: exposureReport.traveling.type)
        traveler.delegate = self
        
        self.internalView = traveler
    }
    
    @objc func close() {
        delegate?.didFinish(self)
    }
    
    @objc func didMergeChangesObjectsIDs(notification: NSNotification) {
        let updated = notification.userInfo?[NSUpdatedObjectIDsKey] as? Set<NSManagedObjectID> ?? Set()
        
        let reports = updated
            .filter({ id in id.entity == ExposureReport.ManagedObject.entity() })
            .compactMap({ id in self.coreDataManager.coreData.viewContext.object(with: id) as? ExposureReport.ManagedObject })
            .compactMap({ managedObject in managedObject.toEntity() })
        guard let updatedReport = reports.first(where: { $0.id == self.exposureReport.id }) else { return }
        self.exposureReport = updatedReport
    }
}

// MARK: - NotifyTravelerViewDelegate
extension NotifyTravelerController: NotifyTravelerViewDelegate {
    func didSelectOption(_ notifyTravelerView: NotifyTravelerView, option: TravelingOption) {
        exposureReport.traveling.type = option
        
        coreDataManager.update(entities: [exposureReport]) { [weak self] error in
            if let error = error {
                DispatchQueue.main.async {
                    self?.showError(error)
                }
            }
        }
    }
    
    func didPressBack(_ notifyTravelerView: NotifyTravelerView) {
        delegate?.didPressBack(self)
    }
    
    func didPressContinue(_ notifyTravelerView: NotifyTravelerView) {
        delegate?.didPressContinue(self, report: exposureReport)
    }
}
