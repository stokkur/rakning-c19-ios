//
//  NotifyCompleteController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol NotifyCompleteControllerDelegate {
    func didFinish(_ notifyCompleteController: NotifyCompleteController)
}

class NotifyCompleteController: ViewController {
    var delegate: NotifyCompleteControllerDelegate?
    
    override func loadView() {
        self.navigationItem.leftBarButtonItem = .init(image: UIImage.close, style: .done, target: self, action: #selector(close))
        
        let complete = NotifyCompleteView(theme: theme)
        complete.delegate = self
        
        internalView = complete
    }
    
    @objc func close() {
        delegate?.didFinish(self)
    }
}

extension NotifyCompleteController: NotifyCompleteViewDelegate {
    func didPressDone(_ notifyCompleteView: NotifyCompleteView) {
        delegate?.didFinish(self)
    }
}
