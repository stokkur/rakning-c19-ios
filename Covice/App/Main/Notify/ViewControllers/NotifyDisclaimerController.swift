//
//  NotifyDisclaimerController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import Combine
import CoreData

protocol NotifyDisclaimerControllerDelegate {
    func didFinish(_ notifyDisclaimerController: NotifyDisclaimerController)
    func didPressContinue(_ notifyDisclaimerController: NotifyDisclaimerController, report: ExposureReport)
}

class NotifyDisclaimerController: ViewController {
    var delegate: NotifyDisclaimerControllerDelegate?
    private var exposureReport: ExposureReport
    private var coreDataManager: CoreDataManager
    private var cancellables = Set<AnyHashable>()
    
    required init(theme: Theme, coreDataManager: CoreDataManager, report: ExposureReport) {
        self.exposureReport = report
        self.coreDataManager = coreDataManager
        super.init(theme: theme)
        
        if #available(iOS 13.0, *){
            let sub = coreDataManager.publisher(for: ExposureReport.self).sink(receiveValue: { [weak self] reports in
                guard let updatedReport = reports.first(where: { $0.id == self?.exposureReport.id }) else { return }
                self?.exposureReport = updatedReport
            })//.store(in: &cancellables)
            _ = cancellables.insert(sub)
        } else {
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(didMergeChangesObjectsIDs),
                name: Notification.Name.NSManagedObjectContextDidMergeChangesObjectIDs,
                object: self.coreDataManager.coreData.viewContext
            )
            
        }
    }
    
    override func loadView() {
        self.title = Localization.share_begin_title
        self.navigationItem.leftBarButtonItem = .init(image: UIImage.close, style: .done, target: self, action: #selector(close))
        
        let disclaimer = NotifyDisclaimerView(theme: theme)
        disclaimer.delegate = self
        
        self.internalView = disclaimer
    }
    
    @objc func close() {
        delegate?.didFinish(self)
    }
    
    @objc func didMergeChangesObjectsIDs(notification: NSNotification) {
        let updated = notification.userInfo?[NSUpdatedObjectIDsKey] as? Set<NSManagedObjectID> ?? Set()
        
        let reports = updated
            .filter({ id in id.entity == ExposureReport.ManagedObject.entity() })
            .compactMap({ id in self.coreDataManager.coreData.viewContext.object(with: id) as? ExposureReport.ManagedObject })
            .compactMap({ managedObject in managedObject.toEntity() })
        guard let updatedReport = reports.first(where: { $0.id == self.exposureReport.id }) else { return }
        self.exposureReport = updatedReport
    }
}

// MARK: - NotifyDisclaimerViewDelegate
extension NotifyDisclaimerController: NotifyDisclaimerViewDelegate {
    func didPressBack(_ notifyDisclaimerView: NotifyDisclaimerView) {
        delegate?.didFinish(self)
    }
    
    func didPressContinue(_ notifyDisclaimerView: NotifyDisclaimerView) {
        delegate?.didPressContinue(self, report: exposureReport)
    }
}
