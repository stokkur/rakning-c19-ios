//
//  NotifyCoordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class NotifyCoordinator: Coordinator {
    required init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.navigationController = NavigationController(theme: theme)
        self.theme = theme
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.storageManager = storageManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
    }
    
    var coreDataManager: CoreDataManager
    
    var networkManager: NetworkManager
    
    var exposureManager: ExposureManager
    
    var storageManager: StorageManager
    
    var localStorageManager: LocalStorageManager
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    var theme: Theme
    
    @discardableResult func start() -> UINavigationController {
        let notify = NotifyController(theme: theme, coreDataManager: coreDataManager)
        notify.delegate = self
        
        navigationController.pushViewController(notify, animated: false)
        
        return self.navigationController
    }
}

// MARK: - NotifyFlow
extension NotifyCoordinator: NotifyControllerDelegate, NotifySubmissionCoordinatorDelegate {
    func presentModal(_ notifyController: NotifyController) {
        let modalCoordinator = NotifySubmissionCoordinator(navigationController: navigationController,
                                                           theme: theme,
                                                           networkManager: networkManager,
                                                           exposureManager: exposureManager,
                                                           storageManager: storageManager,
                                                           localStorageManager: localStorageManager,
                                                           coreDataManager: coreDataManager)
        modalCoordinator.delegate = self
        modalCoordinator.start()
        addChildCoordinator(modalCoordinator)
    }
    
    func presentModal(_ notifyController: NotifyController, forReport report: ExposureReport) {
        let modalCoordinator = NotifySubmissionCoordinator(navigationController: navigationController,
                                                           theme: theme,
                                                           networkManager: networkManager,
                                                           exposureManager: exposureManager,
                                                           storageManager: storageManager,
                                                           localStorageManager: localStorageManager,
                                                           coreDataManager: coreDataManager)
        modalCoordinator.exposureReport = report
        modalCoordinator.delegate = self
        modalCoordinator.start()
        addChildCoordinator(modalCoordinator)
    }
    
    func didFinish(_ notifySubmissionCoordinator: NotifySubmissionCoordinator) {
        removeChildCoordinator(notifySubmissionCoordinator)
    }
}
