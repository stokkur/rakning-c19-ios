//
//  ExposureReport.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

enum ReportStep: Int, Comparable {
    case disclaimer
    case verification
    case symptoms
    case traveler
    case review
    case complete
    
    static func < (lhs: ReportStep, rhs: ReportStep) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}

struct ExposureReport {
    let id: UUID
    let date: Date
    var verificationCode: String?
    var verification: Verification?
    var symptoms: Symptoms
    var traveling: Traveling
    var publish: Publish?
    
    init(id: UUID = UUID(), date: Date = Date(), verificationCode: String? = nil, verification: Verification? = nil, symptoms: Symptoms = Symptoms(), traveling: Traveling = Traveling(), publish: Publish? = nil) {
        self.id = id
        self.date = date
        self.verificationCode = verificationCode
        self.verification = verification
        self.symptoms = symptoms
        self.traveling = traveling
        self.publish = publish
    }
    
    var onsetInterval: Int? {
        return verification?.onsetInterval ?? symptoms.onsetInterval
    }
    
    var hasTraveled: Bool {
        return traveling.type == TravelingOption.yes
    }
    
    var hasShared: Bool {
        return publish != nil
    }
    
    func getCurrentStep() -> ReportStep {
        if verification == nil { return .disclaimer }
        if verification?.symptomDate == nil && symptoms.type == nil { return .symptoms }
        if traveling.type == nil { return .traveler }
        if publish == nil { return .review }
        
        return .complete
    }
}
