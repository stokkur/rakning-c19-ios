//
//  NotifySymptomsView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol NotifySymptomsViewDelegate {
    func didPressBack(_ notifySymptomsView: NotifySymptomsView)
    func didPressContinue(_ notifySymptomsView: NotifySymptomsView)
    func didSelectOption(_ notifySymptomsView: NotifySymptomsView, option: SymptomsOption, withDate date: Date?)
}

enum SymptomsOption: Int32, RadioButtonOption {
    case yes = 0
    case no = 1
    case noAnswer = 2
    
    var title: String {
        switch self {
        case .yes: return Localization.share_onset_symptoms
        case .no: return Localization.share_onset_no_symptoms
        case .noAnswer: return Localization.share_onset_no_answer
        }
    }
    
    var type: RadioButtonType {
        switch self {
        case .yes: return .datePicker
        default: return .normal
        }
    }
}

class NotifySymptomsView: View {
    private var selectedOption: SymptomsOption? {
        didSet {
            continueButton.isEnabled = selectedOption != nil
        }
    }
    
    
    required init(theme: Theme, initialSelection: SymptomsOption?) {
        selectedOption = initialSelection
        super.init(theme: theme)
    }
    
    private lazy var subtitle: UILabel = {
        let label = UILabel()
        label.text = Localization.share_onset_subtitle
        label.font = .systemFont(ofSize: 18.scaled, weight: .regular)
        label.numberOfLines = 0
        label.textColor = theme.colors.darkBlue
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var radioButtonSelection: RadioButtonSelection<SymptomsOption> = {
        let view = RadioButtonSelection<SymptomsOption>(theme: theme, initiallySelected: selectedOption)
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var backButton: Button = {
        let view = Button(title: Localization.btn_back, theme: theme)
        view.style = .info
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressBack(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var continueButton: Button = {
        let view = Button(title: Localization.btn_continue, theme: theme)
        view.style = .primary
        view.isEnabled = selectedOption != nil
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressContinue(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var delegate: NotifySymptomsViewDelegate?
    
    override func build() {
        super.build()
        
        addSubview(subtitle)
        addSubview(radioButtonSelection)
        addSubview(backButton)
        addSubview(continueButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            subtitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 20.scaled),
            subtitle.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            subtitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
        ])
        
        NSLayoutConstraint.activate([
            radioButtonSelection.topAnchor.constraint(equalTo: subtitle.bottomAnchor, constant: 40.scaled),
            radioButtonSelection.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40.scaled),
            radioButtonSelection.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40.scaled),
        ])
        
        NSLayoutConstraint.activate([
            backButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            backButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
        
        NSLayoutConstraint.activate([
            continueButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            continueButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
    }
}

extension NotifySymptomsView: RadioButtonSelectionDelegate {
    func didSelectOption<T>(_ radioButtonSelection: RadioButtonSelection<T>, option: T) {
        guard let option = option as? SymptomsOption else { return }
        self.selectedOption = option
        delegate?.didSelectOption(self, option: option, withDate: nil)
    }
    
    func didSelectDateOption<T>(_ radioButtonSelection: RadioButtonSelection<T>, option: T, date: Date) {
        guard let option = option as? SymptomsOption else { return }
        self.selectedOption = option
        delegate?.didSelectOption(self, option: option, withDate: date)
    }
}
