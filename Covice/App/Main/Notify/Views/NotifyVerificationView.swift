//
//  NotifyVerificationView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol NotifyVerificationViewDelegate {
    func didPressBack(_ notifyVerificationView: NotifyVerificationView)
    func didPressVerify(_ notifyVerificationView: NotifyVerificationView, withVerificationCode code: String)
    func didPressContinue(_ notifyVerificationView: NotifyVerificationView)
}

enum VerificationRequestStatus {
    case none
    case loading
    case success
    case error
}

class NotifyVerificationView: View {
    
    private let verificationCodeLength = 8
    
    lazy var verificationCode: UITextField = {
        let view = UITextField()
        view.keyboardType = .numberPad
        view.backgroundColor = .clear
        view.layer.borderColor = theme.colors.darkGray.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 10.scaled
        view.tintColor = theme.colors.darkOrange
        view.font = .systemFont(ofSize: 18.scaled)
        view.attributedPlaceholder = .init(string: Localization.enter_your_test_identifier,
                                           attributes: [.font: UIFont.systemFont(ofSize: 18.scaled)])
        view.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20.scaled, height: 5.scaled))
        view.leftView = paddingView
        view.leftViewMode = .always
        view.bounds.inset(by: UIEdgeInsets(top: 19.scaled, left: 20.scaled, bottom: 19.scaled, right: 20.scaled))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textContentType = .oneTimeCode
        
        
        return view
    }()
    
    lazy var verificationStatusIndicator: VerificationRequestStatusComponent = {
        let view = VerificationRequestStatusComponent(theme: theme)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var explanationText: UILabel = {
        let label = UILabel()
        label.text = Localization.share_test_identifier_help
        label.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        label.textColor = theme.colors.darkBlue
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var learnMore: LinkLabel = {
        let label = LinkLabel(theme: theme)
        label.text = Localization.learn_more
        label.textColor = theme.colors.darkOrange
        label.font = .systemFont(ofSize: 15.scaled, weight: .semibold )
        label.action = {
            
            guard let url = URL(string: "https://covid.is"), UIApplication.shared.canOpenURL(url) else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        }
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var backButton: Button = {
        let view = Button(title: Localization.btn_back, theme: theme)
        view.style = .info
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressBack(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var continueButton: Button = {
        let view = Button(title: Localization.btn_verify, theme: theme)
        view.style = .primary
        view.action = { [weak self] in
            guard let this = self else { return }
            
            switch this.verificationStatus {
            case .error, .none:
                let code = this.verificationCode.text ?? ""
                guard code.count == this.verificationCodeLength else { return }
                this.delegate?.didPressVerify(this, withVerificationCode: code)
            case .loading: return
            case .success: this.delegate?.didPressContinue(this)
            }
        }
        view.isEnabled = false
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private var backBottomConstraint: NSLayoutConstraint?
    private var continueBottomConstraint: NSLayoutConstraint?
    
    private var verificationStatus: VerificationRequestStatus = .none {
        didSet {
            verificationStatusIndicator.requestStatus = verificationStatus
            
            switch verificationStatus {
            case .success:
                verificationCode.isEnabled = false
                continueButton.isLoading = false
                continueButton.isEnabled = true
                continueButton.title = Localization.btn_continue
            case .loading:
                verificationCode.isEnabled = false
                continueButton.isLoading = true
            case .error:
                verificationCode.isEnabled  = true
                continueButton.isLoading = false
                continueButton.title = Localization.btn_verify
            case .none:
                verificationCode.isEnabled = true
                continueButton.title = Localization.btn_verify
            }
        }
    }
    
    func setVerificationStatus(to status: VerificationRequestStatus) {
        verificationStatus = status
    }
    
    var delegate: NotifyVerificationViewDelegate?
    
    init(theme: Theme) {
        super.init(theme: theme)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        addGestureRecognizer(tapGesture)
    }
    
    override func build() {
        super.build()
        
        addSubview(verificationCode)
        addSubview(verificationStatusIndicator)
        addSubview(explanationText)
        addSubview(learnMore)
        addSubview(backButton)
        addSubview(continueButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            verificationCode.topAnchor.constraint(equalTo: self.topAnchor, constant: 30.scaled),
            verificationCode.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            verificationCode.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            verificationCode.heightAnchor.constraint(greaterThanOrEqualToConstant: 54.scaled),
        ])

        NSLayoutConstraint.activate([
            verificationStatusIndicator.topAnchor.constraint(equalTo: verificationCode.bottomAnchor, constant: 10.scaled),
            verificationStatusIndicator.leadingAnchor.constraint(equalTo: verificationCode.leadingAnchor, constant: 10.scaled),
            verificationStatusIndicator.trailingAnchor.constraint(equalTo: verificationCode.trailingAnchor, constant: -10.scaled),
        ])
        
        NSLayoutConstraint.activate([
            explanationText.topAnchor.constraint(equalTo: verificationStatusIndicator.bottomAnchor, constant: 10.0.scaled),
            explanationText.leadingAnchor.constraint(equalTo: verificationCode.leadingAnchor),
            explanationText.trailingAnchor.constraint(equalTo: verificationCode.trailingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            learnMore.topAnchor.constraint(equalTo: explanationText.bottomAnchor, constant: 10.scaled),
            learnMore.leadingAnchor.constraint(equalTo: verificationCode.leadingAnchor),
            learnMore.trailingAnchor.constraint(equalTo: verificationCode.trailingAnchor),
            learnMore.bottomAnchor.constraint(lessThanOrEqualTo: backButton.topAnchor, constant: 20.scaled),
        ])
        
        
        backButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled).isActive = true
        backBottomConstraint = backButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled)
        backBottomConstraint?.isActive = true
        
        continueButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled).isActive = true
        continueBottomConstraint = continueButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled)
        continueBottomConstraint?.isActive = true
        
    }
    
    @objc private func editingChanged(_ textField: UITextField) {
        let enableRequisite = textField.text?.count == verificationCodeLength
        let isVerified = verificationStatus == .success
        
        continueButton.isEnabled = isVerified || enableRequisite
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        backBottomConstraint?.constant = -13.scaled - keyboardHeight
        continueBottomConstraint?.constant = -13.scaled - keyboardHeight

        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0.25
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
        }
    }

    @objc private func keyboardWillHide(_ notification: Notification) {
        backBottomConstraint?.constant = -13.scaled
        continueBottomConstraint?.constant = -13.scaled

        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0.25
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
        }
    }
    
    @objc private func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        verificationCode.resignFirstResponder()
    }
}

extension NotifyVerificationView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text ?? "") + string
        
        return text.count <= verificationCodeLength
    }
}

