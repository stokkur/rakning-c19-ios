//
//  NotifyCompleteView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol NotifyCompleteViewDelegate {
    func didPressDone(_ notifyCompleteView: NotifyCompleteView)
}

class NotifyCompleteView: View {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = theme.colors.darkBlue
        label.text = Localization.share_confirm_title
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 22.scaled, weight: .semibold)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        if #available(iOS 13.0, *) {
            view.image = UIImage.checkmark
        } else {
            view.image = UIImage.checkmarkCircle
        }
        view.contentMode = .scaleAspectFit
        view.tintColor = theme.colors.darkBlue
        view.layer.shadowColor = theme.colors.darkBlue.cgColor
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var continueButton: Button = {
        let view = Button(title: Localization.btn_done, theme: theme)
        view.style = .primary
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressDone(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var delegate: NotifyCompleteViewDelegate?
    
    override func build() {
        super.build()
        
        addSubview(titleLabel)
        addSubview(imageView)
        addSubview(continueButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.scaled),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40.scaled),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40.scaled),
        ])
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 50.scaled),
            imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 130.scaled),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            continueButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            continueButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
    }
}
