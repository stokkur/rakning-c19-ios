//
//  NotifyReviewView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol NotifyReviewViewDelegate {
    func didPressBack(_ notifyReviewView: NotifyReviewView)
    func didPressShare(_ notifyReviewView: NotifyReviewView)
    func didPressDelete(_ notifyReviewView: NotifyReviewView)
}

enum PublishRequestStatus {
    case none
    case loading
    case success
    case error
}

class NotifyReviewView: View {
    
    var hasShared: Bool = false {
        didSet {
            backButton.isHidden = hasShared
            
            continueButton.style = hasShared ? .info : .primary
            continueButton.title = hasShared ? Localization.btn_delete_status : Localization.share_test_result
            continueButton.setTitleColor(hasShared ? theme.colors.red : theme.colors.white, for: .normal)
        }
    }
    
    var covidStatus: String? {
        didSet {
            covidStatusRow.subtitle = covidStatus
        }
    }
    
    var travelHistory: String? {
        didSet {
            travelHistoryRow.subtitle = travelHistory
        }
    }
    
    var symptomInfo: String? {
        didSet {
            symptomInfoRow.subtitle = symptomInfo
        }
    }
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView(image: UIImage.notify)
        view.contentMode = .scaleAspectFit
        view.layer.shadowColor = theme.colors.darkOrange.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .init(width: 0, height: 10.scaled)
        view.layer.shadowRadius = 10.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var covidStatusRow: NotifyReviewRowComponent = {
        let view = NotifyReviewRowComponent(theme: theme)
        view.title = Localization.share_review_status_subtitle.uppercased()
        view.showUnderline = true
        
        return view
    }()
    
    private lazy var travelHistoryRow: NotifyReviewRowComponent = {
        let view = NotifyReviewRowComponent(theme: theme)
        view.title = Localization.share_review_travel_subtitle.uppercased()
        view.showUnderline = true
        
        return view
    }()
    
    private lazy var symptomInfoRow: NotifyReviewRowComponent = {
        let view = NotifyReviewRowComponent(theme: theme)
        view.title = Localization.share_review_onset_subtitle.uppercased()
        view.showUnderline = false
        
        return view
    }()
    
    private lazy var optionStack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [covidStatusRow, travelHistoryRow, symptomInfoRow])
        stack.axis = .vertical
        stack.spacing = 20.scaled
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
        
    private lazy var backButton: Button = {
        let view = Button(title: Localization.btn_back, theme: theme)
        view.style = .info
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressBack(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var continueButton: Button = {
        let view = Button(title: Localization.share_test_result, theme: theme)
        view.style = .primary
        view.action = { [weak self] in
            guard let this = self else { return }
            
            if this.hasShared {
                this.delegate?.didPressDelete(this)
            } else {
                this.delegate?.didPressShare(this)
            }
            
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var delegate: NotifyReviewViewDelegate?
    
    override func build() {
        super.build()
        
        
        addSubview(imageView)
        addSubview(optionStack)
        addSubview(backButton)
        addSubview(continueButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.scaled),
            imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 200.scaled),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            optionStack.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 40.scaled),
            optionStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            optionStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            optionStack.bottomAnchor.constraint(lessThanOrEqualTo: backButton.topAnchor, constant: -20.scaled)
        ])
        
        NSLayoutConstraint.activate([
            backButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            backButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
        
        NSLayoutConstraint.activate([
            continueButton.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: 10.scaled),
            continueButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            continueButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
    }
    
    func setPublishStatus(to status: PublishRequestStatus) {
        switch status {
        case .loading: continueButton.isLoading = true
        default: continueButton.isLoading = false
        }
    }
}
