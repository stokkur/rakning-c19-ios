//
//  NotifyView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


class NotifyView: View {
    lazy var gradientView: GradientView = {
        let view  = GradientView(theme: theme)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.colors = [
            theme.colors.lightOrange,
            theme.colors.lightOrange.withAlphaComponent(0),
        ]
        view.locations = [0, 0.7]
        
        return view
    }()
    
    lazy var tableView = NotifyTableView()
    
    override func build() {
        super.build()
        
        addSubview(tableView)
        addSubview(gradientView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            gradientView.topAnchor.constraint(equalTo: self.topAnchor),
            gradientView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            gradientView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            gradientView.heightAnchor.constraint(equalToConstant: 15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: self.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
}

protocol NotifyHeaderViewDelegate {
    func shareTestResultsPressed(_ notifyView: NotifyHeaderView)
    func exposureReportPressed(_ notifyView: NotifyHeaderView, exposureReport report: ExposureReport)
}

class NotifyHeaderView: View {
    lazy var imageView: UIImageView = {
        let view = UIImageView(image: UIImage.notify)
        view.contentMode = .scaleAspectFit
        view.layer.shadowColor = theme.colors.darkOrange.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .init(width: 0, height: 10.scaled)
        view.layer.shadowRadius = 10.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var titleView: UILabel = {
        let view = UILabel()
        view.text = Localization.share_test_result_title
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 18.scaled, weight: .bold)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var bodyView: UILabel = {
        let view = UILabel()
        view.text = Localization.share_test_result_description
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 18.scaled, weight: .regular)
        view.textAlignment = .center
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var buttonView: Button = {
        let button = Button(title: Localization.btn_share_positive, theme: theme)
        button.style = .primary
        button.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.shareTestResultsPressed(this)
        }
        
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    var delegate: NotifyHeaderViewDelegate?
    
    override func build() {
        super.build()
        
        addSubview(imageView)
        addSubview(titleView)
        addSubview(bodyView)
        addSubview(buttonView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.scaled),
            imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4),
            imageView.widthAnchor.constraint(equalTo: self.heightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            titleView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            titleView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            titleView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 40.scaled),
        ])
        
        NSLayoutConstraint.activate([
            bodyView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            bodyView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            bodyView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 40.scaled),
        ])
        
        NSLayoutConstraint.activate([
            buttonView.topAnchor.constraint(greaterThanOrEqualTo: bodyView.bottomAnchor, constant: 30.scaled),
            buttonView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            buttonView.leadingAnchor.constraint(greaterThanOrEqualTo: self.leadingAnchor, constant: 20.scaled),
            buttonView.trailingAnchor.constraint(greaterThanOrEqualTo: self.trailingAnchor, constant: -20.scaled),
            buttonView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -40.scaled),
        ])
    }
}
