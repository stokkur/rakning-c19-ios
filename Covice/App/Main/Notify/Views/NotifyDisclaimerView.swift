//
//  NotifyDisclaimerView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol NotifyDisclaimerViewDelegate {
    func didPressBack(_ notifyDisclaimerView: NotifyDisclaimerView)
    func didPressContinue(_ notifyDisclaimerView: NotifyDisclaimerView)
}

class NotifyDisclaimerView: View {
    lazy var disclaimerComponent: DisclaimerComponent = {
        let view = DisclaimerComponent(theme: theme)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
        
    lazy var backButton: Button = {
        let view = Button(title: Localization.btn_back, theme: theme)
        view.style = .info
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressBack(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var continueButton: Button = {
        let view = Button(title: Localization.btn_continue, theme: theme)
        view.style = .primary
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressContinue(this)
        }
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    var delegate: NotifyDisclaimerViewDelegate?
    
    override func build() {
        super.build()
        
        addSubview(backButton)
        addSubview(continueButton)
        addSubview(disclaimerComponent)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            disclaimerComponent.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            disclaimerComponent.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            disclaimerComponent.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -40.scaled)
        ])
        
        NSLayoutConstraint.activate([
            backButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            backButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
        
        NSLayoutConstraint.activate([
            continueButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            continueButton.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled),
        ])
    }
}
