//
//  NotifySubmissionCoordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol NotifySubmissionCoordinatorDelegate {
    func didFinish(_ notifySubmissionCoordinator: NotifySubmissionCoordinator)
}

class NotifySubmissionCoordinator: Coordinator {
    required init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.navigationController = navigationController
        self.theme = theme
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.storageManager = storageManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
    }
    
    var coreDataManager: CoreDataManager
    
    var networkManager: NetworkManager
    
    var exposureManager: ExposureManager
    
    var storageManager: StorageManager
    
    var localStorageManager: LocalStorageManager
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    lazy var modalNavigationController: UINavigationController = {
        let controller = NavigationController(theme: theme)
        return controller
    }()
    
    var theme: Theme
    
    var exposureReport = ExposureReport()
    
    var delegate: NotifySubmissionCoordinatorDelegate?
    
    @discardableResult func start() -> UINavigationController {
        createFlowStack(from: exposureReport)
        navigationController.present(modalNavigationController, animated: true)
        
        return self.navigationController
    }
    
    private func createFlowStack(from report: ExposureReport) {
        let step = report.getCurrentStep()
        
        // We don't need to build a stack if the report has been shared
        if step == .complete {
            reviewFlow(report: report)
            return
        }        
        
        if step >= .disclaimer {
            disclaimerFlow(report: report)
        }
        
        if step >= .verification {
            verificationFlow(report: report)
        }
        
        if step >= .symptoms && report.verification?.symptomDate == nil {
            symptomsFlow(report: report)
        }
        
        if step >= .traveler {
            travelerFlow(report: report)
        }
        
        if step >= .review {
            reviewFlow(report: report)
        }
    }
}


// MARK: - Disclaimer Flow
extension NotifySubmissionCoordinator: NotifyDisclaimerControllerDelegate {
    func disclaimerFlow(report: ExposureReport) {
        let notify = NotifyDisclaimerController(theme: theme, coreDataManager: coreDataManager, report: exposureReport)
        notify.delegate = self
        modalNavigationController.pushViewController(notify, animated: false)
    }
    
    func didFinish(_ notifyDisclaimerController: NotifyDisclaimerController) {
        modalNavigationController.dismiss(animated: true)
        delegate?.didFinish(self)
    }
    
    func didPressContinue(_ notifyDisclaimerController: NotifyDisclaimerController, report: ExposureReport) {
        verificationFlow(report: report)
    }
}

// MARK: - Verification Flow
extension NotifySubmissionCoordinator: NotifyVerificationControllerDelegate {
    func verificationFlow(report: ExposureReport) {
        let verification = NotifyVerificationController(theme: theme, networkManager: networkManager, coreDataManager: coreDataManager, report: exposureReport)
        verification.delegate = self
        
        modalNavigationController.pushViewController(verification, animated: false)
    }
    
    func didFinish(_ notifyVerificationController: NotifyVerificationController) {
        modalNavigationController.dismiss(animated: true)
        delegate?.didFinish(self)
    }
    
    func didPressContinue(_ notifyVerificationController: NotifyVerificationController, report: ExposureReport) {
        let symptomOnsetProvided = report.verification?.symptomDate != nil
        
        if symptomOnsetProvided {
            travelerFlow(report: report)
        } else {
            symptomsFlow(report: report)
        }
    }
    
    func didPressBack(_ notifyVerificationController: NotifyVerificationController) {
        modalNavigationController.popViewController(animated: false)
    }
}

// MARK: - Symptoms Flow
extension NotifySubmissionCoordinator: NotifySymptomsControllerDelegate {
    func symptomsFlow(report: ExposureReport) {
        let symptoms = NotifySymptomsController(theme: theme, coreDataManager: coreDataManager, report: report)
        symptoms.delegate = self
        
        modalNavigationController.pushViewController(symptoms, animated: false)
    }
    
    func didFinish(_ notifySymptomsController: NotifySymptomsController) {
        modalNavigationController.dismiss(animated: true)
        delegate?.didFinish(self)
    }
    
    func didPressContinue(_ notifySymptomsController: NotifySymptomsController, report: ExposureReport) {
        travelerFlow(report: report)
    }
    
    func didPressBack(_ notifySymptomsController: NotifySymptomsController) {
        modalNavigationController.popViewController(animated: false)
    }
}

// MARK: - Traveler Flow
extension NotifySubmissionCoordinator: NotifyTravelerControllerDelegate {
    func travelerFlow(report: ExposureReport) {
        let traveler = NotifyTravelerController(theme: theme, coreDataManager: coreDataManager, report: report)
        traveler.delegate = self
        
        modalNavigationController.pushViewController(traveler, animated: false)
    }
    
    func didFinish(_ notifyTravelerController: NotifyTravelerController) {
        modalNavigationController.dismiss(animated: true)
        delegate?.didFinish(self)
    }
    
    func didPressContinue(_ notifyTravelerController: NotifyTravelerController, report: ExposureReport) {
        reviewFlow(report: report)
    }
    
    func didPressBack(_ notifyTravelerController: NotifyTravelerController) {
        modalNavigationController.popViewController(animated: false)
    }
}

// MARK: - Review Flow
extension NotifySubmissionCoordinator: NotifyReviewControllerDelegate {
    func reviewFlow(report: ExposureReport) {
        let review = NotifyReviewController(theme: theme,
                                            exposureManager: exposureManager,
                                            coreDataManager: coreDataManager,
                                            networkManager: networkManager,
                                            localStorageManager: localStorageManager,
                                            report: report)
        review.delegate = self
        
        modalNavigationController.pushViewController(review, animated: false)
    }
    
    func didFinish(_ notifyReviewController: NotifyReviewController) {
        modalNavigationController.dismiss(animated: true)
        delegate?.didFinish(self)
    }
    
    func didShare(_ notifyReviewController: NotifyReviewController) {
        completionFlow()
    }
    
    func didDelete(_ notifyReviewController: NotifyReviewController) {
        
    }
    
    func didPressBack(_ notifyReviewController: NotifyReviewController) {
        modalNavigationController.popViewController(animated: false)
    }
}

// MARK: - Completion Flow
extension NotifySubmissionCoordinator: NotifyCompleteControllerDelegate {
    func completionFlow() {
        let complete = NotifyCompleteController(theme: theme)
        complete.delegate = self
        
        modalNavigationController.pushViewController(complete, animated: false)
    }
    
    func didFinish(_ notifyCompleteController: NotifyCompleteController) {
        modalNavigationController.dismiss(animated: true)
        delegate?.didFinish(self)
    }
}

