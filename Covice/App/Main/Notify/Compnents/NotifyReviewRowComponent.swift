//
//  NotifyReviewRowComponent.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class NotifyReviewRowComponent: View {
    var showUnderline: Bool = true {
        didSet {
            underlineView.alpha = showUnderline ? 1 : 0
        }
    }
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var subtitle: String? {
        didSet {
            subtitleLabel.text = subtitle
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = theme.colors.darkBlue
        label.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = theme.colors.darkBlue
        label.font = .systemFont(ofSize: 15.scaled, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var underlineView: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.gray
        view.alpha = showUnderline ? 1 : 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func build() {
        super.build()
        
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(underlineView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 15.scaled),
            subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            subtitleLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            underlineView.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: 30.scaled),
            underlineView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            underlineView.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            underlineView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            underlineView.heightAnchor.constraint(equalToConstant: 1.scaled),
        ])
    }
}
