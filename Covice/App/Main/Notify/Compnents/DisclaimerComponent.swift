//
//  DisclaimerComponent.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class DisclaimerComponent: View {
    // MARK: - Header Components
    lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.darkBlue
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.clipsToBounds = true
        view.layer.cornerRadius = 10.scaled
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        return view
    }()
    
    lazy var COAIcon: UIImageView = {
        let view = UIImageView(image: UIImage.coatOfArms)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var titleText: UILabel = {
        let view = UILabel()
        view.text = Localization.onboarding_exposure_info_title
        view.font = .systemFont(ofSize: 17.scaled, weight: .semibold)
        view.textColor = theme.colors.white
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var subtitleText: UILabel = {
        let view = UILabel()
        view.text = Localization.agency_display_name
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.textColor = theme.colors.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    
    // MARK: - Body Component
    lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.white
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.clipsToBounds = true
        view.layer.cornerRadius = 10.scaled
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        return view
    }()
    
    lazy var bodyText: UILabel = {
        let view = UILabel()
        
        let attributedString = NSMutableAttributedString(string: Localization.agency_message)
        let paragraphstyle = NSMutableParagraphStyle()
        paragraphstyle.lineSpacing = 5.scaled
        attributedString.addAttribute(.paragraphStyle, value: paragraphstyle , range: NSRange(location: 0, length: Localization.agency_message.count - 1))
        view.attributedText = attributedString
        view.font = .systemFont(ofSize: 15.scaled, weight: .medium)
        view.textColor = theme.colors.darkBlue
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func build() {
        super.build()
        layer.cornerRadius = 10.scaled
        clipsToBounds = true
        
        addSubview(headerView)
        headerView.addSubview(COAIcon)
        headerView.addSubview(titleText)
        headerView.addSubview(subtitleText)
        
        addSubview(bodyView)
        bodyView.addSubview(bodyText)
        
        addSubview(bodyView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        // MARK: - Header constraints
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: self.topAnchor),
            headerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            COAIcon.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.scaled),
            COAIcon.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10.scaled),
            COAIcon.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -10.scaled),
            COAIcon.widthAnchor.constraint(equalTo: COAIcon.heightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            titleText.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10.scaled),
            titleText.leadingAnchor.constraint(equalTo: COAIcon.trailingAnchor, constant: 10.scaled),
            titleText.trailingAnchor.constraint(greaterThanOrEqualTo: headerView.trailingAnchor, constant: -10.scaled)
        ])
        
        NSLayoutConstraint.activate([
            subtitleText.topAnchor.constraint(equalTo: titleText.bottomAnchor),
            subtitleText.leadingAnchor.constraint(equalTo: titleText.leadingAnchor),
            subtitleText.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -10.scaled),
            subtitleText.trailingAnchor.constraint(greaterThanOrEqualTo: headerView.trailingAnchor, constant: -10.scaled),
        ])
        
        // MARK: - Body constraints
        NSLayoutConstraint.activate([
            bodyView.topAnchor.constraint(equalTo: self.headerView.bottomAnchor),
            bodyView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            bodyView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bodyView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            bodyText.topAnchor.constraint(equalTo: bodyView.topAnchor, constant: 20.scaled),
            bodyText.leadingAnchor.constraint(equalTo: bodyView.leadingAnchor, constant: 20.scaled),
            bodyText.trailingAnchor.constraint(equalTo: bodyView.trailingAnchor, constant: -20.scaled),
            bodyText.bottomAnchor.constraint(equalTo: bodyView.bottomAnchor, constant: -20.scaled),
        ])
    }
}
