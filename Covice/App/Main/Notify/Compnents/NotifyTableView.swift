//
//  NotifyTableView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class NotifyTableView: UITableView {
    init() {
        super.init(frame: .zero, style: .grouped)
        translatesAutoresizingMaskIntoConstraints = false
        
        separatorStyle = .singleLine
        backgroundColor = .clear
        contentInsetAdjustmentBehavior = .never
        
        estimatedRowHeight = 500
        rowHeight = UITableView.automaticDimension
        
        register(NotifyExposureReportCell.self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class NotifyExposureReportCell: UITableViewCell {
    private let theme: Theme
    
    private lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.text = Localization.test_result_subtitle.uppercased()
        label.font = .systemFont(ofSize: 13.scaled, weight: .regular)
        label.textColor = theme.colors.darkBlue
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15.scaled, weight: .medium)
        label.textColor = theme.colors.darkBlue
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15.scaled, weight: .medium)
        label.textColor = theme.colors.gray
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var sharingLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15.scaled, weight: .medium)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: NotifyExposureReportCell.reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        
        build()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        addSubview(statusLabel)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(sharingLabel)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            statusLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20.scaled),
            statusLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
        ])
    
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: statusLabel.bottomAnchor, constant: 20.scaled),
            titleLabel.leadingAnchor.constraint(equalTo: statusLabel.leadingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5.scaled),
            subtitleLabel.leadingAnchor.constraint(equalTo: statusLabel.leadingAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20.scaled),
        ])
        
        NSLayoutConstraint.activate([
            sharingLabel.topAnchor.constraint(equalTo: titleLabel.topAnchor),
            sharingLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
        ])
    }
    
    func configure(status: String?, subtitle: String?, shared: Bool) {
        titleLabel.text = status
        subtitleLabel.text = subtitle
        
        sharingLabel.text = shared
            ? Localization.positive_test_result_status_shared
            : Localization.positive_test_result_status_not_shared
        
        sharingLabel.textColor = shared ? theme.colors.green : theme.colors.red
    }
}
