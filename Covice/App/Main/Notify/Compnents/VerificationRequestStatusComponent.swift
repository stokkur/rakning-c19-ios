//
//  VerificationRequestStatusComponent.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


class VerificationRequestStatusComponent: View {
    var requestStatus: VerificationRequestStatus = .none {
        didSet {
            switch requestStatus {
            case .success:
                heightConstraint?.constant = 30
                // statusIcon.image = .checkmark
                if #available(iOS 13.0, *) {
                    statusIcon.image = .checkmark
                } else {
                    statusIcon.image = UIImage.checkmarkCircle
                }
                statusText.text = Localization.share_test_identifier_verified
                statusIcon.alpha = 1.0

            case .error:
                heightConstraint?.constant = 30
                statusText.text = Localization.notify_verify_code_failed
                if #available(iOS 13.0, *) {
                    statusIcon.image = .remove
                } else {
                    statusIcon.image = UIImage.closeCircle
                }
                statusIcon.alpha = 1.0
           default:
                heightConstraint?.constant = 0
                statusIcon.alpha = 0.0
            }

            UIView.animate(withDuration: 0.2) {
                self.layoutIfNeeded()
            }
        }
    }
    
    lazy var statusIcon: UIImageView = {
        let view = UIImageView()
        view.tintColor = theme.colors.darkBlue
        view.contentMode = .scaleAspectFit
        view.alpha = 0.0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var statusText: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    var heightConstraint: NSLayoutConstraint?
    
    override func build() {
        super.build()
       
        addSubview(statusIcon)
        addSubview(statusText)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        heightConstraint = self.heightAnchor.constraint(equalToConstant: 30.scaled)
        heightConstraint?.isActive = true
        
        NSLayoutConstraint.activate([
            statusIcon.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            statusIcon.topAnchor.constraint(equalTo: self.topAnchor),
            statusIcon.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            statusIcon.heightAnchor.constraint(equalToConstant: 26.scaled),
            statusIcon.widthAnchor.constraint(equalToConstant: 26.scaled),
        ])
        
        NSLayoutConstraint.activate([
            statusText.leadingAnchor.constraint(equalTo: statusIcon.trailingAnchor, constant: 20.scaled),
            statusText.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            statusText.trailingAnchor.constraint(lessThanOrEqualTo: self.trailingAnchor),
        ])
    }
}
