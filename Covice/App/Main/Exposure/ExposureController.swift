//
//  ExposureController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import ExposureNotification

class ExposureController: ViewController {
    private var exposureNotificationStatus: ExposureManagerStatus {
        didSet {
            evaluateState()
        }
    }
    
    private var exposures: [Exposure] = [] {
        didSet {
            evaluateState()
        }
    }
    
    private let exposureManager: ExposureManager
    private let coreDataManager: CoreDataManager
    private let notificationCenter = NotificationCenter.default
    
    init(theme: Theme, exposureManager: ExposureManager, coreDataManager: CoreDataManager) {
        self.exposureManager = exposureManager
        self.coreDataManager = coreDataManager
        self.exposureNotificationStatus = exposureManager.getExposureNotificationStatus()
        super.init(theme: theme)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(exposureNotificationStatusChanged),
                                       name: .exposureNotificationStatus,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(fetchExposures),
                                       name: .NSManagedObjectContextDidSave,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(languageChanged),
                                       name: Bundle.languageChangedNotificatioName,
                                       object: nil)
    }
    
    override func loadView() {
        self.title = Localization.home_tab_exposures_text
            
        evaluateState()
        
        fetchExposures()
    }
    
    @objc func fetchExposures() {
        coreDataManager.fetch() { (result: Result<[Exposure], CoreDataManagerError>) in
            switch result {
            case .success(let exposures):
                DispatchQueue.main.async { [ weak self] in
                    self?.exposures = exposures
                }
            case .failure(let error):
                DispatchQueue.main.async { [ weak self] in
                    self?.showError(error)
                }
            }
        }
    }
    
    @objc func languageChanged() {
        evaluateState()
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.title = Localization.home_tab_exposures_text
        }
    }
    
    @objc private func exposureNotificationStatusChanged(_ notification: Notification) {
        guard let status = notification.object as? ExposureManagerStatus else { return }
        exposureNotificationStatus = status
    }
    
    private func evaluateState() {
       switch exposureNotificationStatus {
        case .active where !exposures.isEmpty:
            let exposureView = ExposureView(theme: theme)
            exposureView.tableView.delegate = self
            exposureView.tableView.dataSource = self
            internalView = exposureView
            
        case .active where exposures.isEmpty:
            self.internalView = NoExposureView(theme: theme)
            
        case .authorizationDenied:
            let disabledView = ExposuresDisabledView(theme: theme, error: .authorizationDenied)
            disabledView.delegate = self
            
            self.internalView = disabledView
            
        case .notAuthorized:
            let disabledView = ExposuresDisabledView(theme: theme, error: .notAuthorized)
            disabledView.delegate = self
            
            self.internalView = disabledView
        
        case .inactive(let error):
            let disabledView = ExposuresDisabledView(theme: theme, error: error)
            disabledView.delegate = self
            
            self.internalView = disabledView
        
        default:
            let disabledView = ExposuresDisabledView(theme: theme, error: .unknown)
            disabledView.delegate = self
            
            self.internalView = disabledView
        }
    }
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension ExposureController: UITableViewDelegate, UITableViewDataSource {    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exposures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ExposureInformationCell(theme: theme)
        
        let exposure = exposures[indexPath.section]
        let status = Localization.notification_channel_name
        let exposureDate = exposure.date
        
        cell.configure(status: status, exposureDate: exposureDate)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = ExposureTableViewFooter(theme: theme)
        
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
}


// MARK: - OnboardingNotificationSettingsViewDelegate
extension ExposureController: ExposureDisabledViewDelegate {
    func didPressEnableExposureDetection(_ exposuresDisabledView: ExposuresDisabledView) {
        let alert = CustomAlertController(theme: theme, title: Localization.turn_on_exposure_notification_title, content: Localization.turn_on_exposure_notification_detail)
        
        let cancel = Button(title: Localization.btn_cancel, theme: theme)
        cancel.style = .ghost
        alert.addAction(button: cancel)
        
        let turnOn = Button(title: Localization.btn_turn_on, theme: theme)
        turnOn.action = {
            self.exposureManager.setExposureNotificationEnabled(true) { [weak self] error in
                guard let self = self else {
                    return
                }
                    NotificationCenter.default.post(name: ExposureManager.authorizationStatusChangeNotification, object: nil)
                    if let error = error {
                        switch error {
                        case .authorizationDenied:
                            fallthrough
                        case .notAuthorized:
                            let notAuthorizedAlert = CustomAlertController(theme: self.theme, title: Localization.not_autorized_error_title, content: Localization.not_autorized_error_content)
                            let cancel = Button(title: Localization.btn_done, theme: self.theme)
                            cancel.style = .ghost
                            notAuthorizedAlert.addAction(button: cancel)
                            self.present(notAuthorizedAlert, animated: true)
                            break
                        default:
                            self.showError(error)
                        }
                        
                    }
            }
        }
        alert.addAction(button: turnOn)
        present(alert, animated: true)
    }
    
    func didPressEnableBluetooth(_ exposuresDisabledView: ExposuresDisabledView) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: nil)
        }
    }
    
}
