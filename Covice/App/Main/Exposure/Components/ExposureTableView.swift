//
//  ExposureTableView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class ExposureTableView: UITableView {
    init() {
        super.init(frame: .zero, style: .plain)
        translatesAutoresizingMaskIntoConstraints = false
        
        separatorStyle = .singleLine
        backgroundColor = .clear
        contentInsetAdjustmentBehavior = .never
        
        estimatedRowHeight = 500
        rowHeight = UITableView.automaticDimension
        
        allowsSelection = false
        
        register(ExposureInformationCell.self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ExposureInformationCell: UITableViewCell {
    private let theme: Theme
    
    private lazy var statusView: TextExposureRow = {
        let view = TextExposureRow(theme: theme)
        view.title = Localization.exposure_details_status_subtitle
        view.image = UIImage.exposure
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var exposureDateView: TextExposureRow = {
        let view = TextExposureRow(theme: theme)
        view.title = Localization.exposure_details_date_exposed_subtitle
        view.image = UIImage.calendar
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var whatsNextView: ButtonExposureRow = {
        let view = ButtonExposureRow(theme: theme)
        view.title = Localization.exposure_details_next_steps
        view.image = UIImage.attention
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 20.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: ExposureInformationCell.reuseIdentifier)
        backgroundColor = .clear
        build()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        stackView.addArrangedSubview(statusView)
        stackView.addArrangedSubview(exposureDateView)
        stackView.addArrangedSubview(whatsNextView)
        contentView.addSubview(stackView)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15.scaled),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20.scaled),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15.scaled),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20.scaled),
        ])
    }
    
    func configure(status: String, exposureDate: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let dateString = dateFormatter.string(from: exposureDate)
        
        statusView.text = status
        exposureDateView.text = dateString
        whatsNextView.buttonTitle = Localization.exposure_details_click_here
        whatsNextView.buttonAction = {
            guard let url = URL(string: Localization.exposure_details_next_steps_link), UIApplication.shared.canOpenURL(url) else { return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

class ExposureTableViewFooter: GradientView {
    private lazy var disclaimerLabel: UILabel = {
        let view = UILabel()
        view.text = Localization.exposure_details_expiring_subtitle
        view.font = .systemFont(ofSize: 15.scaled, weight: .semibold)
        view.textColor = .darkGray
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func build() {
        super.build()
        
        self.colors = [
            theme.colors.lightOrange.withAlphaComponent(0),
            theme.colors.lightOrange,
        ]
        self.locations = [0, 0.3]
    
        addSubview(disclaimerLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            disclaimerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.scaled),
            disclaimerLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            disclaimerLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            disclaimerLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20.scaled)
        ])
    }
}
