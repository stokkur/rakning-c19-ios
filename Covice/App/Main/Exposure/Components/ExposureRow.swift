//
//  ExposureRow.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class ExposureRow: View {
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var image: UIImage? {
        didSet {
            iconView.image = image
        }
    }
    
    fileprivate lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .systemFont(ofSize: 14.scaled, weight: .light)
        view.textColor = .gray
        
        return view
    }()
    
    fileprivate lazy var container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func build() {
        super.build()

        container.addSubview(titleLabel)
        addSubview(container)
        addSubview(iconView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            iconView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            iconView.widthAnchor.constraint(equalToConstant: 40.scaled),
            iconView.heightAnchor.constraint(equalToConstant: 40.scaled),
            iconView.centerYAnchor.constraint(equalTo: container.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint(equalTo: self.topAnchor),
            container.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 20.scaled),
            container.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            container.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: container.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor),
        ])
    }
}


class TextExposureRow: ExposureRow {
    var text = "" {
        didSet {
            bodyText.text = text
        }
    }
    
    private lazy var bodyText: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.textColor = .black
        
        return view
    }()
    
    override func build() {
        super.build()
        container.addSubview(bodyText)
    }
        
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            bodyText.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5.scaled),
            bodyText.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            bodyText.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            bodyText.bottomAnchor.constraint(equalTo: container.bottomAnchor)
        ])
    }
}

class LinkExposureRow: ExposureRow {
    var link = "" {
        didSet {
            urlButton.setTitle(link, for: .normal)
        }
    }
    
    private lazy var urlButton: UIButton = {
        let view = UIButton(type: .system)
        view.setTitleColor(theme.colors.darkOrange, for: .normal)
        view.titleEdgeInsets = .zero
        view.contentHorizontalAlignment = .left
        view.addTarget(self, action: #selector(self.openURL), for: .touchUpInside)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let label = view.titleLabel
        label?.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        label?.textColor = .black
        label?.lineBreakMode = .byWordWrapping
        label?.textAlignment = .left
        
        return view
    }()
    
    override func build() {
        super.build()
        container.addSubview(urlButton)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            urlButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5.scaled),
            urlButton.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            urlButton.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            urlButton.bottomAnchor.constraint(equalTo: container.bottomAnchor),
        ])
    }
    
    @objc private func openURL() {
        
        guard let url = URL(string: link), UIApplication.shared.canOpenURL(url) else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

class ButtonExposureRow: ExposureRow {
    var buttonTitle = "" {
        didSet {
            button.title = buttonTitle
        }
    }
    
    var buttonAction: (() -> ())? {
        didSet {
            button.action = buttonAction
        }
    }
    
    private lazy var button: Button = {
        let button = Button(theme: theme)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.style = .primary
        
        return button
    }()
    
    override func build() {
        super.build()
        container.addSubview(button)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5.scaled),
            button.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            button.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            button.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            button.heightAnchor.constraint(equalToConstant: 50.scaled)
        ])
    }
}
