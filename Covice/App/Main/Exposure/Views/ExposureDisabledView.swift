//
//  ExposureDisabledView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


protocol ExposureDisabledViewDelegate {
    func didPressEnableExposureDetection(_ exposuresDisabledView: ExposuresDisabledView)
    func didPressEnableBluetooth(_ exposuresDisabledView: ExposuresDisabledView)
}


class ExposuresDisabledView: View {
    lazy var imageView: UIImageView = {
        let view = UIImageView(image: UIImage.exposureOff)
        view.contentMode = .scaleAspectFit
        view.layer.shadowColor = theme.colors.darkOrange.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .init(width: 0, height: 10.scaled)
        view.layer.shadowRadius = 10.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var titleView: UILabel = {
        var titleText: String {
            switch error {
            case .bluetoothOff: return Localization.exposure_notifications_are_inactive
            case .disabled: return Localization.exposure_notifications_are_turned_off
            default: return ""
            }
        }
        
        let view = UILabel()
        view.text = titleText
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 18.scaled, weight: .bold)
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var bodyView: UILabel = {
        var bodyText: String {
            switch error {
            case .bluetoothOff: return Localization.ble_off_warning
            default: return Localization.notify_turn_on_exposure_notifications_header
            }
        }
        
        let view = UILabel()
        view.text = bodyText
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 18.scaled, weight: .regular)
        view.textAlignment = .center
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var buttonView: Button = {
        var buttonText: String {
            switch error {
            case .bluetoothOff: return Localization.device_settings
            default: return Localization.btn_turn_on
            }
        }
        
        var buttonAction = { [weak self] in
            guard let this = self else { return }
            switch this.error {
            case .bluetoothOff: this.delegate?.didPressEnableBluetooth(this)
            default: this.delegate?.didPressEnableExposureDetection(this)    
            }
        }
        
        let button = Button(title: buttonText, theme: theme)
        button.style = .primary
        button.action = buttonAction
        
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
        
    var delegate: ExposureDisabledViewDelegate?
    private var error: ExposureManagerError
    
    required init(theme: Theme, error: ExposureManagerError) {
        self.error = error
        super.init(theme: theme)
    }
    
    override func build() {
        super.build()
        
        addSubview(imageView)
        addSubview(titleView)
        addSubview(bodyView)
        addSubview(buttonView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.scaled),
            imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4),
            imageView.widthAnchor.constraint(equalTo: self.heightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            titleView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            titleView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            titleView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 40.scaled)
        ])
        
        NSLayoutConstraint.activate([
            bodyView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            bodyView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            bodyView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 20.scaled),
        ])
        
        NSLayoutConstraint.activate([
            buttonView.topAnchor.constraint(greaterThanOrEqualTo: bodyView.bottomAnchor, constant: 30.scaled),
            buttonView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            buttonView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -40.scaled),
        ])
    }
}
