//
//  NoExposureView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class NoExposureView: View {
    lazy var imageView: UIImageView = {
        let view = UIImageView(image: UIImage.noExposures)
        view.contentMode = .scaleAspectFit
        view.layer.shadowColor = theme.colors.darkOrange.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .init(width: 0, height: 10)
        view.layer.shadowRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var titleView: UILabel = {
        let view = UILabel()
        view.text = Localization.no_recent_exposure_subtitle
        view.numberOfLines = 0
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 18.scaled, weight: .bold)
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var bodyView: UILabel = {
        let view = UILabel()
        view.text = Localization.notifications_enabled_info
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 18.scaled, weight: .regular)
        view.textAlignment = .center
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func build() {
        super.build()
        
        addSubview(imageView)
        addSubview(titleView)
        addSubview(bodyView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.scaled),
            imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            imageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.4),
            imageView.widthAnchor.constraint(equalTo: self.heightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            titleView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            titleView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            titleView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 40.scaled)
        ])
        
        NSLayoutConstraint.activate([
            bodyView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            bodyView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            bodyView.topAnchor.constraint(equalTo: titleView.bottomAnchor, constant: 30.scaled),
        ])
    }
}
