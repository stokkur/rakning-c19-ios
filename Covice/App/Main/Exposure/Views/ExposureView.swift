//
//  ExposureView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class ExposureView: View {
    private lazy var recentExposuresLabel: UILabel = {
        let view = UILabel()
        view.text = Localization.recent_exposures_subtitle
        view.font = .systemFont(ofSize: 16.scaled, weight: .bold)
        view.textColor = .darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var gradientView: GradientView = {
        let view  = GradientView(theme: theme)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.colors = [
            theme.colors.lightOrange,
            theme.colors.lightOrange.withAlphaComponent(0),
        ]
        view.locations = [0, 0.7]
        
        return view
    }()
    
    lazy var tableView = ExposureTableView()
    
    override func build() {
        super.build()
        
        addSubview(recentExposuresLabel)
        addSubview(tableView)
        addSubview(gradientView)
    }

    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            recentExposuresLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 30.scaled),
            recentExposuresLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            recentExposuresLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled)
        ])
        
        NSLayoutConstraint.activate([
            gradientView.topAnchor.constraint(equalTo: recentExposuresLabel.bottomAnchor),
            gradientView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            gradientView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            gradientView.heightAnchor.constraint(equalToConstant: 30.scaled)
        ])
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: recentExposuresLabel.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
}
