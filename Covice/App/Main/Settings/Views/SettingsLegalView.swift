//
//  SettingsMinistryView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol SettingsLegalViewDelegate {
    func didPressReturn()
}

class SettingsLegalView: View {
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.text = Localization.settings_country_name
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 16.scaled, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .center
        return view
    }()
    
    lazy var subTitle: UILabel = {
        let view = UILabel()
        view.text = Localization.settings_directorate_of_health
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 16.scaled, weight: .semibold)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .center
        return view
    }()
    
    lazy var logo: UIImageView = {
        let view = UIImageView(image: type.icon)
        view.contentMode = .scaleAspectFit
        view.layer.shadowColor = theme.colors.darkOrange.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .init(width: 0, height: 10.scaled)
        view.layer.shadowRadius = 10.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var body: UILabel = {
        let view = UILabel()
        view.text = type.body
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 16.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .natural
        
        return view
    }()
    
    lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = type != .ministry
        return view
    }()
    
    lazy var learnMoreAt: UILabel = {
        let view = UILabel()
        view.text = Localization.settings_learn_more
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .natural
        view.isHidden = type != .ministry
        return view
    }()
    
    lazy var learnMore: LinkLabel = {
        let view = LinkLabel(theme: theme)
        view.text = type.learnMoreLink
        view.action = { [weak self] in
            guard let self = self else {
                return
            }
            let languageSelected = Bundle.SupportedLanguage.mapLanguageCode(Bundle.getSelectedAppLangCode())
            var langLink = ""
            switch Bundle.SupportedLanguage.mapLanguageCode(Bundle.getSelectedAppLangCode()) {
            case .english:
                langLink = "english"
            case .polish:
                langLink = "polski"
            default:
                break
            }
            if let url = URL(string: self.type.learnMoreLink + langLink) {
                UIApplication.shared.open(url)
            }
        }
        view.font = .systemFont(ofSize: 15.scaled, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .natural
        view.isUserInteractionEnabled = true
        view.isHidden = type != .ministry
        return view
    }()
    
    let type: SettingsLegalController.Legal
    
    var delegate: SettingsLegalViewDelegate?
    
    init(theme: Theme, type: SettingsLegalController.Legal) {
        self.type = type
        super.init(theme: theme)
    }
    
    
    override func build() {
        super.build()
        addSubview(logo)
        addSubview(title)
        addSubview(subTitle)
        addSubview(body)
        addSubview(separatorView)
        addSubview(learnMoreAt)
        addSubview(learnMore)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            logo.topAnchor.constraint(equalTo: self.topAnchor, constant: 20.scaled),
            logo.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -21.scaled),
            logo.widthAnchor.constraint(equalToConstant: 50.scaled),
            logo.heightAnchor.constraint(equalTo: logo.widthAnchor)
        ])
        
        NSLayoutConstraint.activate([
            title.bottomAnchor.constraint(equalTo: logo.centerYAnchor),
            title.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40.scaled)
        ])
        
        NSLayoutConstraint.activate([
            subTitle.topAnchor.constraint(equalTo: logo.centerYAnchor),
            subTitle.leadingAnchor.constraint(equalTo: title.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            body.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 20.scaled),
            body.leadingAnchor.constraint(equalTo: title.leadingAnchor),
            body.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40.scaled)
        ])
        
        NSLayoutConstraint.activate([
            separatorView.topAnchor.constraint(equalTo: body.bottomAnchor, constant: 25.scaled),
            separatorView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 25.scaled),
            separatorView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -25.scaled),
            separatorView.heightAnchor.constraint(equalToConstant: 1.scaled)
        ])
        
        NSLayoutConstraint.activate([
            learnMoreAt.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 25.scaled),
            learnMoreAt.leadingAnchor.constraint(equalTo: body.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            learnMore.topAnchor.constraint(equalTo: learnMoreAt.bottomAnchor),
            learnMore.leadingAnchor.constraint(equalTo: learnMoreAt.leadingAnchor),
            learnMore.trailingAnchor.constraint(equalTo: body.trailingAnchor)
        ])
    }
}
