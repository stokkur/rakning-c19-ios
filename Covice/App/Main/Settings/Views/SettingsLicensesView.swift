//
//  SettingsLicensesView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol SettingsLicensesViewDelegate {
    func didPressReturn()
}

class SettingsLicensesView: View {
    lazy var appLogo: UIImageView = {
        let view = UIImageView(image: UIImage.appLogo)
        view.contentMode = .scaleAspectFit
        view.layer.shadowColor = theme.colors.darkOrange.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .init(width: 0, height: 10.scaled)
        view.layer.shadowRadius = 10.scaled
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var appVersion: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.font = .systemFont(ofSize: 9.scaled, weight: .bold)
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        var conf: String = ""
        #if DEBUG
        conf = "debug"
        #else
        conf = "release"
        #endif
        view.text = String(format: Localization.settings_licenses_app_version, version!, build!, conf)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var tableView = LicensesTableView()
    
    var delegate: SettingsLicensesViewDelegate?
    
    override func build() {
        super.build()
        
        addSubview(appLogo)
        addSubview(appVersion)
        addSubview(tableView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            appLogo.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            appLogo.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 20.scaled),
            appLogo.heightAnchor.constraint(equalToConstant: 100.scaled),
            appLogo.widthAnchor.constraint(equalTo: appLogo.heightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            appVersion.centerXAnchor.constraint(equalTo: appLogo.centerXAnchor),
            appVersion.topAnchor.constraint(equalTo: appLogo.bottomAnchor, constant: 15.scaled)
        ])
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: appVersion.bottomAnchor, constant: 15.scaled),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
}
