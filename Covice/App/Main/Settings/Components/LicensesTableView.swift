//
//  LicensesTableView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit


class LicensesTableView: UITableView {
    init() {
        super.init(frame: .zero, style: .plain)
        translatesAutoresizingMaskIntoConstraints = false

        separatorStyle = .none
        backgroundColor = .clear

        isScrollEnabled = true

        estimatedRowHeight = 100.scaled
        rowHeight = UITableView.automaticDimension

        allowsSelection = false
        
        tableFooterView = UIView()
        tableHeaderView = UIView()
        
        register(LicenseCell.self)
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class LicenseCell: UITableViewCell {
    
    lazy var licenseStack: UIStackView = {
        let view = UIStackView()
        let background = UIView(frame: view.bounds)
        background.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        background.backgroundColor = .white
        background.layer.borderColor = theme.colors.gray.cgColor
        background.layer.borderWidth = 1
        background.layer.cornerRadius = 10.scaled
        view.addSubview(background)
        
        view.axis = .vertical
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 17.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var autor: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var separator1: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var content: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var descriptionLbl: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14.scaled, weight: .regular)
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var separator2: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var footerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var version: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var licenseType: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let theme: Theme
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: LicenseCell.reuseIdentifier)
        backgroundColor = .clear
        contentView.isUserInteractionEnabled = false
        build()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        addSubview(licenseStack)
        
        headerView.addSubview(title)
        headerView.addSubview(autor)
        
        content.addSubview(descriptionLbl)
        
        footerView.addSubview(version)
        footerView.addSubview(licenseType)
        
        licenseStack.addArrangedSubview(headerView)
        licenseStack.addArrangedSubview(separator1)
        licenseStack.addArrangedSubview(content)
        licenseStack.addArrangedSubview(separator2)
        licenseStack.addArrangedSubview(footerView)
    }

    func setupConstraints() {
        
        NSLayoutConstraint.activate([
            licenseStack.topAnchor.constraint(equalTo: self.topAnchor, constant: 10.scaled),
            licenseStack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10.scaled),
            licenseStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10.scaled),
            licenseStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10.scaled)
        ])
        
        NSLayoutConstraint.activate([
            headerView.leadingAnchor.constraint(equalTo: licenseStack.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: licenseStack.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            title.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 5.scaled),
            title.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 5.scaled),
            title.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: -5.scaled)
        ])
        
        NSLayoutConstraint.activate([
            autor.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -5.scaled),
            autor.bottomAnchor.constraint(equalTo: title.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            separator1.leadingAnchor.constraint(equalTo: licenseStack.leadingAnchor),
            separator1.trailingAnchor.constraint(equalTo: licenseStack.trailingAnchor),
            separator1.heightAnchor.constraint(equalToConstant: 1.scaled)
        ])
        
        NSLayoutConstraint.activate([
            content.leadingAnchor.constraint(equalTo: licenseStack.leadingAnchor),
            content.trailingAnchor.constraint(equalTo: licenseStack.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLbl.topAnchor.constraint(equalTo: content.topAnchor, constant: 5.scaled),
            descriptionLbl.leadingAnchor.constraint(equalTo: content.leadingAnchor, constant: 5.scaled),
            descriptionLbl.trailingAnchor.constraint(equalTo: content.trailingAnchor, constant: -5.scaled),
            descriptionLbl.bottomAnchor.constraint(equalTo: content.bottomAnchor, constant: -5.scaled)
        ])
        
        NSLayoutConstraint.activate([
            separator2.leadingAnchor.constraint(equalTo: licenseStack.leadingAnchor),
            separator2.trailingAnchor.constraint(equalTo: licenseStack.trailingAnchor),
            separator2.heightAnchor.constraint(equalToConstant: 1.scaled)
        ])
        
        NSLayoutConstraint.activate([
            footerView.leadingAnchor.constraint(equalTo: licenseStack.leadingAnchor),
            footerView.trailingAnchor.constraint(equalTo: licenseStack.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            version.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 5.scaled),
            version.leadingAnchor.constraint(equalTo: footerView.leadingAnchor, constant: 5.scaled),
            version.bottomAnchor.constraint(equalTo: footerView.bottomAnchor, constant: -5.scaled)
        ])
        
        NSLayoutConstraint.activate([
            licenseType.centerYAnchor.constraint(equalTo: version.centerYAnchor),
            licenseType.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -5.scaled)
        ])
    }
    
    func configure(title: String, autor: String, description: String, version: String, licenseType: String) {
        self.title.text = title
        self.autor.text = autor
        self.descriptionLbl.text = description
        self.version.text = version
        self.licenseType.text = licenseType
    }
}
