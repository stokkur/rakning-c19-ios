//
//  SettingsTableView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol CellConfigurator {
    var icon: UIImage? { get }
    var title: String? { get }
    var body: String? { get }
    var subIcon: UIImage? { get }
}

protocol RelocalizableCell {
    func relocalizeContent()
}

class SettingsTableView: UITableView {
    init() {
        super.init(frame: .zero, style: .plain)
        translatesAutoresizingMaskIntoConstraints = false

        separatorStyle = .none
        backgroundColor = .clear

        isScrollEnabled = false

        estimatedRowHeight = 100
        rowHeight = UITableView.automaticDimension

        allowsSelection = true
        
        tableFooterView = UIView()
        tableHeaderView = UIView()
        
        register(SettingsCell.self)
        register(SettingsLanguageCell.self)
        register(SettingsShareCell.self)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func relocalizeContent(){
        self.visibleCells.forEach { (cell) in
            DispatchQueue.main.async {
                guard let relocalizableCell = cell as? RelocalizableCell else {
                    return
                }
                relocalizableCell.relocalizeContent()
            }
        }
    }
}

class SettingsCell: UITableViewCell {
    private let theme: Theme
    
    lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var contentStack: UIStackView = {
        let view = UIStackView()
        view.backgroundColor = .clear
        view.axis = .vertical
        view.translatesAutoresizingMaskIntoConstraints = false
        view.distribution = .fillProportionally
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        
        return view
    }()
    
    lazy var status: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.gray
        view.font = .systemFont(ofSize: 13.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    lazy var subIcon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    var config: CellConfigurator?
    var statusGetter: (()->String?)?
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: SettingsCell.reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        build()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        addSubview(icon)
        addSubview(contentStack)
        contentStack.addArrangedSubview(title)
        contentStack.addArrangedSubview(status)
        addSubview(subIcon)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            icon.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            icon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 21.scaled),
            icon.heightAnchor.constraint(equalToConstant: 24.scaled),
            icon.widthAnchor.constraint(equalToConstant: 24.scaled),
        ])
        
        NSLayoutConstraint.activate([
            contentStack.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor, constant: 15.scaled),
            contentStack.bottomAnchor.constraint(greaterThanOrEqualTo: self.bottomAnchor, constant: -15.scaled),
            contentStack.centerYAnchor.constraint(equalTo: icon.centerYAnchor),
            contentStack.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 21.scaled),
            contentStack.trailingAnchor.constraint(equalTo: subIcon.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            subIcon.centerYAnchor.constraint(equalTo: icon.centerYAnchor),
            subIcon.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22.scaled),
            subIcon.heightAnchor.constraint(equalToConstant: 24.scaled),
            subIcon.widthAnchor.constraint(equalToConstant: 24.scaled),
        ])
    }
    
    func configure(config: CellConfigurator, statusGetter: (()->String?)? = nil ) {
        self.statusGetter = statusGetter
        self.config = config
        if statusGetter != nil {
            self.status.isHidden = false
        }
        if config.subIcon != nil {
            self.subIcon.isHidden = false
        }
        
        self.icon.image = config.icon
        self.title.text = config.title
        self.status.text = statusGetter?()
        self.subIcon.image = config.subIcon
    }
}

extension SettingsCell: RelocalizableCell {
    func relocalizeContent() {
        self.icon.image = self.config?.icon
        self.title.text = self.config?.title
        self.status.text = self.statusGetter?()
        self.subIcon.image = self.config?.subIcon
    }
}

class SettingsLanguageCell: UITableViewCell {
    private let theme: Theme
    
    lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var contentStack: UIStackView = {
        let view = UIStackView()
        view.backgroundColor = .clear
        view.axis = .vertical
        view.translatesAutoresizingMaskIntoConstraints = false
        view.distribution = .fillProportionally
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var selectedLanguage: PickerLabel = {
        let view = PickerLabel(theme: theme, displayDone: true)
        view.textColor = theme.colors.gray
        view.font = .systemFont(ofSize: 13.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    lazy var subIcon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isHidden = true
        return view
    }()
    
    var config: CellConfigurator?
    var statusGetter: (()->String?)?
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: SettingsCell.reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        build()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        addSubview(icon)
        addSubview(contentStack)
        contentStack.addArrangedSubview(title)
        contentStack.addArrangedSubview(selectedLanguage)
        addSubview(subIcon)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            icon.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            icon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 21.scaled),
            icon.heightAnchor.constraint(equalToConstant: 24.scaled),
            icon.widthAnchor.constraint(equalToConstant: 24.scaled),
        ])
        
        NSLayoutConstraint.activate([
            contentStack.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor, constant: 15.scaled),
            contentStack.bottomAnchor.constraint(greaterThanOrEqualTo: self.bottomAnchor, constant: -15.scaled),
            contentStack.centerYAnchor.constraint(equalTo: icon.centerYAnchor),
            contentStack.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 21.scaled),
            contentStack.trailingAnchor.constraint(equalTo: subIcon.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            subIcon.centerYAnchor.constraint(equalTo: icon.centerYAnchor),
            subIcon.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22.scaled),
            subIcon.heightAnchor.constraint(equalToConstant: 24.scaled),
            subIcon.widthAnchor.constraint(equalToConstant: 24.scaled),
        ])
        
    }
    
    func configure(config: CellConfigurator, statusGetter: (()->String?)? = nil ) {
        self.statusGetter = statusGetter
        self.config = config
        if statusGetter != nil {
            self.selectedLanguage.isHidden = false
        }
        if config.subIcon != nil {
            self.selectedLanguage.isHidden = false
        }
        
        self.icon.image = config.icon
        self.title.text = config.title
        self.selectedLanguage.text = statusGetter?()
        self.subIcon.image = config.subIcon
    }
}

extension SettingsLanguageCell: RelocalizableCell {
    func relocalizeContent() {
        self.icon.image = self.config?.icon
        self.title.text = self.config?.title
        self.selectedLanguage.text = self.statusGetter?()
        self.subIcon.image = self.config?.subIcon
        self.selectedLanguage.reloadView()
    }
}

protocol SettingsShareCellDelegate {
    func didPressShare()
}
class SettingsShareCell: UITableViewCell {
    private let theme: Theme
    
    lazy var icon: UIImageView = {
        let view = UIImageView()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    lazy var body: UILabel = {
        let view = UILabel()
        
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.numberOfLines = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var share: Button = {
        let view = Button(title: Localization.btn_share_this_app, theme: theme)
        view.action = { [weak self] in
            guard let this = self else {
                return
            }
            this.delegate?.didPressShare()
        }
        view.style = .ghost
        view.titleLabel?.font = .systemFont(ofSize: 18.scaled, weight: .semibold)
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var delegate: SettingsShareCellDelegate?
    var config: CellConfigurator?
    
    init(theme: Theme) {
        self.theme = theme
        super.init(style: .default, reuseIdentifier: SettingsShareCell.reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        isUserInteractionEnabled = true
        contentView.isUserInteractionEnabled = true
        build()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func build() {
        addSubview(icon)
        addSubview(body)
        addSubview(share)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: self.topAnchor, constant: 21.scaled),
            icon.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 21.scaled),
            icon.heightAnchor.constraint(equalToConstant: 24.scaled),
            icon.widthAnchor.constraint(equalToConstant: 24.scaled),
        ])
        
        NSLayoutConstraint.activate([
            body.topAnchor.constraint(equalTo: icon.topAnchor, constant: 5.scaled),
            body.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 21.scaled),
            body.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -21.scaled)
        ])
        
        NSLayoutConstraint.activate([
            share.leadingAnchor.constraint(equalTo: body.leadingAnchor),
            share.topAnchor.constraint(equalTo: body.bottomAnchor, constant: 10.scaled),
            share.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -21.scaled)
        ])
    }
    
    func configure(config: CellConfigurator) {
        self.config = config
        self.icon.image = config.icon
        self.body.text = config.body
    }
}

extension SettingsShareCell: RelocalizableCell {
    func relocalizeContent() {
        self.icon.image = self.config?.icon
        self.body.text = self.config?.body
        self.share.title = Localization.btn_share_this_app
    }
}

