//
//  SettingsLicensesController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol SettingsLicensesControllerDelegate {
    func didFinished(_ settingsLlicensesController: SettingsLicensesController)
}

class SettingsLicensesController: ViewController {
    enum Licenses: Int, CaseIterable {
        case swiftLint = 0
        case swiftProtobuf
        case zipFoundation
        
        var title: String {
            switch self {
            case .swiftLint:
                return "SwiftLint"
            case .swiftProtobuf:
                return "Swift Protobuf"
            case .zipFoundation:
                return "ZIP Foundation"
            }
        }
        var autor: String {
            switch self {
            case .swiftLint:
                return "Realm Inc."
            case .swiftProtobuf:
                return "Apple"
            case .zipFoundation:
                return "Thomas Zoechling"
            }
        }
        var description: String {
            switch self {
            case .swiftLint:
                return "A tool to enforce Swift style and conventions, loosely based on GitHub's Swift Style Guide."
            case .swiftProtobuf:
                return "Apple's Swift programming language is a perfect complement to Google's Protocol Buffer (\"protobuf\") serialization technology. They both emphasize high performance and programmer safety."
            case .zipFoundation:
                return "ZIP Foundation is a library to create, read and modify ZIP archive files."
            }
        }
        var version: String {
            switch self {
            case .swiftLint:
                return "0.42.0"
            case .swiftProtobuf:
                return "1.15.0"
            case .zipFoundation:
                return "0.9.11"
            }
        }
        var licenseType: String {
            switch self {
            case .swiftLint:
                return "MIT License"
            case .swiftProtobuf:
                return "Apache-2.0 License"
            case .zipFoundation:
                return "MIT License"
            }
        }
    }
    
    var delegate: SettingsLicensesControllerDelegate?
    
    override func loadView() {
        let view = SettingsLicensesView(theme: theme)
        view.delegate = self
        view.tableView.delegate = self
        view.tableView.dataSource = self
        internalView = view
    }
}

extension SettingsLicensesController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Licenses.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = LicenseCell(theme: theme)
        guard let lib = Licenses(rawValue: indexPath.row) else {
            return UITableViewCell()
        }
        
        cell.configure(title: lib.title, autor: lib.autor, description: lib.description, version: lib.version, licenseType: lib.licenseType)
        return cell
    }
}

extension SettingsLicensesController: SettingsLicensesViewDelegate {
    func didPressReturn() {
        delegate?.didFinished(self)
    }
}
