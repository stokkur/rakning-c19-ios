//
//  SettingsLegalController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol SettingsLegalControllerDelegate {
    
}

class SettingsLegalController: ViewController {
    enum Legal {
        case ministry
        case terms
        
        var icon: UIImage? {
            return UIImage.appLogo
        }
        var pageHeader: String {
            switch self {
            case .ministry: return Localization.settings_directorate_of_health
            case .terms: return Localization.settings_directorate_of_health
            }
        }
        var title: String {
            switch self {
            case .ministry: return Localization.settings_country_name
            case .terms: return Localization.settings_country_name
            }
        }
        var subTitle: String {
            switch self {
            case .ministry: return Localization.settings_directorate_of_health
            case .terms: return Localization.settings_directorate_of_health
            }
        }
        var body: String {
            switch self {
            case .ministry: return Localization.settings_directorate_message
            case .terms: return Localization.settings_legal_terms_message
            }
        }
        var learnMoreLink: String {
            switch self {
            case .ministry: return Localization.settings_learn_more_link
            default: return ""
            }
        }
    }
    
    var type: Legal
    
    var delegate: SettingsControllerDelgate?
    
    init(theme: Theme, type: SettingsController.SettingsSection.LegalRow) {
        switch type {
        case .ministry:
            self.type = .ministry
        default:
            self.type = .terms
        }
        super.init(theme: theme)
    }
    
    override func loadView() {
        self.title = Localization.settings_directorate_of_health
        
        let view = SettingsLegalView(theme: theme, type: type)
        view.delegate = self
        self.internalView = view
    }
}

extension SettingsLegalController: SettingsLegalViewDelegate {
    func didPressReturn() {
        navigationController?.popViewController(animated: true)
    }
}
