//
//  SettingsViewController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol SettingsControllerDelgate {
    func didPressPhoneRegistration(_ settingsController:SettingsController)
    func didPressLegal(_ settingsController: SettingsController, type: SettingsController.SettingsSection.LegalRow)
    func didPressLicenses(_ settingsController: SettingsController)
}

class SettingsController: ViewController {
    
    enum SettingsSection: Int, CaseIterable {

        case app = 0
        case legal
        case dev
        case share
        
        enum AppRow: Int, CaseIterable, CellConfigurator{
            case exposure
            case phone
            case language
            var icon: UIImage? {
                switch self {
                case .exposure: return UIImage.exposureNotification
                case .phone: return UIImage.alertBell
                case .language: return UIImage.language
                }
            }
            var title: String? {
                switch self {
                case .exposure: return Localization.settings_exposure_notifications_subtitle
                case .phone: return Localization.settings_enter_phone_number
                case .language: return Localization.language
                }
            }
            var body: String? {
                return nil
            }
            var subIcon: UIImage? {
                return nil
            }
        }
        enum LegalRow: Int, CaseIterable, CellConfigurator{
            case ministry
            // case terms
            case privacy
            var icon: UIImage? {
                switch self {
                case .ministry: return UIImage.exclamationCircle
                // case .terms: return UIImage.docPlain
                case .privacy: return UIImage.checkShield
                }
            }
            var title: String? {
                switch self {
                case .ministry: return Localization.settings_directorate_of_health
                // case .terms: return Localization.settings_legal_terms
                case .privacy: return Localization.settings_privacy_policy
                }
            }
            var body: String? {
                return nil
            }
            var subIcon: UIImage? {
                switch self {
                case .privacy: return UIImage.squareArrowUp
                default: return nil
                }
            }
        }
        enum DevRow: Int, CaseIterable, CellConfigurator{
            
            case licenses
            var icon: UIImage? {
                switch self {
                case .licenses: return UIImage.brackets
                }
            }
            var title: String? {
                switch self {
                case .licenses: return Localization.settings_open_source
                }
            }
            var body: String? {
                return nil
            }
            var subIcon: UIImage? {
                return nil
            }
        }
        enum ShareRow: Int, CaseIterable, CellConfigurator{
            case app
            var icon: UIImage? {
                switch self {
                case .app: return UIImage.share
                }
            }
            var title: String? {
                return nil
            }
            var body: String? {
                switch self {
                case .app: return Localization.settings_share
                }
            }
            var subIcon: UIImage? {
                return nil
            }
        }
        var rowsInSection: Int {
            switch self {
            case .app: return AppRow.allCases.count
            case .legal: return LegalRow.allCases.count
            case .dev: return DevRow.allCases.count
            case .share: return ShareRow.allCases.count
            }
        }   
    }
    
    var delegate: SettingsControllerDelgate?
    var networkManager: NetworkManager
    var exposureManager: ExposureManager
    var localStorageManager: LocalStorageManager
    var expNotEnabled: Bool = false
    var languageLabel: PickerLabel?
    
    init(theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, localStorageManager: LocalStorageManager) {
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.localStorageManager = localStorageManager
        super.init(theme: theme)
    }
    
    override func loadView() {
        self.title = Localization.home_tab_settings_text
        
        let view = SettingsView(theme: theme)
        view.tableView.delegate = self
        view.tableView.dataSource = self
        self.internalView = view
    }
    
    
    private func getExposureStatus() -> String? {
        switch exposureManager.getExposureNotificationStatus() {
        case .active:
            expNotEnabled = true
            return Localization.settings_exposure_notifications_on
        default:
            return Localization.settings_exposure_notifications_off
        }
    }
    
    private func getPhoneStatus() -> String? {
        return localStorageManager.registeredPhoneNumber.wrappedValue ?? Localization.settings_phone_off
    }
    
    private func didPressPhoneRegistration() {
        delegate?.didPressPhoneRegistration(self)
    }
    
    private func didPressLenguageSelection(){
        self.languageLabel?.becomeFirstResponder()
    }
    
    private func didPressLegal(_ type :SettingsSection.LegalRow) {
        delegate?.didPressLegal(self, type: type)
    }
    
    private func shareThisApp(){
        let text = "\(Localization.settings_share_message) https://apps.apple.com/\(Bundle.getSelectedAppLangCode())/app/rakning-c-19/id1504655876"
        let activityViewController = UIActivityViewController(activityItems: [text] , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    private func didPressLegalTerms() {
        // TODO: Check if this link can be localized. Couldn't find the specific legal terms localized.
        guard let url = URL(string: "https://www.covid.is/app/personuverndarstefna") else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func didPressLicenses(){
        self.delegate?.didPressLicenses(self)
    }
    
    
    private func turnOnExposureNotification(){
        let title = expNotEnabled ? Localization.exposure_turn_off_title : Localization.turn_on_exposure_notification_title
        let detail = expNotEnabled ? Localization.exposure_turn_off_detail : Localization.turn_on_exposure_notification_detail
        
        let alert = CustomAlertController(theme: theme, title: title, content: detail)
        
        let cancel = Button(title: Localization.btn_cancel, theme: theme)
        cancel.style = .ghost
        alert.addAction(button: cancel)
        
        if expNotEnabled {
            let turnOff = Button(title: Localization.btn_turn_off, theme: theme)
            turnOff.action = {
                self.exposureManager.setExposureNotificationEnabled(false) { [weak self] error in
                    NotificationCenter.default.post(name: ExposureManager.authorizationStatusChangeNotification, object: nil)
                    guard let this = self else {
                        return
                    }
                    if let error = error {
                        switch error {
                        case .authorizationDenied:
                            fallthrough
                        case .notAuthorized:
                            let notAuthorizedAlert = CustomAlertController(theme: this.theme, title: Localization.not_autorized_error_title, content: Localization.not_autorized_error_content)
                            let cancel = Button(title: Localization.btn_done, theme: this.theme)
                            cancel.style = .ghost
                            notAuthorizedAlert.addAction(button: cancel)
                            this.present(notAuthorizedAlert, animated: true)
                            break
                        default:
                            this.showError(error)
                        }
                    }
                    this.expNotEnabled = false
                    (this.internalView as! SettingsView).tableView.reloadData()
                }
            }
            alert.addAction(button: turnOff)
        } else {
            let turnOn = Button(title: Localization.btn_turn_on, theme: theme)
            turnOn.action = {
                self.exposureManager.setExposureNotificationEnabled(true) { [weak self] error in
                    NotificationCenter.default.post(name: ExposureManager.authorizationStatusChangeNotification, object: nil)
                    guard let this = self else {
                        return
                    }
                    if let error = error {
                        switch error {
                        case .authorizationDenied:
                            fallthrough
                        case .notAuthorized:
                            let notAuthorizedAlert = CustomAlertController(theme: this.theme, title: Localization.not_autorized_error_title, content: Localization.not_autorized_error_content)
                            let cancel = Button(title: Localization.btn_done, theme: this.theme)
                            cancel.style = .ghost
                            notAuthorizedAlert.addAction(button: cancel)
                            this.present(notAuthorizedAlert, animated: true)
                            break
                        default:
                            this.showError(error)
                        }
                    } else {
                        this.expNotEnabled = true
                        (this.internalView as! SettingsView).tableView.reloadData()
                    }
                }
            }
            alert.addAction(button: turnOn)
        }
        alert.delegate = self
        present(alert, animated: true)
    }
    
    private func getExposureNotificationStatus() -> ExposureManagerStatus {
         return exposureManager.getExposureNotificationStatus()
    }
    
    private func languageChanged(){
        guard let view = self.internalView as? SettingsView else {
            // TODO: - Log Error ?
            return
        }
        view.relocalizeView()
        DispatchQueue.main.async { [weak self] in
            guard let this = self else { return }
            this.title = Localization.home_tab_settings_text
        }
    }
    override func relocalizeContent() {
        guard let view = self.internalView as? SettingsView else {
            // TODO: - Log Error ?
            return
        }
        view.relocalizeView()
    }
    
}

extension SettingsController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return SettingsSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SettingsSection(rawValue: section)?.rowsInSection ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SettingsCell(theme: theme)
        
        switch SettingsSection(rawValue: indexPath.section) {
        case .app:
            guard let row = SettingsSection.AppRow(rawValue: indexPath.row) else {
                return UITableViewCell()
            }
            if row == .language {
                let languageCell = SettingsLanguageCell(theme: theme)
                languageCell.configure(config: row) { () -> String? in
                    return Bundle.SupportedLanguage.mapLanguageCode(Bundle.getSelectedAppLangCode()).asText
                }
                self.languageLabel = languageCell.selectedLanguage
                languageCell.selectedLanguage.pickerDelegate = self
                languageCell.selectedLanguage.pickerDataSource = self
                languageCell.selectedLanguage.selectRow(
                    Bundle.SupportedLanguage.mapLanguageCode(Bundle.getSelectedAppLangCode()).rawValue
                )
                return languageCell
            } else {
                cell.configure(config: row) { [weak self] () -> String? in
                    guard let this = self else {
                        return nil
                    }
                    switch row {
                    case .exposure:
                        return this.getExposureStatus()
                    case .phone:
                        return this.getPhoneStatus()
                    default:
                        return nil
                    }
                }
            }
            break
        case .legal:
            guard let row = SettingsSection.LegalRow(rawValue: indexPath.row) else {
                return UITableViewCell()
            }
            cell.configure(config: row)
            break
        case .dev:
            guard let row = SettingsSection.DevRow(rawValue: indexPath.row) else {
                return UITableViewCell()
            }
            cell.configure(config: row)
            break
        case .share:
            guard let row = SettingsSection.ShareRow(rawValue: indexPath.row) else {
                return UITableViewCell()
            }
            let shareCell = SettingsShareCell(theme: theme)
            shareCell.delegate = self
            shareCell.configure(config: row)
            return shareCell
        case .none:
            return UITableViewCell()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch SettingsSection(rawValue: section) {
        case .share:
            return 0
        default:
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = theme.colors.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch SettingsSection(rawValue: indexPath.section) {
        case .app:
            guard let row = SettingsSection.AppRow(rawValue: indexPath.row) else {
                return
            }
            switch row {
            case .exposure:
                turnOnExposureNotification()
                break
            case .phone:
                didPressPhoneRegistration()
                break
            case .language:
                didPressLenguageSelection()
                break
            }
            break
        case .legal:
            guard let row = SettingsSection.LegalRow(rawValue: indexPath.row) else {
                return
            }
            switch row {
            case .ministry:
                didPressLegal(row)
                break
            case .privacy:
                didPressLegalTerms()
                break
            }
            break
        case .dev:
            guard let row = SettingsSection.DevRow(rawValue: indexPath.row) else {
                return
            }
            switch row {
            case .licenses:
                didPressLicenses()
                break
            }
        default:
            break
        }
    }
    
}

extension SettingsController: SettingsShareCellDelegate{
    func didPressShare() {
        shareThisApp()
    }
}

extension SettingsController: CustomAlertControllerDelegate {
    func didDismissView(_ customAlertController: CustomAlertController) {
        customAlertController.dismiss(animated: true)
    }
}

extension SettingsController: PickerDelegate, PickerDataSource {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Bundle.SupportedLanguage.init(rawValue: row)?.asText
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let selectedLang = Bundle.SupportedLanguage(rawValue: row) else {
            // Do not change language ?
            return
        }
        Bundle.setLanguage(lang: selectedLang.code)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
            self.languageChanged()
            NotificationCenter.default.post(name: Bundle.languageChangedNotificatioName, object: nil)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Bundle.SupportedLanguage.allCases.count
    }
    
    
}
