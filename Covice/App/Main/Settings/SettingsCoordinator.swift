//
//  SettingsCoordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

class SettingsCoordinator: Coordinator {
    required init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.navigationController = NavigationController(theme: theme)
        self.navigationController.setNavigationBarHidden(false, animated: false)
        self.theme = theme
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.storageManager = storageManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
    }
    
    var coreDataManager: CoreDataManager
    
    var networkManager: NetworkManager
    
    var exposureManager: ExposureManager
    
    var storageManager: StorageManager
    
    var localStorageManager: LocalStorageManager
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    var theme: Theme
    
    var mainController: SettingsController?
    
    @discardableResult func start() -> UINavigationController {
        let settingsController = SettingsController(theme: theme, networkManager: networkManager, exposureManager: exposureManager, localStorageManager: localStorageManager)
        settingsController.delegate = self
        navigationController.pushViewController(settingsController, animated: false)
        mainController = settingsController
        return self.navigationController
    }
    
}

extension SettingsCoordinator: SettingsControllerDelgate {
    
    func didPressLicenses(_ settingsController: SettingsController) {
        let controller = SettingsLicensesController(theme: theme)
        controller.delegate = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func didPressLegal(_ settingsController: SettingsController, type: SettingsController.SettingsSection.LegalRow) {
        let controller = SettingsLegalController(theme: theme, type: type)
        controller.delegate = self
        navigationController.pushViewController(controller, animated: true)
    }
    
    func didPressPhoneRegistration(_ settingsController: SettingsController) {
        phoneRegistrationFlow()
    }
}

// MARK: - Phone Registration Flow
extension SettingsCoordinator: PhoneRegistrationCoordinatorDelegate {
    func phoneRegistrationFlow() {
        let registrationCoordinator = PhoneRegistrationCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        registrationCoordinator.delegate = self
        registrationCoordinator.asModalFlow = true
        
        registrationCoordinator.start()
        addChildCoordinator(registrationCoordinator)
    }
    
    func didFinish(_ onboardingPhoneRegistrationCoordinator: PhoneRegistrationCoordinator) {
        removeChildCoordinator(onboardingPhoneRegistrationCoordinator)
        mainController?.relocalizeContent()
    }
    
    func didClose(_ onboardingPhoneRegistrationCoordinator: PhoneRegistrationCoordinator) {
        removeChildCoordinator(onboardingPhoneRegistrationCoordinator)
    }
}

extension SettingsCoordinator: SettingsLicensesControllerDelegate{
    func didFinished(_ settingsLlicensesController: SettingsLicensesController) {
        navigationController.popViewController(animated: true)
    }
}
