//
//  MainCoordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol MainCoordinatorDelegate {
    func didFinish(_ mainCoordinator: MainCoordinator)
}

class MainCoordinator: Coordinator {
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    required init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.navigationController = navigationController
        self.theme = theme
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.storageManager = storageManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
    }
    
    var coreDataManager: CoreDataManager
    
    var networkManager: NetworkManager
    
    var exposureManager: ExposureManager
    
    var storageManager: StorageManager
    
    var localStorageManager: LocalStorageManager
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    private let notificationCenter = NotificationCenter.default
    
    var delegate: MainCoordinatorDelegate?
    
    lazy var tabBarController: UITabBarController = {
        let tabController = UITabBarController()
        tabController.tabBar.backgroundColor = theme.colors.white
        tabController.tabBar.tintColor = theme.colors.darkBlue
        tabController.tabBar.isTranslucent = false
        
        var controllers = [UIViewController]()
        
        let exposureCoordinator = ExposureCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        let exposureRootController = exposureCoordinator.start()
        let exposureTabBarItem = TabBarItem(title: Localization.home_tab_exposures_text, image: UIImage.exposureTabInactive, selectedImage: UIImage.exposureTabActive)
        exposureTabBarItem.relocalizeTitleFunc = { return Localization.home_tab_exposures_text }
        exposureRootController.tabBarItem = exposureTabBarItem
        controllers.append(exposureRootController)
        
        let notifyCoordinator = NotifyCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        let notifyRootController = notifyCoordinator.start()
        let notifyTabBarItem = TabBarItem(title: Localization.home_tab_notify_text, image: UIImage.notifyTabInactive, selectedImage: UIImage.notifyTabActive)
        notifyTabBarItem.relocalizeTitleFunc = { return Localization.home_tab_notify_text }
        notifyRootController.tabBarItem = notifyTabBarItem
        controllers.append(notifyRootController)
        
        let settingsCoordinator = SettingsCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        let settingsRootController = settingsCoordinator.start()
        let settingsTabBarItem = TabBarItem(title: Localization.home_tab_settings_text, image: UIImage.settingsTabInactive, selectedImage: UIImage.settingsTabActive)
        settingsTabBarItem.relocalizeTitleFunc = { return Localization.home_tab_settings_text }
        settingsRootController.tabBarItem = settingsTabBarItem
        controllers.append(settingsRootController)
        
        #if !PROD
        let developerCoordinator = DeveloperCoordinator(navigationController: navigationController, theme: theme, networkManager: networkManager, exposureManager: exposureManager, storageManager: storageManager, localStorageManager: localStorageManager, coreDataManager: coreDataManager)
        developerCoordinator.delegate = self
        let developerRootController = developerCoordinator.start()
        developerRootController.tabBarItem = UITabBarItem(title: "Developer", image: UIImage.settingsTabInactive, selectedImage: UIImage.settingsTabActive)
        controllers.append(developerRootController)
        #endif

        tabController.viewControllers = controllers
        
        return tabController
    }()
    
    var theme: Theme
    
    @discardableResult func start() -> UINavigationController {
        notificationCenter.addObserver(self,
                                       selector: #selector(presentNegativeResult),
                                       name: AppDelegate.negativeTestNotificationName,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(presentExposureDetected),
                                       name: AppDelegate.exposureDetectedNotificationName,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(presentBluetoothOffAlert),
                                       name: AppDelegate.bluetoothOffAlertNotificationName,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(selectExposuresTab),
                                       name: AppDelegate.bluetoothOffNotificationName,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(languageChanged),
                                       name: Bundle.languageChangedNotificatioName,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(presentPhoneReRegistration),
                                       name: AppDelegate.phoneReRegistrationNotificationName,
                                       object: nil)
        navigationController.pushViewController(tabBarController, animated: false)
        
        if localStorageManager.showNegativeTestResult.wrappedValue {
            localStorageManager.showNegativeTestResult.wrappedValue = false
            presentNegativeResult()
        }
        
        return self.navigationController
    }
    
    @objc func selectExposuresTab(){
        tabBarController.selectedIndex = 0
    }
    @objc func languageChanged() {
        tabBarController.viewControllers?.forEach({ (uiVC) in
            guard let navCtrl = uiVC as? UINavigationController,
                  let tabBarItem = navCtrl.tabBarItem as? TabBarItem else {
                return
            }
            DispatchQueue.main.async {
                tabBarItem.relocalizeTitle()
            }
        })
    }
    @objc func presentPhoneReRegistration(){
        let customAlert = CustomAlertController(theme: theme, title: Localization.phone_re_registration_title, content: Localization.phone_re_registration_body)
        customAlert.addAction(button: Button(title: Localization.btn_got_it, theme: theme))
        customAlert.delegate = self
        navigationController.present(customAlert, animated: true)
    }
}

// MARK: - DeveloperCoordinatorDelegate
extension MainCoordinator: DeveloperCoordinatorDelegate {
    func didFinish(_ developerCoordinator: DeveloperCoordinator) {
        delegate?.didFinish(self)
    }
}

extension MainCoordinator: CustomAlertControllerDelegate{
    func didDismissView(_ customAlertController: CustomAlertController) {
        customAlertController.dismiss(animated: true)
    }
    
    @objc func presentNegativeResult(){
        let customAlert = CustomAlertController(theme: theme, title: Localization.border_test_results, content: Localization.test_results_negative)
        customAlert.addAction(button: Button(title: Localization.btn_got_it, theme: theme))
        customAlert.delegate = self
        navigationController.present(customAlert, animated: true)
    }
    
    @objc func presentExposureDetected(){
        let customAlert = CustomAlertController(theme: theme, title: Localization.alert_exposure_title, content: Localization.alert_exposure_body)
        customAlert.addAction(button: Button(title: Localization.btn_continue, theme: theme))
        customAlert.delegate = self
        navigationController.present(customAlert, animated: true)
    }
    
    @objc func presentBluetoothOffAlert(){
        let customAlert = CustomAlertController(theme: theme, title: Localization.alert_bluetooth_off_title, content: Localization.alert_bluetooth_off_body)
        customAlert.addAction(button: Button(title: Localization.btn_continue, theme: theme))
        customAlert.delegate = self
        navigationController.present(customAlert, animated: true)
    }
    
}
