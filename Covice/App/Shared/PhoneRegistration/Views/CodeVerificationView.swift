//
//  OnboardingPhoneRegistrationView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol CodeVerificationViewDelegate {
    func didPressContinue(_ onboardingCodeVerificationView: CodeVerificationView, with code: String?)
    func didPressCancel(_ onboardingCodeVerificationView: CodeVerificationView)
    func didPressSendAgain(_ onboardingCodeVerificationView: CodeVerificationView)
}

enum CodeVerificationRequestStatus {
    case none
    case resent
    case loading
    case success
    case error
}

class CodeVerificationView: View {
    
    lazy var activationCode: UITextField = {
        let view = UITextField()
        view.keyboardType = .numberPad
        view.backgroundColor = .clear
        view.layer.borderColor = theme.colors.darkGray.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 10.scaled
        view.font = .systemFont(ofSize: 18.scaled)
        view.attributedPlaceholder = .init(string: Localization.onboarding_verify_code_placeholder,
                                           attributes: [.font: UIFont.systemFont(ofSize: 18.scaled)])
        
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20.scaled, height: 5.scaled))
        view.leftView = paddingView
        view.leftViewMode = .always
        view.bounds.inset(by: UIEdgeInsets(top: 19.scaled, left: 20.scaled, bottom: 19.scaled, right: 20.scaled))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textContentType = .oneTimeCode
        
        return view
    }()
    
    lazy var body: UILabel = {
        let view = UILabel()
        view.text = Localization.onboarding_verify_code_detail
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .natural
        return view
    }()
    
    lazy var sendAgain: LinkLabel = {
        let view = LinkLabel(theme: theme)
        view.text = Localization.onboarding_verify_code_send_again
        view.font = .systemFont(ofSize: 15.scaled, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .natural
        view.isUserInteractionEnabled = true
        view.action = { [weak self] in
            guard let self = self else {
                return
            }
            self.delegate?.didPressSendAgain(self)
        }
        return view
    }()
    
    lazy var cancel: Button = {
        let view = Button(title: Localization.btn_cancel, theme: theme)
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressCancel(this)
        }
        view.style = .info
        view.titleLabel?.font = .systemFont(ofSize: 19.scaled, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var continueBtn: Button = {
        let view = Button(title: Localization.btn_continue, theme: theme)
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressContinue(this, with: this.activationCode.text)
        }
        view.style = .primary
        view.titleLabel?.font = .systemFont(ofSize: 19.scaled, weight: .bold)
        view.isEnabled = false
        view.setTitleColor(theme.colors.white, for: .disabled)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var delegate: CodeVerificationViewDelegate?
    var cancelBottomConstraint: NSLayoutConstraint?
    var continueBottomConstraint: NSLayoutConstraint?
    
    private var codeVerificationRequestStatus: CodeVerificationRequestStatus = .none {
        didSet {
            switch codeVerificationRequestStatus {
            case .loading: continueBtn.isLoading = true
            case .resent:
                continueBtn.isLoading = false
                guard let textCount = activationCode.text?.count else {
                    continueBtn.isEnabled = false
                    break
                }
                continueBtn.isEnabled = textCount <= 9 && textCount >= 6 ? true : false
                break
            default: continueBtn.isLoading = false
            }
        }
    }
    
    init(theme: Theme) {
        super.init(theme: theme)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        addGestureRecognizer(tapGesture)
    }
    
    override func build() {
        super.build()
    
        addSubview(activationCode)
        addSubview(body)
        addSubview(sendAgain)
        addSubview(cancel)
        addSubview(continueBtn)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            activationCode.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.scaled),
            activationCode.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            activationCode.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            activationCode.heightAnchor.constraint(greaterThanOrEqualToConstant: 54.scaled)
        ])
        
        NSLayoutConstraint.activate([
            body.topAnchor.constraint(equalTo: activationCode.bottomAnchor, constant: 15.scaled),
            body.leadingAnchor.constraint(equalTo: activationCode.leadingAnchor),
            body.trailingAnchor.constraint(equalTo: activationCode.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            sendAgain.topAnchor.constraint(equalTo: body.bottomAnchor, constant: 10.scaled),
            sendAgain.leadingAnchor.constraint(equalTo: body.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            cancel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
        ])
        cancelBottomConstraint = cancel.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled)
        cancelBottomConstraint?.isActive = true
        
        NSLayoutConstraint.activate([
            continueBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
        ])
        continueBottomConstraint = continueBtn.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled)
        continueBottomConstraint?.isActive = true
        
    }
    
    func setCodeVerificationStatus(to status: CodeVerificationRequestStatus) {
        codeVerificationRequestStatus = status
    }
    
    func activateContinueBtn() {
        continueBtn.isEnabled = true
    }
    
    func deactivateContinueBtn() {
        continueBtn.isEnabled = false
    }
    
    @objc func doneButtonAction(){
        activationCode.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        cancelBottomConstraint?.constant = -13.scaled - keyboardHeight
        continueBottomConstraint?.constant = -13.scaled - keyboardHeight

        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        cancelBottomConstraint?.constant = -13.scaled
        continueBottomConstraint?.constant = -13.scaled

        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
        }
    }
    
    @objc private func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        activationCode.resignFirstResponder()
    }
}
