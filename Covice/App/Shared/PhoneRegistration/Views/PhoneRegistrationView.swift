//
//  OnboardingPhoneRegistrationView.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol PhoneRegistrationViewDelegate {
    func didPressContinue(_ onboardingPhoneRegistrationView: PhoneRegistrationView)
    func didPressCancel(_ onboardingPhoneRegistrationView: PhoneRegistrationView)
}

enum PhoneRequestRequestStatus {
    case none
    case loading
    case success
    case error
}

class PhoneRegistrationView: View {
    
    lazy var phoneRegistrationView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.spacing = 0
        let background = UIView(frame: view.bounds)
        background.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        background.backgroundColor = .clear
        background.layer.borderWidth = 1
        background.layer.cornerRadius = 10.scaled
        view.addSubview(background)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var phoneNumberContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var phoneNumberEntry: UITextField = {
        let view = UITextField()
        view.backgroundColor  = .clear
        view.layer.cornerRadius = 10.scaled
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        view.keyboardType = .numberPad
        view.clearButtonMode = .always
        view.font = .systemFont(ofSize: 18.scaled)
        view.attributedPlaceholder = .init(string: Localization.onboarding_add_phone_placeholder,
                                           attributes: [.font: UIFont.systemFont(ofSize: 18.scaled)])
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = theme.colors.darkGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var areaCodeContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var areaCodePicker: Picker = {
        let view = Picker(theme: theme,displayDone: false)
        view.backgroundColor = .clear
        view.font = .systemFont(ofSize: 15.scaled, weight: .bold)
        view.textColor = theme.colors.darkBlue
        view.text = "+354"
        view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var body: UILabel = {
        let view = UILabel()
        view.text = Localization.onboarding_add_phone_detail
        view.textColor = theme.colors.darkBlue
        view.font = .systemFont(ofSize: 15.scaled, weight: .regular)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .natural
        return view
    }()
    
    lazy var skip: LinkLabel = {
        let view = LinkLabel(theme: theme)
        view.text = Localization.skip_phone_registration
        view.font = .systemFont(ofSize: 15.scaled, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .natural
        view.isUserInteractionEnabled = true
        
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressCancel(this)
        }
        
        return view
    }()
    
    lazy var cancel: Button = {
        let view = Button(title: Localization.btn_cancel, theme: theme)
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressCancel(this)
        }
        view.style = .info
        view.titleLabel?.font = .systemFont(ofSize: 19.scaled, weight: .bold)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var continueBtn: Button = {
        let view = Button(title: Localization.btn_continue, theme: theme)
        view.action = { [weak self] in
            guard let this = self else { return }
            this.delegate?.didPressContinue(this)
        }
        view.style = .primary
        view.titleLabel?.font = .systemFont(ofSize: 19.scaled, weight: .bold)
        view.setTitleColor(theme.colors.white, for: .disabled)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isEnabled = false
        return view
    }()
    
    var cancelBottomConstraint: NSLayoutConstraint?
    var continueBottomConstraint: NSLayoutConstraint?
    var delegate: PhoneRegistrationViewDelegate?
    
    private var phoneRegistrationStatus: PhoneRequestRequestStatus = .none {
        didSet {
            switch phoneRegistrationStatus {
            case .loading: continueBtn.isLoading = true
            default: continueBtn.isLoading = false
            }
        }
    }
    
    init(theme: Theme) {
        super.init(theme: theme)
        // Use to change position fo the buttons
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        addGestureRecognizer(tapGesture)
    }
    
    override func build() {
        super.build()
        
        addSubview(phoneRegistrationView)
        
        phoneRegistrationView.addArrangedSubview(areaCodeContainerView)
        phoneRegistrationView.addArrangedSubview(separatorView)
        phoneRegistrationView.addArrangedSubview(phoneNumberContainerView)
        
        areaCodeContainerView.addSubview(areaCodePicker)
        phoneNumberContainerView.addSubview(phoneNumberEntry)
        
        addSubview(body)
        addSubview(skip)
        addSubview(cancel)
        addSubview(continueBtn)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        NSLayoutConstraint.activate([
            phoneRegistrationView.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.scaled),
            phoneRegistrationView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
            phoneRegistrationView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
            phoneRegistrationView.heightAnchor.constraint(greaterThanOrEqualToConstant: 54.scaled)
        ])
        
        NSLayoutConstraint.activate([
            areaCodeContainerView.widthAnchor.constraint(lessThanOrEqualToConstant: 150.scaled),
            areaCodeContainerView.widthAnchor.constraint(greaterThanOrEqualToConstant: 90.scaled),
        ])
        
        NSLayoutConstraint.activate([
            phoneNumberContainerView.widthAnchor.constraint(greaterThanOrEqualToConstant: 194.scaled)
        ])
        
        NSLayoutConstraint.activate([
            areaCodePicker.leadingAnchor.constraint(equalTo: areaCodeContainerView.leadingAnchor, constant: 20.scaled),
            areaCodePicker.trailingAnchor.constraint(equalTo: areaCodeContainerView.trailingAnchor,constant: -11.scaled),
            areaCodePicker.centerYAnchor.constraint(equalTo: areaCodeContainerView.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            separatorView.widthAnchor.constraint(equalToConstant: 1.scaled),
            separatorView.heightAnchor.constraint(equalTo: phoneRegistrationView.heightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            phoneNumberEntry.leadingAnchor.constraint(equalTo: phoneNumberContainerView.leadingAnchor, constant: 18.scaled),
            phoneNumberEntry.trailingAnchor.constraint(equalTo: phoneNumberContainerView.trailingAnchor, constant: -18.scaled),
            phoneNumberEntry.centerYAnchor.constraint(equalTo: phoneNumberContainerView.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            body.topAnchor.constraint(equalTo: phoneRegistrationView.bottomAnchor, constant: 15.scaled),
            body.leadingAnchor.constraint(equalTo: phoneRegistrationView.leadingAnchor),
            body.trailingAnchor.constraint(equalTo: phoneRegistrationView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            skip.topAnchor.constraint(equalTo: body.bottomAnchor, constant: 10.scaled),
            skip.leadingAnchor.constraint(equalTo: body.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            cancel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.scaled),
        ])
        
        cancelBottomConstraint = cancel.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled)
        cancelBottomConstraint?.isActive = true
        
        NSLayoutConstraint.activate([
            continueBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.scaled),
        ])
        
        continueBottomConstraint = continueBtn.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -13.scaled)
        continueBottomConstraint?.isActive = true
        
    }
    
    func setRegistrationStatus(to status: PhoneRequestRequestStatus) {
        phoneRegistrationStatus = status
    }
    
    func activateContinueBtn() {
        continueBtn.isEnabled = true
    }
    
    func deactivateContinueBtn() {
        continueBtn.isEnabled = false
    }
    
    @objc func doneButtonAction(){
        phoneNumberEntry.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        cancelBottomConstraint?.constant = -13.scaled - keyboardHeight
        continueBottomConstraint?.constant = -13.scaled - keyboardHeight

        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        cancelBottomConstraint?.constant = -13.scaled
        continueBottomConstraint?.constant = -13.scaled

        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.layoutIfNeeded()
        }
    }
    
    @objc private func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        phoneNumberEntry.resignFirstResponder()
    }
}
