//
//  OnboardingCodeVerificationController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import Combine
import os
import Firebase
import FirebaseMessaging

protocol CodeVerificationControllerDelegate {
    func didFinish(_ onboardingCodeVerificationController: CodeVerificationController)
    func didClose(_ onboardingCodeVerificationController: CodeVerificationController)
}

enum CodeVerificationError: Error, LocalizedError {
    case FirebaseTokenError
    case RecievedCodeError
    var errorDescription: String {
        switch self {
        case .FirebaseTokenError:
            return "Unable to retrive the firebase token"
        case .RecievedCodeError:
            return "Recieved code was nil"
        }
    }
}

class CodeVerificationController: ViewController {
    
    let networkManager: NetworkManager
    let localStorageManager: LocalStorageManager
    let phoneNumber: String
    
    var receivedToken: String
    var verificationCode: String?
    var verifiedToken: String?
    var isModallyPresented: Bool = false {
        didSet {
            self.navigationItem.leftBarButtonItem = isModallyPresented
                ? .init(image: UIImage.close, style: .done, target: self, action: #selector(close))
                : .init()
        }
    }
    var subscriptions = Set<AnyHashable>()
    var delegate: CodeVerificationControllerDelegate?
    
    init(theme: Theme, networkManager: NetworkManager, localStorageManager: LocalStorageManager, receivedToken: String, phoneNumber: String) {
        self.phoneNumber = phoneNumber
        self.receivedToken = receivedToken
        self.networkManager = networkManager
        self.localStorageManager = localStorageManager

        super.init(theme: theme)
    }
    
    override func loadView() {
        self.title = Localization.onboarding_verify_code_title

        let view = CodeVerificationView(theme: theme)
        view.delegate = self
        view.activationCode.delegate = self
        self.internalView = view
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let view = self.internalView as? CodeVerificationView
        view?.activationCode.becomeFirstResponder()
    }
    
    private func verifyPin(with receivedCode:String?){
        guard let code = receivedCode else {
            if let view = self.internalView as? CodeVerificationView {
                view.setCodeVerificationStatus(to: .error)
            }
            self.showError(CodeVerificationError.RecievedCodeError)
            return
        }
        
        let request = VerifyPinRequest(phone: self.phoneNumber, pin: code, token: self.receivedToken, locale: "is")
        if #available(iOS 13.0, *) {
            let sub = self.networkManager.verifyPin(request: request)
                .sink { (completion) in
                    switch completion{
                    case .failure(let error):
                        os_log(.debug, "Failed to verify code with error: %{public}s", error.localizedDescription)
                        DispatchQueue.main.async {
                            if let view = self.internalView as? CodeVerificationView {
                                view.setCodeVerificationStatus(to: .error)
                            }
                            self.showError(error)
                        }
                        break
                    case .finished:
                        break
                    }
                } receiveValue: { [weak self] (response) in
                    os_log(.debug, "Got response from server:%{public}s", response.token)
                    guard let this = self else { return }
                    this.storeFirebaseToken(response.token)
                }
            _ = subscriptions.insert(sub)
        } else {
            self.networkManager.verifyPin(request: request) { [weak self] (result) in
                switch result {
                case .failure(let error):
                    os_log(.debug, "Failed to verify code with error: %{public}s", error.localizedDescription)
                    DispatchQueue.main.async { [weak self] in
                        guard let this = self else {
                            return
                        }
                        if let view = this.internalView as? CodeVerificationView {
                            view.setCodeVerificationStatus(to: .error)
                        }
                        this.showError(error)
                    }
                    break
                case .success(let response):
                    os_log(.debug, "Got response from server:%{public}s", response.token)
                    guard let this = self else { return }
                    this.storeFirebaseToken(response.token)
                    break
                }
            }
        }

    }
    
    private func storeFirebaseToken(_ token: String) {
        Messaging.messaging().token { [weak self] (fbToken, error) in
            guard let this = self else {
                // Return error, can't get self reference
                os_log(.debug, "StoreFirebaseToken: Unable to get reference to self.")
                return
            }
            
            if error != nil {
                os_log(.debug, "Failed to retrive firebase messaging token with error: %{public}s",error!.localizedDescription as CVarArg)
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else {
                        return
                    }
                    if let view = this.internalView as? CodeVerificationView {
                        view.setCodeVerificationStatus(to: .error)
                    }
                    this.showError(CodeVerificationError.FirebaseTokenError)
                }
                return
            }
            
            guard let firebaseToken = fbToken else {
                os_log(.debug, "The firebase token was nil!")
                DispatchQueue.main.async { [weak self] in
                    guard let this = self else {
                        return
                    }
                    if let view = this.internalView as? CodeVerificationView {
                        view.setCodeVerificationStatus(to: .error)
                    }
                    this.showError(CodeVerificationError.FirebaseTokenError)
                }
                return
            }
            let request = StoreFirebaseTokenRequest(pushToken: firebaseToken)
            if #available(iOS 13.0, *) {
                let sub = this.networkManager.storeFirebaseToken(request: request, with: token)
                    .sink { [weak self] (compeltion) in
                        guard let this = self else { return }
                        switch compeltion{
                        case .failure(let error):
                            DispatchQueue.main.async { [weak self] in
                                guard let this = self else {
                                    return
                                }
                                if let view = this.internalView as? CodeVerificationView {
                                    view.setCodeVerificationStatus(to: .error)
                                }
                                this.showError(error.asExposureManagerError)
                            }
                            break
                        case .finished:
                            this.localStorageManager.registeredPhoneNumber.wrappedValue = this.phoneNumber
                            DispatchQueue.main.async { [weak self] in
                                guard let this = self else {
                                    return
                                }
                                if let view = this.internalView as? CodeVerificationView {
                                    view.setCodeVerificationStatus(to: .success)
                                }
                                this.delegate?.didFinish(this)
                            }
                            break
                        }
                    } receiveValue: { (value) in
                        // Do nothing, Wait till the completition gets called.
                    }
                _ = this.subscriptions.insert(sub)
            } else {
                this.networkManager.storeFirebaseToken(request: request, with: token) { [weak self] (result) in
                    guard let this = self else {
                        return
                    }
                    switch result {
                    case .failure(let error):
                        DispatchQueue.main.async { [weak self] in
                            guard let this = self else {
                                return
                            }
                            if let view = this.internalView as? CodeVerificationView {
                                view.setCodeVerificationStatus(to: .error)
                            }
                            this.showError(error)
                        }
                        break
                    case .success(_):
                        this.localStorageManager.registeredPhoneNumber.wrappedValue = this.phoneNumber
                        DispatchQueue.main.async {
                            this.delegate?.didFinish(this)
                        }
                        break
                    }
                }
            }
            
        }
    }
    
    override func showError(_ error: LocalizedError) {
        os_log(.error, ": %{public}s", error.errorDescription! as CVarArg)
        self.presentAlert(title: Localization.error_phone_verification_title, content: Localization.error_phone_verification_content)
    }
    
    @objc func close() {
        delegate?.didClose(self)
    }
    
}

extension CodeVerificationController: CodeVerificationViewDelegate {
    func didPressContinue(_ onboardingCodeVerificationView: CodeVerificationView, with code: String?) {
        onboardingCodeVerificationView.setCodeVerificationStatus(to: .loading)
        verifyPin(with: code)
    }
    
    func didPressCancel(_ onboardingCodeVerificationView: CodeVerificationView) {
        self.delegate?.didClose(self)
    }
    
    func didPressSendAgain(_ onboardingCodeVerificationView: CodeVerificationView) {
        let fullPhoneNumber = self.phoneNumber
        
        onboardingCodeVerificationView.setCodeVerificationStatus(to: .loading)
        
        let request = PinNumberRequest(phone: fullPhoneNumber)
        if #available(iOS 13.0, *) {
            let subscription = networkManager.getPinNumber(request: request)
                .sink { (completion) in
                    switch completion{
                    case .failure(let error):
                        DispatchQueue.main.async {
                            onboardingCodeVerificationView.setCodeVerificationStatus(to: .error)
                            self.showError(error)
                        }
                        os_log(.error, "Unable to get pin for phone number with error: %{public}s", error.asExposureManagerError.localizedDescription)
                        break
                    case .finished:
                        break
                    }
                } receiveValue: { (response) in
                    os_log(.debug, "Got response token.")
                    DispatchQueue.main.async { [weak self] in
                        guard let this = self else { return }
                        onboardingCodeVerificationView.setCodeVerificationStatus(to: .resent)
                        this.receivedToken = response.token
                    }
                }
            _ = subscriptions.insert(subscription)
        } else {
            networkManager.getPinNumber(request: request) { (result) in
                switch result{
                case .failure(let error):
                    DispatchQueue.main.async {
                        onboardingCodeVerificationView.setCodeVerificationStatus(to: .error)
                        self.showError(error)
                    }
                    os_log(.error, "Unable to get pin for phone number with error: %{public}s", error.asExposureManagerError.localizedDescription)
                    break
                case .success(let pinNumberResponse):
                    os_log(.debug, "Got response token.")
                    DispatchQueue.main.async { [weak self] in
                        guard let this = self else { return }
                        onboardingCodeVerificationView.setCodeVerificationStatus(to: .resent)
                        this.receivedToken = pinNumberResponse.token
                    }
                    break
                }
            }
        }
    }
}

extension CodeVerificationController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText:String = textField.text ?? ""
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: string)
        guard let onboardingView = self.view as? CodeVerificationView else {
            return updatedText.count <= 6
        }
        if updatedText.count == 6 {
            onboardingView.activateContinueBtn()
        }
        else if updatedText.count > 6 {
            return updatedText.count < 9
        }
        else {
            onboardingView.deactivateContinueBtn()
        }
        return true
    }
}
