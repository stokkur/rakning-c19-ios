//
//  OnboardingPhoneRegistrationController.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit
import os
import Combine

protocol PhoneRegistrationControllerDelegate {
    func didClose(_ onboardingPhoneRegistrationController: PhoneRegistrationController)
    func didFinish(_ onboardingPhoneRegistrationController: PhoneRegistrationController, with recievedToken: String,and phoneNumber: String)
}

class PhoneRegistrationController: ViewController {
    var subscriptions = Set<AnyHashable>()
    let networkManager: NetworkManager
    var selectedAreaCode: String
    
    var isModallyPresented: Bool = false {
        didSet {
            self.navigationItem.leftBarButtonItem = isModallyPresented
                ? .init(image: UIImage.close, style: .done, target: self, action: #selector(close))
                : .init()
        }
    }
    
    private let areaCodes: [String] = ["+1", "+7", "+20", "+27", "+30", "+31", "+32", "+33", "+34", "+36", "+39", "+40", "+41", "+43", "+44", "+45", "+46", "+47", "+48", "+49", "+51", "+52", "+53", "+54", "+55", "+56", "+57", "+58", "+60", "+61", "+62", "+63", "+64", "+65", "+66", "+77", "+81", "+82", "+84", "+86", "+90", "+91", "+92", "+93", "+94", "+95", "+98", "+212", "+213", "+216", "+218", "+220", "+221", "+222", "+223", "+224", "+225", "+226", "+227", "+228", "+229", "+230", "+231", "+232", "+233", "+234", "+235", "+236", "+237", "+238", "+239", "+240", "+241", "+242", "+243", "+244", "+245", "+246", "+248", "+249", "+250", "+251", "+252", "+253", "+254", "+255", "+256", "+257", "+258", "+260", "+261", "+262", "+263", "+264", "+265", "+266", "+267", "+268", "+269", "+290", "+291", "+297", "+298", "+299", "+345", "+350", "+351", "+352", "+353", "+354", "+355", "+356", "+358", "+359", "+370", "+371", "+372", "+373", "+374", "+375", "+376", "+377", "+378", "+379", "+380", "+381", "+382", "+385", "+386", "+387", "+389", "+420", "+421", "+423", "+500", "+501", "+502", "+503", "+504", "+505", "+506", "+507", "+508", "+509", "+537", "+590", "+591", "+593", "+594", "+595", "+596", "+597", "+598", "+599", "+670", "+672", "+673", "+674", "+675", "+676", "+677", "+678", "+679", "+680", "+681", "+682", "+683", "+685", "+686", "+687", "+688", "+689", "+690", "+691", "+692", "+850", "+852", "+853", "+855", "+856", "+872", "+880", "+886", "+960", "+961", "+962", "+963", "+964", "+965", "+966", "+967", "+968", "+970", "+971", "+972", "+973", "+974", "+975", "+976", "+977", "+992", "+993", "+994", "+995", "+996", "+998", "+1242", "+1246", "+1264", "+1268", "+1284", "+1340", "+1441", "+1473", "+1649", "+1664", "+1670", "+1671", "+1684", "+1758", "+1767", "+1784", "+1849", "+1868", "+1869", "+1876", "+1939"
    ]
    
    var delegate: PhoneRegistrationControllerDelegate?
    
    init(theme: Theme, networkManager: NetworkManager){
        self.networkManager = networkManager
        self.selectedAreaCode = areaCodes[109]
        super.init(theme: theme)
    }
    
    override func loadView() {
        self.title = Localization.onboarding_add_phone_title
        
        let view = PhoneRegistrationView(theme: theme)
        
        view.delegate = self
        view.areaCodePicker.pickerDelegate = self
        view.areaCodePicker.pickerDataSource = self
        view.phoneNumberEntry.delegate = self
        
        self.internalView = view
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let view = self.internalView as? PhoneRegistrationView
        view?.areaCodePicker.selectRow(109)
        view?.phoneNumberEntry.becomeFirstResponder()
    }
    
    override func showError(_ error: LocalizedError) {
        os_log(.error, ": %{public}s", error.localizedDescription)
        self.presentAlert(title: Localization.error_phone_registration_title, content: Localization.error_phone_registration_content)
    }
    
    @objc func close() {
        delegate?.didClose(self)
    }
}

extension PhoneRegistrationController: PhoneRegistrationViewDelegate {
    func didPressContinue(_ onboardingPhoneRegistrationView: PhoneRegistrationView) {
        let phonenumber = onboardingPhoneRegistrationView.phoneNumberEntry.text ?? ""
        let fullPhoneNumber = self.selectedAreaCode + phonenumber
        
        onboardingPhoneRegistrationView.setRegistrationStatus(to: .loading)
        
        let request = PinNumberRequest(phone: fullPhoneNumber)
        if #available(iOS 13.0, *) {
            let subscription = networkManager.getPinNumber(request: request)
                .sink { (completion) in
                    switch completion{
                    case .failure(let error):
                        DispatchQueue.main.async {
                            onboardingPhoneRegistrationView.setRegistrationStatus(to: .error)
                            self.showError(error)
                        }
                        os_log(.error, "Unable to get pin for phone number with error: %{public}s", error.asExposureManagerError.localizedDescription)
                        break
                    case .finished:
                        break
                    }
                } receiveValue: { (response) in
                    os_log(.debug, "Got response token.")
                    DispatchQueue.main.async { [weak self] in
                        guard let this = self else { return }
                        onboardingPhoneRegistrationView.setRegistrationStatus(to: .success)
                        this.delegate?.didFinish(this, with: response.token, and: fullPhoneNumber)
                    }
                }
            _ = subscriptions.insert(subscription)
        } else {
            networkManager.getPinNumber(request: request) { (result) in
                switch result{
                case .failure(let error):
                    DispatchQueue.main.async {
                        onboardingPhoneRegistrationView.setRegistrationStatus(to: .error)
                        self.showError(error)
                    }
                    os_log(.error, "Unable to get pin for phone number with error: %{public}s", error.asExposureManagerError.localizedDescription)
                    break
                case .success(let pinNumberResponse):
                    os_log(.debug, "Got response token.")
                    DispatchQueue.main.async { [weak self] in
                        guard let this = self else { return }
                        onboardingPhoneRegistrationView.setRegistrationStatus(to: .success)
                        this.delegate?.didFinish(this, with: pinNumberResponse.token, and: fullPhoneNumber)
                    }
                    break
                }
            }
        }
    }
    
    func didPressCancel(_ onboardingPhoneRegistrationView: PhoneRegistrationView) {
        self.delegate?.didClose(self)
    }
}
    
extension PhoneRegistrationController: PickerDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return areaCodes.count
    }
}

extension PhoneRegistrationController: PickerDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return areaCodes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedAreaCode = areaCodes[row]
    }
}

extension PhoneRegistrationController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText:String = textField.text ?? ""
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: string)
        guard let view = self.view as? PhoneRegistrationView else {
            return false
        }
        if updatedText.count >= 7 {
            view.activateContinueBtn()
            return updatedText.count <= 14
        }
        else {
            view.deactivateContinueBtn()
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        guard let view = self.view as? PhoneRegistrationView else {
            return false
        }
        view.deactivateContinueBtn()
        return true
    }
}
