//
//  OnboardingPhoneRegistrationCoordinator.swift
//  Covice
//

//  Copyright 2021 Directorate of Health
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import UIKit

protocol PhoneRegistrationCoordinatorDelegate {
    func didFinish(_ onboardingPhoneRegistrationCoordinator: PhoneRegistrationCoordinator)
    func didClose(_ onboardingPhoneRegistrationCoordinator: PhoneRegistrationCoordinator)
}

class PhoneRegistrationCoordinator: Coordinator {
    required init(navigationController: UINavigationController, theme: Theme, networkManager: NetworkManager, exposureManager: ExposureManager, storageManager: StorageManager, localStorageManager: LocalStorageManager, coreDataManager: CoreDataManager) {
        self.navigationController = navigationController
        self.theme = theme
        self.networkManager = networkManager
        self.exposureManager = exposureManager
        self.storageManager = storageManager
        self.localStorageManager = localStorageManager
        self.coreDataManager = coreDataManager
    }
    
    var coreDataManager: CoreDataManager
    
    var networkManager: NetworkManager
    
    var exposureManager: ExposureManager
    
    var storageManager: StorageManager
    
    var localStorageManager: LocalStorageManager
    
    var childCoordinators: [Coordinator] = []
    
    var navigationController: UINavigationController
    
    var asModalFlow: Bool = false
    
    lazy var modalNavigationController: UINavigationController = {
        let controller = NavigationController(theme: theme)
        
        return controller
    }()
    
    var theme: Theme
    
    var delegate: PhoneRegistrationCoordinatorDelegate?
    
    @discardableResult func start() -> UINavigationController {
        phoneRegistrationFlow()
        
        return self.navigationController
    }
}

// MARK: - Phone Registration Flow
extension PhoneRegistrationCoordinator: PhoneRegistrationControllerDelegate {
    func phoneRegistrationFlow() {
        let register = PhoneRegistrationController(theme: theme, networkManager: networkManager)
        register.isModallyPresented = asModalFlow
        register.delegate = self
        
        if asModalFlow {
            modalNavigationController.pushViewController(register, animated: false)
            navigationController.present(modalNavigationController, animated: true)
        } else {
            navigationController.pushViewController(register, animated: true)
            navigationController.setNavigationBarHidden(false, animated: false)
        }
        
        
    }
    
    func didFinish(_ onboardingPhoneRegistrationController: PhoneRegistrationController, with recievedToken: String, and phoneNumber: String) {
        let codeVerification = CodeVerificationController(theme: theme, networkManager: networkManager, localStorageManager: localStorageManager, receivedToken: recievedToken, phoneNumber: phoneNumber)
        codeVerification.isModallyPresented = asModalFlow
        codeVerification.delegate = self
        
        if asModalFlow {
            modalNavigationController.pushViewController(codeVerification, animated: false)
        } else {
            navigationController.pushViewController(codeVerification, animated: true)
        }
    }
    
    func didClose(_ onboardingPhoneRegistrationController: PhoneRegistrationController) {
        if asModalFlow {
            modalNavigationController.dismiss(animated: true)
        } else {
            navigationController.setNavigationBarHidden(true, animated: false)
        }
    
        delegate?.didClose(self)
    }
}

// MARK: - Code Verification Flow
extension PhoneRegistrationCoordinator: CodeVerificationControllerDelegate {
    func didFinish(_ onboardingCodeVerificationController: CodeVerificationController) {
        if asModalFlow {
            modalNavigationController.dismiss(animated: true)
        } else {
            navigationController.setNavigationBarHidden(true, animated: false)
        }
       
        delegate?.didFinish(self)
    }
    
    func didClose(_ onboardingCodeVerificationController: CodeVerificationController) {
        if asModalFlow {
            modalNavigationController.dismiss(animated: true)
        } else {
            navigationController.setNavigationBarHidden(true, animated: false)
        }
        
        delegate?.didClose(self)
    }
}
